﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for PatientViewListViewItemControl.xaml
    /// </summary>
    public partial class PatientViewListViewItemControl : UserControl
    {
        public PatientViewListViewItemControl()
        {
            InitializeComponent();
        }

        public ICommand RXCommand
        {
            get { return (ICommand)GetValue(RXCommandProperty); }
            set { SetValue(RXCommandProperty, value); }
        }

        public static readonly DependencyProperty RXCommandProperty = DependencyProperty.Register("RXCommand",
            typeof(ICommand), typeof(PatientViewListViewItemControl));

        public ICommand ViewCommand
        {
            get { return (ICommand)GetValue(ViewCommandProperty); }
            set { SetValue(ViewCommandProperty, value); }
        }

        public static readonly DependencyProperty ViewCommandProperty = DependencyProperty.Register("ViewCommand",
            typeof(ICommand), typeof(PatientViewListViewItemControl));


        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewCommand != null)
            {
                if (ViewCommand.CanExecute(null))
                {
                    ViewCommand.Execute(null);
                }
            }
        }

        private void RXBtn_Click(object sender, RoutedEventArgs e)
        {
            if(RXCommand != null)
            {
                if (RXCommand.CanExecute(null))
                {
                    RXCommand.Execute(null);
                }
            }
        }
    }
}
