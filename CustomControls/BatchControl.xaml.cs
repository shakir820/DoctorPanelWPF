﻿using DoctorPanelWPF.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for BatchControl.xaml
    /// </summary>
    public partial class BatchControl : UserControl
    {
        public BatchControl()
        {
            InitializeComponent();
        }

        public ICommand RescheduleCommand
        {
            get { return (ICommand)GetValue(RescheduleCommandProperty); }
            set { SetValue(RescheduleCommandProperty, value); }
        }

        public static readonly DependencyProperty RescheduleCommandProperty = DependencyProperty.Register("RescheduleCommand",
            typeof(ICommand), typeof(BatchControl));

       

        private void RescheduleBtn_Click(object sender, RoutedEventArgs e)
        {
            if (RescheduleCommand != null)
            {
                if (RescheduleCommand.CanExecute(null))
                {
                    RescheduleCommand.Execute(null);
                }
            }

            //Dashboard.Current.GoToScheduleEditPage();
           
        }
    }
}
