﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for MyCustomTimePicker2.xaml
    /// </summary>
    public partial class MyCustomTimePicker2 : UserControl, INotifyPropertyChanged
    {
        public MyCustomTimePicker2()
        {
            InitializeComponent();
            GenerateData();
        }
        private DateTime? _mySelectedTime;
        private string _hintString;
        private bool ImWorking = false;

        public ObservableCollection<string> ScheduleTimes = new ObservableCollection<string>();

        public DateTime? MySelectedTime
        {
            get { return (DateTime?)GetValue(MySelectedTimeProperty);}
            set
            {
                if(_mySelectedTime != value)
                {
                    _mySelectedTime = value;

                    if (value == null)
                    {
                        TimePickerComboBox.SelectedValue = null;
                    }
                    else
                    {
                        if (ImWorking != true)
                        {
                            TimePickerComboBox.SelectedValue = ((DateTime)value).ToShortTimeString();
                        }
                    }
                    SetValue(MySelectedTimeProperty, value);
                    //RaisePropertyChanged();
                }  
            }
        }

        public static readonly DependencyProperty MySelectedTimeProperty = DependencyProperty.Register("MySelectedTime",
            typeof(DateTime?), typeof(MyCustomTimePicker2), new PropertyMetadata(null, new
                 PropertyChangedCallback(MySelectedTimeChanged)));

        private static void MySelectedTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var timepicker = (MyCustomTimePicker2)d;
            timepicker.TimePickerComboBox.SelectionChanged -= timepicker.TimePickerComboBox_SelectionChanged;
            timepicker.MySelectedTime = (DateTime?)e.NewValue;
            timepicker.TimePickerComboBox.SelectionChanged += timepicker.TimePickerComboBox_SelectionChanged;
        }

        public string HintString
        {
            get { return (string)GetValue(HintStringProperty); }
            set
            {
                if(_hintString != value)
                {
                    _hintString = value;
                    SetValue(HintStringProperty, value);
                    RaisePropertyChanged();
                }
            }
        }

        public ICommand DropDownClosedCommand
        {
            get { return (ICommand)GetValue(DropDownClosedCommandProperty); }
            set { SetValue(DropDownClosedCommandProperty, value); }
        }


        public static readonly DependencyProperty DropDownClosedCommandProperty = DependencyProperty.Register(
            "DropDownClosedCommand", typeof(ICommand), typeof(MyCustomTimePicker2));

        public static readonly DependencyProperty HintStringProperty = DependencyProperty.Register("HintString",
            typeof(string), typeof(MyCustomTimePicker2), new PropertyMetadata("", new
                 PropertyChangedCallback(HintStringPropertyChanged)));

        private static void HintStringPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var mytimepicker = (MyCustomTimePicker2)d;
            HintAssist.SetHint(mytimepicker, (string)e.NewValue);
        }

        private void GenerateData()
        {
            var today = DateTime.Today;
            var year = today.Year;
            var month = today.Month;
            var day = today.Day;
            int h, m;
            h = m = 0;

            while (true )
            {
                if (m == 60) m = 0;
                if (h == 24) break;
                ScheduleTimes.Add((new DateTime(year, month, day, h, m, 0)).ToShortTimeString());
                m = m + 30;
                if(m == 60) h = h + 1;
            }
        }


        private void TimePickerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TimePickerComboBox.SelectedValue != null)
            {
                string val = (string)TimePickerComboBox.SelectedValue;
                DateTime gg;
                if (DateTime.TryParse(val, out gg))
                {    
                    if (_mySelectedTime != gg)
                    {
                        TimePickerComboBox.SelectionChanged -= TimePickerComboBox_SelectionChanged;
                        ImWorking = true;
                        MySelectedTime = gg;
                        ImWorking = false;
                        TimePickerComboBox.SelectionChanged += TimePickerComboBox_SelectionChanged;
                    }
                }
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            TimePickerComboBox.ItemsSource = ScheduleTimes;
            //DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }


        private void TimePickerComboBox_DropDownClosed(object sender, EventArgs e)
        {
           if(DropDownClosedCommand != null)
            {
                if (DropDownClosedCommand.CanExecute(null))
                {
                    DropDownClosedCommand.Execute(null);
                }
            }
        }
    }
}
