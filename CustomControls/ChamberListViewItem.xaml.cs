﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for ChamberListViewItem.xaml
    /// </summary>
    public partial class ChamberListViewItem : UserControl, INotifyPropertyChanged
    {
        public ChamberListViewItem()
        {
            InitializeComponent();
        }

        #region Depedency Properties 
        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }

        public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register("DeleteCommand", 
            typeof(ICommand), typeof(ChamberListViewItem));


        public ICommand EditCommand
        {
            get { return (ICommand)GetValue(EditCommandProperty); }
            set { SetValue(EditCommandProperty, value); }
        }

        public static readonly DependencyProperty EditCommandProperty = DependencyProperty.Register("EditCommand",
            typeof(ICommand), typeof(ChamberListViewItem));

        public ICommand ViewCommand
        {
            get { return (ICommand)GetValue(ViewCommandProperty); }
            set { SetValue(ViewCommandProperty, value); }
        }

        public static readonly DependencyProperty ViewCommandProperty = DependencyProperty.Register("ViewCommand",
            typeof(ICommand), typeof(ChamberListViewItem));
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
           if(ViewCommand != null)
            {
                ViewCommand.Execute(null);
            }               
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (EditCommand != null)
            {
                EditCommand.Execute(null);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteCommand != null)
            {
                DeleteCommand.Execute(null);
            }
        }
    }
}
