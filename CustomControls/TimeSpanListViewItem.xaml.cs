﻿using DoctorPanelWPF.Helpers;
using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for TimeSpanListViewItem.xaml
    /// </summary>
   
    public partial class TimeSpanListViewItem : UserControl, INotifyPropertyChanged
    {
        public TimeSpanListViewItem()
        {
            InitializeComponent();
        }

        //private bool InOpenTime = false;
        //private bool InCloseTime = false;

        //private DateTime? openTime;
        //private DateTime? closeTime;

        //public DateTime? OpenTime
        //{
        //    get { return openTime; }
        //    set
        //    {
        //        if(openTime != value)
        //        {
        //            openTime = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}

        //public DateTime? CloseTime
        //{
        //    get { return closeTime; }
        //    set
        //    {
        //        if(closeTime != value)
        //        {
        //            closeTime = value;
        //            RaisePropertyChanged();
        //        }
        //    }
        //}



        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register("CancelCommand",
            typeof(ICommand), typeof(TimeSpanListViewItem));

        //public DateTime? OpeningTime
        //{
        //    get { return (DateTime?)GetValue(OpeningTimeProperty); }
        //    set { SetValue(OpeningTimeProperty, value); }
        //}

        //public static readonly DependencyProperty OpeningTimeProperty = DependencyProperty.Register(
        //    "OpeningTime", typeof(DateTime?), typeof(TimeSpanListViewItem), new PropertyMetadata(null, new
        //         PropertyChangedCallback(OpeningTimeChangedCallback)));

        //private static void OpeningTimeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    TimeSpanListViewItem control = (TimeSpanListViewItem)d;
        //    control.openTime = (DateTime?)e.NewValue;
        //    control.RaisePropertyChanged("OpenTime");
        //}

        //public DateTime? ClosingTime
        //{
        //    get { return (DateTime?)GetValue(ClosingTimeProperty); }
        //    set { SetValue(ClosingTimeProperty, value); }
        //}

        //public static readonly DependencyProperty ClosingTimeProperty = DependencyProperty.Register(
        //    "ClosingTime", typeof(DateTime?), typeof(TimeSpanListViewItem), new PropertyMetadata(null, new
        //         PropertyChangedCallback(ClosingTimeChangedCallback)));

        //private static void ClosingTimeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    TimeSpanListViewItem control = (TimeSpanListViewItem)d;
        //    control.closeTime = (DateTime?)e.NewValue;
        //    control.RaisePropertyChanged("CloseTime");
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (CancelCommand != null)
            {
                if (CancelCommand.CanExecute(null))
                {
                    CancelCommand.Execute(null);
                }
            }
        }

       

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

      
    }
}
