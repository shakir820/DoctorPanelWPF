﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for QuestionListViewItem.xaml
    /// </summary>
    public partial class QuestionListViewItem : UserControl, INotifyPropertyChanged
    {
        public QuestionListViewItem()
        {
            InitializeComponent();
        }

        private string _title = "Sample Title";
        private string _detail = "Sample details for the question";
        private string _date = "Dec 15, 2018";
        private int _age = 25;
        private Gender _gender = Gender.Female;
        private string _questionedBy = "Saima";

        private int PreviousLineCount = 1;

        //public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(QuestionListViewItem), new PropertyMetadata(null));
        //public static readonly DependencyProperty DetailProperty = DependencyProperty.Register("Detail", typeof(string), typeof(QuestionListViewItem), new PropertyMetadata(null));

        public string Title { get { return _title; } set { _title = value; RaisePropertyChanged(); } }
        public string Detail { get { return _detail; } set { _detail = value; RaisePropertyChanged(); } }
        public string Date { get { return _date; } set { _date = value; RaisePropertyChanged(); } }
        public int Age { get { return _age; } set { _age = value; RaisePropertyChanged(); } }
        public Gender Gender { get { return _gender; } set { _gender = value; RaisePropertyChanged(); } }
        public string QuestionedBy { get { return _questionedBy; } set { _questionedBy = value; RaisePropertyChanged(); } }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void AnswerBtn_Click(object sender, RoutedEventArgs e)
        {
           

            if (AnswerBtn.Content == "Cancel" || AnswerBtn.Content == "Done")
            {
                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                myDoubleAnimation.To = 0;
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.3));
                AnswerGrid.BeginAnimation(HeightProperty, myDoubleAnimation);
                AnswerBtn.Content = "Answer";
            }
            else
            {
              
                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                //myDoubleAnimation.From = 0;
                myDoubleAnimation.To = 50;
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.3));
                AnswerGrid.BeginAnimation(HeightProperty, myDoubleAnimation);
                AnswerBtn.Content = "Cancel";
            }
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AnswerTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(AnswerTextBox.Text == "")
            {
                AnswerBtn.Content = "Cancel";
            }
            else
            {
                AnswerBtn.Content = "Done";
            }

            if(AnswerTextBox.LineCount > PreviousLineCount)
            {
                PreviousLineCount = AnswerTextBox.LineCount;
                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                //myDoubleAnimation.From = 0;
                myDoubleAnimation.To = AnswerGrid.ActualHeight + 13;
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.3));
                AnswerGrid.BeginAnimation(HeightProperty, myDoubleAnimation);
            }
            else if(AnswerTextBox.LineCount < PreviousLineCount)
            {
                //PreviousLineCount = AnswerTextBox.LineCount;
                //DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                ////myDoubleAnimation.From = 0;
                //myDoubleAnimation.To = AnswerTextBox.LineCount * 13;
                //myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.3));
                //AnswerGrid.BeginAnimation(HeightProperty, myDoubleAnimation);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //NameScope.SetNameScope(this, new NameScope());
            //this.RegisterName(AnswerGrid.Name, "AnswerGrid");
        }

        private void AnswerTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
           
        }

        private void AnswerTextBox_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void AnswerTextBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }

        private void AnswerTextBox_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }
    }
}
