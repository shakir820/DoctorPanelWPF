﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for ChronicDiseaseListViewItemControl.xaml
    /// </summary>
    public partial class ChronicDiseaseListViewItemControl : UserControl
    {
        public ChronicDiseaseListViewItemControl()
        {
            InitializeComponent();
        }

        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }

        public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register("DeleteCommand",
            typeof(ICommand), typeof(ChronicDiseaseListViewItemControl));


        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteCommand != null)
            {
                if (DeleteCommand.CanExecute(null))
                {
                    DeleteCommand.Execute(null);
                }
            }
        }
    }
}
