﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.CustomControls
{
    /// <summary>
    /// Interaction logic for PhoneNumberListControl.xaml
    /// </summary>
    public partial class PhoneNumberListControl : UserControl
    {
        public PhoneNumberListControl()
        {
            InitializeComponent();
            
        }

        public ObservableCollection<PhoneNumberModel> PhoneNumberItemsSource
        {
            get { return (ObservableCollection<PhoneNumberModel>)GetValue(PhoneNumberItemsSourceProperty); }
            set { SetValue(PhoneNumberItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty PhoneNumberItemsSourceProperty = DependencyProperty.Register(
            "PhoneNumberItemsSource", typeof(ObservableCollection<PhoneNumberModel>), typeof(PhoneNumberListControl));

       

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            PhoneNumbersListView.ItemsSource = PhoneNumberItemsSource;
        }
    }
}
