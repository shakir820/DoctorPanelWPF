﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace DoctorPanelWPF.CustomControls
{

    [TemplatePart(Name = "PART_Button", Type = typeof(Button))]
    [TemplatePart(Name = "PART_Popup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_TextBox", Type = typeof(DatePickerTextBox))]
    public class MyCustomTimePicker: TimePicker
    {
        private Popup popup;
        public override void OnApplyTemplate()
        {
            popup = this.GetTemplateChild("PART_Popup") as Popup;
            popup.Closed += Popup_Closed;
            base.OnApplyTemplate();
        }

        public ICommand PopupClosedCommand
        {
            get { return (ICommand)GetValue(PopupClosedCommandProperty);}
            set { SetValue(PopupClosedCommandProperty, value); }
        }

        public static readonly DependencyProperty PopupClosedCommandProperty = DependencyProperty.Register(
            "PopupClosedCommand", typeof(ICommand), typeof(MyCustomTimePicker));

        private void Popup_Closed(object sender, EventArgs e)
        {
            if(PopupClosedCommand != null)
            {
                if (PopupClosedCommand.CanExecute(null))
                {
                    PopupClosedCommand.Execute(null);
                }
            }
        }
    }
}
