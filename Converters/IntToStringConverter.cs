﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.Converters
{
    public class IntToStringConverter: IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int oo;
            if (value != null)
            {
                string val = value.ToString();
                if (int.TryParse(val, out oo))
                {
                    return val;
                }
                else
                {
                    return "";
                }
            }
            else return "";
           
            
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;
            int result;
            bool gg = int.TryParse(val, out result);
            if (gg == true)
            {
                return result;
            }
            else return 0;
        }
    }
}
