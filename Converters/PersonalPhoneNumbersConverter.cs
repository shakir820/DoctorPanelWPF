﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.Converters
{
    public class PersonalPhoneNumbersConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string nums = "";
            var numbers = (ObservableCollection<PhoneNumberModel>)value;
            int count = numbers.Count;

            if(count > 0)
            {
                foreach (var num in numbers)
                {
                    nums = num.PhoneNumber;
                    if (count != 1)
                    {
                        if (num.Count != count)
                        {
                            nums += ", ";
                        }
                    }
                }
            }
            return nums;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
