﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.Converters
{
    public class ClosingTimeConverter: IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                DateTime ClosingTime = (DateTime)value;
                string FormatedClosingTime = ClosingTime.ToString("t", CultureInfo.CreateSpecificCulture("en-US"));
                return FormatedClosingTime;
            }
            else return null;
            
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
