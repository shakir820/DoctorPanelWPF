﻿using HTMLConverter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;

namespace DoctorPanelWPF.Converters
{
    public class html_to_FlowDocumentConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null && (string)value != "")
            {
                try
                {
                    var html_str = (string)value;
                    var xaml_str = HtmlToXamlConverter.ConvertHtmlToXaml(html_str, true);
                    return SetRTF(xaml_str);
                }
                catch
                {
                    return new FlowDocument();
                }
               
                
                //= XamlWriter.Save(ff.Blocks);
                //var Bcollections = ff.Blocks;
               
            }
            return new FlowDocument();
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }



        private static FlowDocument SetRTF(string xamlString)
        {
            StringReader stringReader = new StringReader(xamlString);
            System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
            var sec = XamlReader.Load(xmlReader) as FlowDocument;
            return sec;
            //FlowDocument doc = new FlowDocument();
            //while (sec.Blocks.Count > 0)
            //{
            //    var block = sec.Blocks.FirstBlock;
            //    sec.Blocks.Remove(block);
            //    doc.Blocks.Add(block);
            //}
            //return doc;
        }
    }
}
