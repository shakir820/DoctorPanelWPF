﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.Converters
{
    public class DoubleToStringConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null)
            {
                double val = (double)value;
                int val2 = (int)val;
                return val2.ToString();
            }
            else
            {
                return "";
            }
            
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str =  (string)value;
            double val;
            if (double.TryParse(str, out val))
            {
                return val;
            }
            else return null;
        }
    }
}
