﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.Converters
{
    public class DateTimeStringConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            else
            {
                DateTime dateTime = (DateTime)value;
                return dateTime.ToShortTimeString();
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            else
            {
               
                string str = (string)value;
                DateTime gg;
                if (DateTime.TryParse(str, out gg))
                {
                    return gg;
                }
                else return null;

            }
        }
    }
}
