﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class WeekDayModel: INotifyPropertyChanged
    {
        public ObservableCollection<TimeModel> TimeSlots;
        public string WeekDayName;
        public ScheduleModel Schedule;
        public DateTime OpeningTime;
        public DateTime ClosingTime;
        public event EventHandler<WeekDayCountChangedEventArgs> WeekDayTimeSlotCountChanged;
        public WeekDayModel()
        {
            TimeSlots = new ObservableCollection<TimeModel>();
        }

        public WeekDayModel(string name, ScheduleModel scheduleModel)
        {
           
            WeekDayName = name;
            Schedule = scheduleModel;
            TimeSlots = new ObservableCollection<TimeModel>();
            TimeSlots.Add(new TimeModel(true, this, WeekDayName) { Count = 1, });
            TimeSlots.CollectionChanged += TimeSlots_CollectionChanged;
        }

        private void TimeSlots_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            WeekDayTimeSlotCountChanged?.Invoke(this, new WeekDayCountChangedEventArgs() { count = TimeSlots.Count, WeekDay = this});
        }

        public void ChangeBoundaryClosingTime(DateTime closingTime)
        {
            ClosingTime = closingTime;
            //foreach (var timeslot in TimeSlots)
            //{
            //    timeslot.ChangeBoundaryClosingTime(closingTime);
            //}
        }

        public void ChangeBoundaryOpeningTime(DateTime openingTime)
        {
            OpeningTime = openingTime;
            //foreach(var timeslot in TimeSlots)
            //{
            //    timeslot.ChangeBoundaryOpeningTime(openingTime);
            //}
        }

        public void DeleteTimeSlot(TimeModel timeslot)
        {
            TimeSlots.Remove(timeslot);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }


    public class WeekDayCountChangedEventArgs
    {
        public int count;
        public WeekDayModel WeekDay;
    }
}
