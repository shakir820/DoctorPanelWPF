﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class EducationDetails: INotifyPropertyChanged
    {
        public static EducationDetails Current;
        public ObservableCollection<Degree> Degrees = new ObservableCollection<Degree>(); 

        public EducationDetails()
        {
            Current = this;
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
            Degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE" });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
