﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class MedicineModel : INotifyPropertyChanged
    {
        public MedicineModel()
        {
            DeleteCommand = new RelayCommand(DeleteThisOne);
            MedicineSchedule = new MedicineScheduleModel()
            {
                ThreeTimesADay = new ThreeTimes() { One = 0, Two = 0, Three = 0},
                FourTimesADay = new FourTimes() { One = 0, Two = 0, Three = 0, Four = 0}
            };
            MedicineSchedule.ScheduleChanged += MedicineSchedule_ScheduleChanged;
            MedicineSchedule.ThreeTimesADay.ThreeTimesChanged += ThreeTimesADay_ThreeTimesChanged;
            MedicineSchedule.FourTimesADay.FourTimesChanged += FourTimesADay_FourTimesChanged;

        }

        private void FourTimesADay_FourTimesChanged(object sender, FourTimesChangedEventArgs e)
        {
            RaisePropertyChanged("ScheduleForView");
        }

        private void ThreeTimesADay_ThreeTimesChanged(object sender, ThreeTimesChangedEventArgs e)
        {
            RaisePropertyChanged("ScheduleForView");
        }

        private void MedicineSchedule_ScheduleChanged(object sender, MedicineScheduleChangedEventArgs e)
        {
            RaisePropertyChanged("ScheduleForView");
        }

        private ICommand deleteCommand;
        private string name;
        private DrugType medicineType;
        private ScheduleFrequency frequency;
        private MedicineScheduleModel medicineSchedule;
        private string unit;
        private Route medicineRoute;
        private string note;
        private MedicineDurationType duration;
        private PrescriptionModel prescription;
        private string durationText;
        private int count;

        public int Count { get { if (Prescription != null) { return Prescription.Medicines.Count; } else return 0; } set { count = value; RaisePropertyChanged(); } }
        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public DrugType MedicineType { get { return medicineType; } set { medicineType = value; RaisePropertyChanged(); } }
        public ScheduleFrequency Frequency { get { return frequency; } set { frequency = value; RaisePropertyChanged(); RaisePropertyChanged("ScheduleForView"); } }
        public MedicineScheduleModel MedicineSchedule { get { return medicineSchedule; } set { medicineSchedule = value; RaisePropertyChanged(); RaisePropertyChanged("ScheduleForView"); } }
        public string Unit { get { return unit; } set { unit = value; RaisePropertyChanged(); } }
        public Route MedicineRoute { get { return medicineRoute;  } set { medicineRoute = value; RaisePropertyChanged(); } }
        public string Note { get { return note; } set { note = value; RaisePropertyChanged(); } }
        public MedicineDurationType Duration { get { return duration; } set { duration = value; RaisePropertyChanged(); } }
        public PrescriptionModel Prescription
        {
            get { return prescription; }
            set
            {
                prescription = value;
                RaisePropertyChanged();
            }
        }
        public string DurationText { get { return durationText; } set { durationText = value; RaisePropertyChanged(); } }
        public ICommand DeleteCommand { get { return deleteCommand; } set { deleteCommand = value; RaisePropertyChanged(); } }

        public string ScheduleForView
        {
            get
            {
                if (Frequency == ScheduleFrequency.ThriceADay)
                {
                    return (int)MedicineSchedule.ThreeTimesADay.One
                        + " + " +
                        (int)MedicineSchedule.ThreeTimesADay.Two
                        + " + " +
                        (int)MedicineSchedule.ThreeTimesADay.Three;
                }
                else if (Frequency == ScheduleFrequency.FourTimesInADay)
                {
                    return (int)MedicineSchedule.FourTimesADay.One
                        + " + " +
                        (int)MedicineSchedule.FourTimesADay.Two
                        + " + " +
                        (int)MedicineSchedule.FourTimesADay.Three
                        + " + " +
                        (int)MedicineSchedule.FourTimesADay.Four;
                }
                else if (Frequency == ScheduleFrequency.AsNeeded)
                {
                    return MedicineSchedule.AsNeeded;
                }
                else return "";
            }
        }

        private void DeleteThisOne()
        {
            Prescription.Medicines.Remove(this);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    

    public enum DrugType
    {
        Tab,
        Cap,
        Spray,
        Suppository,
        Inhaler,
        Nebulizer,
        Ointment,
        Cream,
        EyeDrop,
        Drop
    }

    public enum ScheduleFrequency
    {
        ThriceADay,
        FourTimesInADay,
        AsNeeded
    }

    public enum Route
    {
        PerUrethal,
        OverTheTongue,
        Ear,
        Eye,
        Nasal,
        Inhalation,
        Skin,
        Locally,
        Oral,
        IV,
        IM,
        SC,
        PerRectal,
        IntraDermal,
        SubLingual,
        PerVeginal
    }

    public enum MedicineDurationType
    {
        Days,
        Weeks,
        Months,
        Years,
        Continue
    }

    public class ThreeTimes : INotifyPropertyChanged
    {
        private double? one;
        private double? two;
        private double? three;

        public double? One { get { return one; } set { one = value; RaisePropertyChanged(); RaiseThreeTimesChanged(); } }
        public double? Two { get { return two; } set { two = value; RaisePropertyChanged(); RaiseThreeTimesChanged(); } }
        public double? Three { get { return three; } set { three = value; RaisePropertyChanged(); RaiseThreeTimesChanged(); } }



        public event EventHandler<ThreeTimesChangedEventArgs> ThreeTimesChanged;

        private void RaiseThreeTimesChanged()
        {
            ThreeTimesChanged?.Invoke(this, new ThreeTimesChangedEventArgs());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class ThreeTimesChangedEventArgs: EventArgs
    {

    }

    public class FourTimes : INotifyPropertyChanged
    {
        private double? one;
        private double? two;
        private double? three;
        private double? four;

        public double? One { get { return one; } set { one = value; RaisePropertyChanged(); RaiseFourTimesChanged(); } }
        public double? Two { get { return two; } set { two = value; RaisePropertyChanged(); RaiseFourTimesChanged(); } }
        public double? Three { get { return three; } set { three = value; RaisePropertyChanged(); RaiseFourTimesChanged(); } }
        public double? Four { get { return four; } set { four = value; RaisePropertyChanged(); RaiseFourTimesChanged(); } }

        public event EventHandler<FourTimesChangedEventArgs> FourTimesChanged;

        private void RaiseFourTimesChanged()
        {
            FourTimesChanged?.Invoke(this, new FourTimesChangedEventArgs());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class FourTimesChangedEventArgs: EventArgs
    {

    }

    public class MedicineScheduleModel : INotifyPropertyChanged
    {
        private string asNeeded;
        private ThreeTimes threeTimesADay;
        private FourTimes fourTimesADay;


        public string AsNeeded { get { return asNeeded; } set { asNeeded = value; RaisePropertyChanged(); RaiseScheduleChanged(); } }
        public ThreeTimes ThreeTimesADay { get { return threeTimesADay; } set { threeTimesADay = value; RaisePropertyChanged(); RaiseScheduleChanged(); } }
        public FourTimes FourTimesADay { get { return fourTimesADay; } set { fourTimesADay = value; RaisePropertyChanged(); RaiseScheduleChanged(); } }




        public event EventHandler<MedicineScheduleChangedEventArgs> ScheduleChanged;

        private void RaiseScheduleChanged()
        {
            ScheduleChanged?.Invoke(this, new MedicineScheduleChangedEventArgs());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class MedicineScheduleChangedEventArgs: EventArgs
    {

    }
}
