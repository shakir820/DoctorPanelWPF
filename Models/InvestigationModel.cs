﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class InvestigationModel: INotifyPropertyChanged
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }
        private PrescriptionModel prescriptionModel;

        public PrescriptionModel Prescription
        {
            get { return prescriptionModel; }
            set { prescriptionModel = value; RaisePropertyChanged(); }
        }


        private string _note;
        public string Note { get { return _note; } set { _note = value; RaisePropertyChanged(); } }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get { return _deleteCommand; }
            set { _deleteCommand = value; RaisePropertyChanged(); }
        }

        private void Delete()
        {
            if (Prescription != null)
            {
                Prescription.Investigations.Remove(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
