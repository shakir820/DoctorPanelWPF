﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.API_Data
{
    public partial class Welcome
    {
        [JsonProperty("districts")]
        public List<District> Districts { get; set; }

        [JsonProperty("_metadata")]
        public Metadata Metadata { get; set; }
    }

    public partial class District : INotifyPropertyChanged
    {
        #region fields
        public long _districtId;
        public string _districtName;
        public object _districtBnName;
        public long _divisionId;
        public string _divisionName;
        public object _bbsCode;
        public double _latitude;
        public double _longitude;
        public object _areas;
        #endregion



        #region properties
        [JsonProperty("district_id")]
        public long DistrictId { get { return _districtId; } set { _districtId = value; RaisePropertyChanged(); } }

        [JsonProperty("district_name")]
        public string DistrictName { get { return _districtName; } set { _districtName = value; RaisePropertyChanged(); } }

        [JsonProperty("district_bn_name")]
        public object DistrictBnName { get { return _districtBnName; } set { _districtBnName = value; RaisePropertyChanged(); } }

        [JsonProperty("division_id")]
        public long DivisionId { get { return _divisionId; } set { _divisionId = value; RaisePropertyChanged(); } }

        [JsonProperty("division_name")]
        public string DivisionName { get { return _divisionName; } set { _divisionName = value; RaisePropertyChanged(); } }

        [JsonProperty("bbs_code")]
        public object BbsCode { get { return _bbsCode; } set { _bbsCode = value; RaisePropertyChanged(); } }

        [JsonProperty("latitude")]
        public double Latitude { get { return _latitude; } set { _latitude = value; RaisePropertyChanged(); } }

        [JsonProperty("longitude")]
        public double Longitude { get { return _longitude; } set { _longitude = value; RaisePropertyChanged(); } }

        [JsonProperty("areas")]
        public object Areas { get { return _areas; } set { _areas = value; RaisePropertyChanged(); } }
        #endregion



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public partial class Metadata
    {
        [JsonProperty("total_result")]
        public long TotalResult { get; set; }
    }
}
