﻿using DoctorPanelWPF.Models.CommunicateWithServer;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.API_Data
{
    public partial class Welcome
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("registration_status")]
        public bool RegistrationStatus { get; set; }

        [JsonProperty("login_status")]
        public bool LoginStatus { get; set; }

        [JsonProperty("status_code")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long StatusCode { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("divisions")]
        public List<Division> Divisions { get; set; }
    }

    public partial class User : INotifyPropertyChanged
    {
        public static User Current;

        public User()
        {
            Current = this;
        }


        [JsonProperty("user_id")]
        public long UserId { get; set; }


        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("username")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Username { get; set; }

        [JsonProperty("is_temp_password_used")]
        public bool IsTempPasswordUsed { get; set; }

        [JsonProperty("request_change_email")]
        public bool RequestChangeEmail { get; set; }

        [JsonProperty("short_bio")]
        public string ShortBio { get; set; }

        [JsonProperty("short_bio_bn")]
        public object ShortBioBn { get; set; }

        [JsonProperty("email_verified")]
        public bool EmailVerified { get; set; }

        [JsonProperty("mobile_verified")]
        public bool MobileVerified { get; set; }

        [JsonProperty("subscription_type")]
        public string SubscriptionType { get; set; }

        [JsonProperty("is_subscribed")]
        public bool IsSubscribed { get; set; }

        [JsonProperty("subscribed_number")]
        public bool SubscribedNumber { get; set; }

        [JsonProperty("is_premium")]
        public bool IsPremium { get; set; }

        [JsonProperty("email_verification_code")]
        public string EmailVerificationCode { get; set; }

        [JsonProperty("is_password_set")]
        public bool IsPasswordSet { get; set; }

        [JsonProperty("father_name")]
        public object FatherName { get; set; }

        [JsonProperty("mother_name")]
        public object MotherName { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("blood_group")]
        public object BloodGroup { get; set; }

        [JsonProperty("date_of_birth")]
        public object DateOfBirth { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("nid")]
        public object Nid { get; set; }

        [JsonProperty("birth_cert_id")]
        public object BirthCertId { get; set; }

        [JsonProperty("passport_no")]
        public object PassportNo { get; set; }

        [JsonProperty("alternate_email")]
        public object AlternateEmail { get; set; }

        [JsonProperty("alternate_mobile")]
        public object AlternateMobile { get; set; }

        [JsonProperty("division_name")]
        public string DivisionName { get; set; }

        [JsonProperty("district_name")]
        public string DistrictName { get; set; }

        [JsonProperty("upazilla_name")]
        public object UpazillaName { get; set; }

        [JsonProperty("division_id")]
        public long? DivisionId { get; set; }

        [JsonProperty("district_id")]
        public long? DistrictId { get; set; }

        [JsonProperty("upazilla_id")]
        public long? UpazillaId { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("marital_status")]
        public object MaritalStatus { get; set; }

        [JsonProperty("emergency_contact")]
        public object EmergencyContact { get; set; }

        [JsonProperty("registered_from")]
        public string RegisteredFrom { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("is_trial_user")]
        public bool IsTrialUser { get; set; }

        [JsonProperty("subscription_plan_id")]
        public object SubscriptionPlanId { get; set; }

        [JsonProperty("enabled_subscription")]
        public bool EnabledSubscription { get; set; }

        [JsonProperty("is_practitioner")]
        public bool IsPractitioner { get; set; }

        [JsonProperty("practitioner_info")]
        public PractitionerInfo PractitionerInfo { get; set; }

        [JsonProperty("subscription_max_expiry_date")]
        public object SubscriptionMaxExpiryDate { get; set; }

        [JsonProperty("subscribed_plans")]
        public List<SubscribedPlan> SubscribedPlans { get; set; }

        [JsonProperty("user_nominee_info")]
        public object UserNomineeInfo { get; set; }


        public async Task<bool> UpdateDataAsync(Dictionary<string, object> keyValuePairs)
        {
            string uri = "http://api.healthsheba.com/api/v2/practitioner/profile/update?api_token=" + AccessToken;
            bool YesYouCanHitAPI = false;

            if (keyValuePairs.Count > 0)
            {
                foreach (var item in keyValuePairs)
                {
                    string parameter_name = item.Key;

                    if(item.Value != null)
                    {
                        var val = Convert.ChangeType(item.Value, item.Value.GetType());
                        uri += "&";
                        uri += (parameter_name + "=" + val);
                        YesYouCanHitAPI = true;
                    }
                }
            }

            if (YesYouCanHitAPI == true)
            {
                var finalUri = uri;
                return await mDaktarServer.HitApiAsync(finalUri);
            }
            else return false;
        }

      

        

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public partial class PractitionerInfo : INotifyPropertyChanged
    {


        [JsonProperty("qualification_title")]
        public object QualificationTitle { get; set; }

        //was long
        [JsonProperty("bmdc_cert_no")]
        //[JsonConverter(typeof(ParseStringConverter))]
        public string BmdcCertNo { get; set; }

        [JsonProperty("bmdc_cert_img")]
        public object BmdcCertImg { get; set; }

        [JsonProperty("designation")]
        public object Designation { get; set; }

        [JsonProperty("educational_info")]
        public EducationalInfo EducationalInfo { get; set; }

        [JsonProperty("specialities")]
        public List<Speciality> Specialities { get; set; }

        [JsonProperty("national_id")]
        public object NationalId { get; set; }

        [JsonProperty("membership_number")]
        public object MembershipNumber { get; set; }

        [JsonProperty("email")]
        public object Email { get; set; }

        [JsonProperty("mobile")]
        public object Mobile { get; set; }

        [JsonProperty("mobile_2")]
        public object Mobile2 { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("division")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Division { get; set; }

        [JsonProperty("district")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long District { get; set; }

        [JsonProperty("upazilla")]
        public object Upazilla { get; set; }

        [JsonProperty("area")]
        public object Area { get; set; }

        [JsonProperty("latitude")]
        public object Latitude { get; set; }

        [JsonProperty("longitude")]
        public object Longitude { get; set; }

        [JsonProperty("image_path")]
        public object ImagePath { get; set; }

        [JsonProperty("personal_details_provided")]
        public bool PersonalDetailsProvided { get; set; }

        [JsonProperty("educational_details_provided")]
        public bool EducationalDetailsProvided { get; set; }

        [JsonProperty("certificate_details_provided")]
        public bool CertificateDetailsProvided { get; set; }

        [JsonProperty("specialization_designation_provided")]
        public bool SpecializationDesignationProvided { get; set; }

        [JsonProperty("chamber_schedule_added")]
        public bool ChamberScheduleAdded { get; set; }

        [JsonProperty("profile_completeness")]
        public long ProfileCompleteness { get; set; }

        [JsonProperty("verified_doctor")]
        public long VerifiedDoctor { get; set; }

        [JsonProperty("bmdc_cert_no_verified")]
        public long BmdcCertNoVerified { get; set; }

        [JsonProperty("bmdc_cert_img_verified")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long BmdcCertImgVerified { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public partial class SubscribedPlan
    {
        [JsonProperty("plan_id")]
        public long PlanId { get; set; }

        [JsonProperty("plan_name")]
        public string PlanName { get; set; }

        [JsonProperty("plan_slug")]
        public string PlanSlug { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("subscription_activation_date")]
        public object SubscriptionActivationDate { get; set; }

        [JsonProperty("subscription_expiry_date")]
        public object SubscriptionExpiryDate { get; set; }

        [JsonProperty("insurance_claim_info")]
        public InsuranceClaimInfo InsuranceClaimInfo { get; set; }
    }

    public partial class InsuranceClaimInfo
    {
        [JsonProperty("ipd")]
        public Ipd Ipd { get; set; }

        [JsonProperty("opd")]
        public Ipd Opd { get; set; }

        [JsonProperty("lab_test")]
        public Ipd LabTest { get; set; }

        [JsonProperty("life_insurance")]
        public Ipd LifeInsurance { get; set; }
    }

    public partial class Ipd
    {
        [JsonProperty("claim_limit")]
        public long ClaimLimit { get; set; }

        [JsonProperty("max_limit_per_claim")]
        public long MaxLimitPerClaim { get; set; }

        [JsonProperty("max_limit_per_night", NullValueHandling = NullValueHandling.Ignore)]
        public long? MaxLimitPerNight { get; set; }

        [JsonProperty("total_claimed")]
        public long TotalClaimed { get; set; }

        [JsonProperty("claim_left")]
        public long ClaimLeft { get; set; }
    }

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }
}
