﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.API_Data
{
    public partial class EducationalInfo
    {
        [JsonProperty("details")]
        public List<Detail> Details { get; set; }
    }

    public partial class Detail
    {
        [JsonProperty("degree_name")]
        public string DegreeName { get; set; }

        [JsonProperty("degree_id")]
        public long DegreeId { get; set; }

        [JsonProperty("institute")]
        public string Institute { get; set; }

        [JsonProperty("passing_year")]
        public string PassingYear { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }
    }

    public partial class Speciality
    {
        [JsonProperty("id")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

}
