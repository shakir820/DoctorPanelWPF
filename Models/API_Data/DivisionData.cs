﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DoctorPanelWPF.Models.API_Data
{

    public partial class Division: INotifyPropertyChanged
    {
        #region fields
        public long _divisionId;
        public string _divisionName;
        public string _divisionBnName;
        public string _bbsCode;   
        public long? _latitude;
        public long? _longitude;
        public object _districts;
        #endregion


        #region properties
        [JsonProperty("division_id")]
        public long DivisionId { get { return _divisionId; } set { _divisionId = value; RaisePropertyChanged(); } }

        [JsonProperty("division_name")]
        public string DivisionName { get { return _divisionName; } set { _divisionName = value; RaisePropertyChanged(); } }

        [JsonProperty("division_bn_name")]
        public string DivisionBnName { get { return _divisionBnName; } set { _divisionBnName = value; RaisePropertyChanged(); } }

        [JsonProperty("bbs_code")]
        public string BbsCode { get { return _bbsCode; } set { _bbsCode = value; RaisePropertyChanged(); } }

        [JsonProperty("latitude")]
        public long? Latitude { get { return _latitude; } set { _latitude = value; RaisePropertyChanged(); } }

        [JsonProperty("longitude")]
        public long? Longitude { get { return _longitude; } set { _longitude = value; RaisePropertyChanged(); } }

        [JsonProperty("districts")]
        public object Districts { get { return _districts; } set { _districts = value; RaisePropertyChanged(); } }
        #endregion


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
