﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.API_Data
{
    public static class CRUDHelper
    {
        public static void UpdateData(object rootObject, Dictionary<string, object> data)
        {
            string uri = "http://api.healthsheba.com/api/v2/practitioner/profile/update?api_token=" + User.Current.AccessToken;
            bool YesYouCanHitAPI = false;

            if (data.Count > 0)
            {
                foreach(var item in data)
                {
                    var propertyName = GetPropertyName(item.Key);
                    Type valueType = item.Value.GetType();
                    var val = Convert.ChangeType(item.Value, valueType);
                    uri += "&";
                    uri += propertyName + "=" + val;
                    Type t = rootObject.GetType();
                    var pr = t.GetProperties(BindingFlags.Public);
                    YesYouCanHitAPI = true;

                    PropertyInfo pi = rootObject.GetType().GetProperty(item.Key);
                    pi.SetValue(rootObject, item.Value);
                }
            }

            if(YesYouCanHitAPI == true)
            {
                var finalUri = uri;
            }
        }


        public static string GetPropertyName(string propertyName)
        {
            //var ty = User.Current.Name.GetType();

            JsonPropertyAttribute JsonPropertyName = (JsonPropertyAttribute)(typeof(User).GetProperty(propertyName).GetCustomAttribute(typeof(JsonPropertyAttribute)));
            return JsonPropertyName.PropertyName;
        }
    }
}
