﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.LoginRemeberPassword
{
    [Serializable]
    public class LoginInfoModel
    {
        public string email;
        public string password;
        public bool rememberPassword;
    }


    public static class LoginHelper
    {
        public static void SaveLoginInfo(LoginInfoModel loginInfoModel)
        {
            var folderpath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var path = folderpath + "/LoginInfo.bin";

            FileStream fs = new FileStream(path, FileMode.Create);

            // Construct a BinaryFormatter and use it to serialize the data to the stream.
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, loginInfoModel);
            }
            catch (SerializationException e)
            {

            }
            finally
            {
                fs.Close();
            }
        }


        public static LoginInfoModel RetrieveLoginInfo()
        {
            
            LoginInfoModel addresses = null;

            var folderpath = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData);
            var path = folderpath + "/LoginInfo.bin";
            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open);
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    // Deserialize the hashtable from the file and 
                    // assign the reference to the local variable.
                    addresses = (LoginInfoModel)formatter.Deserialize(fs);
                }
                catch (SerializationException e)
                {

                }
                finally
                {
                    fs.Close();
                }
            }
            // Open the file containing the data that you want to deserialize.
            return addresses;
        }
    }
}
