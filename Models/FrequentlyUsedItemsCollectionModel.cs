﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class FrequentlyUsedItemsCollectionModel: INotifyPropertyChanged
    {
        public static FrequentlyUsedItemsCollectionModel Current;
        public ObservableCollection<ChiefComplainModel> ChiefComplains = new ObservableCollection<ChiefComplainModel>();
        public ObservableCollection<ChronicDiseaseModel> ChronicDiseases = new ObservableCollection<ChronicDiseaseModel>();
        public ObservableCollection<PastMedicalModel> PastMedicalHistory = new ObservableCollection<PastMedicalModel>();
        public ObservableCollection<AllergyModel> Allergies = new ObservableCollection<AllergyModel>();
        public ObservableCollection<FamilyHistoryModel> FamilyHistory = new ObservableCollection<FamilyHistoryModel>();
        public ObservableCollection<InvestigationModel> InvestigationLabTests = new ObservableCollection<InvestigationModel>();
        public ObservableCollection<DiagnosisModel> DiagnosisList = new ObservableCollection<DiagnosisModel>();
        public ObservableCollection<MedicineModel> MedicineList = new ObservableCollection<MedicineModel>();
        public ObservableCollection<AdviceModel> AdviceList = new ObservableCollection<AdviceModel>();


        public FrequentlyUsedItemsCollectionModel()
        {
            Current = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
