﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DoctorPanelWPF.Models
{
    public class UserModel : INotifyPropertyChanged
    {
        #region private fields

        private long user_id;
        private string access_token;
        private string name;
        private string email;
        private string mobile;
        public long username;
        private bool is_temp_password_used;
        private bool request_change_email;
        private object short_bio;
        private object short_bio_bn;
        private bool email_verified;
        private bool mobile_verified;
        private string subscription_type;
        public bool is_subscribed;
        public bool subscribed_number;
        public bool is_premium;
        public string email_verification_code;
        public bool is_password_set;
        public object father_name;
        public object mother_name;
        public string gender;
        public object blood_group;
        public object date_of_birth;
        public object age;
        public object nid;
        public object birth_cert_id;
        public object passport_no;
        public object alternate_email;
        public string alternate_mobile;
        public string division_name;
        public string district_name;
        public string upazilla_name;
        public long? division_id;
        public long? district_id;
        public long? upazilla_id;
        public string address;
        public object marital_status;
        public object emergency_contact;
        public object registered_from;
        public object image;
        public bool is_trial_user;
        public object subscription_plan_id;
        public bool enabled_subscription;
        public bool is_practitioner;
        public PractitionerInfo practitioner_info;
        public object subscription_max_expiry_date;
        //public SubscribedPlan[] subscribed_plans;
        public object user_nominee_info;
        #endregion

        #region Pubic Property
        public long UserId
        {
            get { return user_id; }
            set
            {
                user_id = value;
                RaisePropertyChanged();
            }
        }

        public string AccessToken
        {
            get { return access_token; }
            set { access_token = value; RaisePropertyChanged(); }
        }


        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }


        public string Email { get { return email; } set { email = value; RaisePropertyChanged(); } }


        public string Mobile { get { return mobile; } set { mobile = value; RaisePropertyChanged(); } }


        public long Username { get { return username; } set { username = value; RaisePropertyChanged(); } }


        public bool IsTempPasswordUsed { get { return is_temp_password_used; } set { is_temp_password_used = value; RaisePropertyChanged(); } }


        public bool RequestChangeEmail { get { return request_change_email; } set { request_change_email = value; RaisePropertyChanged(); } }


        public object ShortBio { get { return short_bio; } set { short_bio = value; RaisePropertyChanged(); } }


        public object ShortBioBn { get { return short_bio_bn; } set { short_bio_bn = value; RaisePropertyChanged(); } }


        public bool EmailVerified { get { return email_verified; } set { email_verified = value; RaisePropertyChanged(); } }


        public bool MobileVerified { get { return mobile_verified; } set { mobile_verified = value; RaisePropertyChanged(); } }


        public string SubscriptionType { get { return subscription_type; } set { subscription_type = value; RaisePropertyChanged(); } }


        public bool IsSubscribed { get { return is_subscribed; } set { is_subscribed = value; RaisePropertyChanged(); } }


        public bool SubscribedNumber { get { return subscribed_number; } set { subscribed_number = value; RaisePropertyChanged(); } }


        public bool IsPremium { get { return is_premium; } set { is_premium = value; RaisePropertyChanged(); } }


        public string EmailVerificationCode { get { return email_verification_code; } set { email_verification_code = value; RaisePropertyChanged(); } }


        public bool IsPasswordSet { get { return is_password_set; } set { is_password_set = value; RaisePropertyChanged(); } }


        public object FatherName { get { return father_name; } set { father_name = value; RaisePropertyChanged(); } }


        public object MotherName { get { return mother_name; } set { mother_name = value; RaisePropertyChanged(); } }


        public string Gender { get { return gender; } set { gender = value; RaisePropertyChanged(); } }


        public object BloodGroup { get { return blood_group; } set { blood_group = value; RaisePropertyChanged(); } }


        public object DateOfBirth { get { return date_of_birth; } set { date_of_birth = value; RaisePropertyChanged(); } }


        public object Age { get { return age; } set { age = value; RaisePropertyChanged(); } }


        public object Nid { get { return nid; } set { nid = value; RaisePropertyChanged(); } }


        public object BirthCertId { get { return birth_cert_id; } set { birth_cert_id = value; RaisePropertyChanged(); } }


        public object PassportNo { get { return passport_no; } set { passport_no = value; RaisePropertyChanged(); } }


        public object AlternateEmail { get { return alternate_email; } set { alternate_email = value; RaisePropertyChanged(); } }


        public string AlternateMobile { get { return alternate_mobile; } set { alternate_mobile = value; RaisePropertyChanged(); } }


        public string DivisionName { get { return division_name; } set { division_name = value; RaisePropertyChanged(); } }


        public string DistrictName { get { return district_name; } set { district_name = value; RaisePropertyChanged(); } }


        public string UpazillaName { get { return upazilla_name; } set { upazilla_name = value; RaisePropertyChanged(); } }


        public long? DivisionId { get { return division_id; } set { division_id = value; RaisePropertyChanged(); } }


        public long? DistrictId { get { return district_id; } set { district_id = value; RaisePropertyChanged(); } }


        public long? UpazillaId { get { return upazilla_id; } set { upazilla_id = value; RaisePropertyChanged(); } }


        public string Address { get { return address; } set { address = value; RaisePropertyChanged(); } }


        public object MaritalStatus { get { return marital_status; } set { marital_status = value; RaisePropertyChanged(); } }


        public object EmergencyContact { get { return emergency_contact; } set { emergency_contact = value; RaisePropertyChanged(); } }


        public object RegisteredFrom { get { return registered_from; } set { registered_from = value; RaisePropertyChanged(); } }


        public object Image { get { return image; } set { image = value; RaisePropertyChanged(); } }


        public bool IsTrialUser { get { return is_trial_user; } set { is_trial_user = value; RaisePropertyChanged(); } }


        public object SubscriptionPlanId { get { return subscription_plan_id; } set { subscription_plan_id = value; RaisePropertyChanged(); } }


        public bool EnabledSubscription { get { return enabled_subscription; } set { enabled_subscription = value; RaisePropertyChanged(); } }


        public bool IsPractitioner { get { return is_practitioner; } set { is_practitioner = value; RaisePropertyChanged(); } }


        public PractitionerInfo PractitionerInfo { get { return practitioner_info; } set { practitioner_info = value; RaisePropertyChanged(); } }


        public object SubscriptionMaxExpiryDate { get { return subscription_max_expiry_date; } set { subscription_max_expiry_date = value; RaisePropertyChanged(); } }

        public ObservableCollection<SubscribedPlan> SubscribedPlans = new ObservableCollection<SubscribedPlan>();


        public object UserNomineeInfo { get { return user_nominee_info; } set { user_nominee_info = value; RaisePropertyChanged(); } }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }



    public partial class SubscribedPlan
    {
        public long PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanSlug { get; set; }
        public bool IsActive { get; set; }
        public object SubscriptionActivationDate { get; set; }
        public object SubscriptionExpiryDate { get; set; }
        public InsuranceClaimInfo InsuranceClaimInfo { get; set; }
    }

    public partial class InsuranceClaimInfo
    {
        public Ipd Ipd { get; set; }
        public Ipd Opd { get; set; }
        public Ipd LabTest { get; set; }
        public Ipd LifeInsurance { get; set; }
    }

    public partial class Ipd
    {
        public long ClaimLimit { get; set; }
        public long MaxLimitPerClaim { get; set; }
        public long? MaxLimitPerNight { get; set; }
        public long TotalClaimed { get; set; }
        public long ClaimLeft { get; set; }
    }


    public partial class PractitionerInfo : INotifyPropertyChanged
    {
        private object _qualificationTitle;
        private string _bmdcCertNo;
        private object _bmdcCertImg;
        private object _designation;
        private object _educationalInfo;
        private object _specialities;
        private object _nationalId;
        private object _membershipNumber;
        private object _email;
        private string _mobile;
        private string _mobile2;
        private string _address;
        private long _division;
        private long _district;
        private long? _upazilla;
        private object _area;
        private object _latitude;
        private object _longitude;
        private object _imagePath;
        private bool _personalDetailsProvided;
        private bool _educationalDetailsProvided;
        private bool _certificateDetailsProvided;
        private bool _specializationDesignationProvided;
        private bool _chamberScheduleAdded;
        private double _profileCompleteness;
        private long _verifiedDoctor;
        private long _bmdcCertNoVerified;
        private long _bmdcCertImgVerified;



        public object QualificationTitle { get { return _qualificationTitle; } set { _qualificationTitle = value; RaisePropertyChanged(); } }
        public string BmdcCertNo { get { return _bmdcCertNo; } set { _bmdcCertNo = value; RaisePropertyChanged(); } }
        public object BmdcCertImg { get { return _bmdcCertImg; } set { _bmdcCertImg = value; RaisePropertyChanged(); } }
        public object Designation { get { return _designation; } set { _designation = value; RaisePropertyChanged(); } }
        public object EducationalInfo { get { return _educationalInfo; } set { _educationalInfo = value; RaisePropertyChanged(); } }
        public object Specialities { get { return _specialities; } set { _specialities = value; RaisePropertyChanged(); } }
        public object NationalId { get { return _nationalId; } set { _nationalId = value; RaisePropertyChanged(); } }
        public object MembershipNumber { get { return _membershipNumber; } set { _membershipNumber = value; RaisePropertyChanged(); } }
        public object Email { get { return _email; } set { _email = value; RaisePropertyChanged(); } }
        public string Mobile { get { return _mobile; } set { _mobile = value; RaisePropertyChanged(); } }
        public string Mobile2 { get { return _mobile2; } set { _mobile2 = value; RaisePropertyChanged(); } }
        public string Address { get { return _address; } set { _address = value; RaisePropertyChanged(); } }
        public long Division { get { return _division; } set { _division = value; RaisePropertyChanged(); } }
        public long District { get { return _district; } set { _district = value; RaisePropertyChanged(); } }
        public long? Upazilla { get { return _upazilla; } set { _upazilla = value; RaisePropertyChanged(); } }
        public object Area { get { return _area; } set { _area = value; RaisePropertyChanged(); } }
        public object Latitude { get { return _latitude; } set { _latitude = value; RaisePropertyChanged(); } }
        public object Longitude { get { return _longitude; } set { _longitude = value; RaisePropertyChanged(); } }
        public object ImagePath { get { return _imagePath; } set { _imagePath = value; RaisePropertyChanged(); } }
        public bool PersonalDetailsProvided { get { return _personalDetailsProvided; } set { _personalDetailsProvided = value; RaisePropertyChanged(); RaiseProfileDetailsChangedEvent(); } }
        public bool EducationalDetailsProvided { get { return _educationalDetailsProvided; } set { _educationalDetailsProvided = value; RaisePropertyChanged(); RaiseProfileDetailsChangedEvent(); } }
        public bool CertificateDetailsProvided { get { return _certificateDetailsProvided; } set { _certificateDetailsProvided = value; RaisePropertyChanged(); RaiseProfileDetailsChangedEvent(); } }
        public bool SpecializationDesignationProvided { get { return _specializationDesignationProvided; } set { _specializationDesignationProvided = value; RaisePropertyChanged(); RaiseProfileDetailsChangedEvent(); } }
        public bool ChamberScheduleAdded { get { return _chamberScheduleAdded; } set { _chamberScheduleAdded = value; RaisePropertyChanged(); } }
        public double ProfileCompleteness { get { return _profileCompleteness; } set { _profileCompleteness = value; RaisePropertyChanged(); } }
        public long VerifiedDoctor { get { return _verifiedDoctor; } set { _verifiedDoctor = value; RaisePropertyChanged(); } }
        public long BmdcCertNoVerified { get { return _bmdcCertNoVerified; } set { _bmdcCertNoVerified = value; RaisePropertyChanged(); } }
        public long BmdcCertImgVerified { get { return _bmdcCertImgVerified; } set { _bmdcCertImgVerified = value; RaisePropertyChanged(); } }



        public event EventHandler<ProfileDetailsChangedEventArgs> ProfileDetailsChanged;

        private void RaiseProfileDetailsChangedEvent([CallerMemberName]string name = null)
        {
            ProfileDetailsChanged?.Invoke(this, new ProfileDetailsChangedEventArgs() { PropertyName = name});
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }



    public class ProfileDetailsChangedEventArgs: EventArgs
    {
        public string PropertyName;
        public bool provided;
    }

}
