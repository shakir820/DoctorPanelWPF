﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class PrescriptionModel: INotifyPropertyChanged
    {
        public PrescriptionModel()
        {
            ExaminedDetails = new PatientExaminationModel();
        }

        public string _patientName;
        public int _age;
        public Gender _sex;
        public DateTime _date;
        public PatientExaminationModel _examinedDetails;



        public string PatientName
        {
            get { return _patientName; }
            set { _patientName = value; RaisePropertyChanged(); }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; RaisePropertyChanged(); }
        }

        public Gender Sex
        {
            get { return _sex; }
            set { _sex = value; RaisePropertyChanged(); }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; RaisePropertyChanged(); }
        }

        public PatientExaminationModel ExaminedDetails
        {
            get { return _examinedDetails; }
            set { _examinedDetails = value; RaisePropertyChanged(); }
        }

        public ObservableCollection<ChiefComplainModel> ChiefComplains = new ObservableCollection<ChiefComplainModel>();
        public ObservableCollection<ChronicDiseaseModel> ChronicDiseases = new ObservableCollection<ChronicDiseaseModel>();
        public ObservableCollection<PastMedicalModel> PastMedicalsHistory = new ObservableCollection<PastMedicalModel>();   
        public ObservableCollection<AllergyModel> Allergies = new ObservableCollection<AllergyModel>();
        public ObservableCollection<FamilyHistoryModel> FamilyHistory = new ObservableCollection<FamilyHistoryModel>();
        public ObservableCollection<InvestigationModel> Investigations = new ObservableCollection<InvestigationModel>();
        public ObservableCollection<DiagnosisModel> DiagnosisList = new ObservableCollection<DiagnosisModel>();
        public ObservableCollection<MedicineModel> Medicines = new ObservableCollection<MedicineModel>();
        public ObservableCollection<AdviceModel> Advices = new ObservableCollection<AdviceModel>();





        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
