﻿using DoctorPanelWPF.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DoctorPanelWPF.Models
{
    public class PatientModel: INotifyPropertyChanged
    {

        public string _patientName;
        public int _age;
        public Gender _gender;
        public string _mobile;
        public BitmapImage _profileImage;
        private ICommand _viewCommand;
        private ICommand _rxCommand;
        public ObservableCollection<PrescriptionModel> Prescriptions = new ObservableCollection<PrescriptionModel>();
        public ICommand ViewCommand
        {
            get { return _viewCommand; }
            set { _viewCommand = value; RaisePropertyChanged(); }
        }

        public ICommand RXCommand
        {
            get { return _rxCommand; }
            set { _rxCommand = value; RaisePropertyChanged(); }
        }

        public string PatientName
        {
            get { return _patientName; }
            set { _patientName = value; RaisePropertyChanged(); }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; RaisePropertyChanged(); }
        }

        public Gender Gender
        {
            get { return _gender; }
            set { _gender = value; RaisePropertyChanged(); }
        }

        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value;  RaisePropertyChanged(); }
        }

        public BitmapImage ProfileImage
        {
            get { return _profileImage; }
            set { _profileImage = value; RaisePropertyChanged(); }
        }

        public PatientModel()
        {
            ViewCommand = new RelayCommand(ViewPatient);
            RXCommand = new RelayCommand(GoToRXPage);
        }

        private void ViewPatient()
        {

        }

        private void GoToRXPage()
        {
            PrescriptionModel prescriptionModel = new PrescriptionModel();
            Prescriptions.Add(prescriptionModel);
            Dashboard.Current.GoToRXPage(prescriptionModel);
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
