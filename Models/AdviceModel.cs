﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class AdviceModel: INotifyPropertyChanged
    {
        public AdviceModel()
        {
            DeleteCommand = new RelayCommand(Delete);
        }

        private string name;
        private string note;
        private int count;

        public int Count { get { if (Prescription != null) { return Prescription.Advices.Count; } else return 0; } }
        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public string Note { get { return note; } set { note = value; RaisePropertyChanged(); } }

        private PrescriptionModel prescriptionModel;

        public PrescriptionModel Prescription
        {
            get { return prescriptionModel; }
            set { prescriptionModel = value; RaisePropertyChanged(); }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get { return _deleteCommand; }
            set { _deleteCommand = value; RaisePropertyChanged(); }
        }

        private void Delete()
        {
            if (Prescription != null)
            {
                Prescription.Advices.Remove(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
