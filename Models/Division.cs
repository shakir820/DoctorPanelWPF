﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public enum Division
    {
        Borishal,
        Dhaka,
        Chattagram,
        Khulna,
        Mymensingh,
        Rajshahi,
        Sylhet,
        Rangpur
    }
}
