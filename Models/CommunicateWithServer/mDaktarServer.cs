﻿using DoctorPanelWPF.Models.API_Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models.CommunicateWithServer
{
    public static class mDaktarServer
    {
        

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        public static async Task<bool> UpdateProfileData(string uri, FormUrlEncodedContent content)
        {
            if (CheckForInternetConnection())
            {
                try
                {
                    HttpResponseMessage response = await App.client.PostAsync(uri, content);
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        // Now update ViewModel data
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
                catch
                {
                    //cannot communicate with server. It may have many reasons.
                    return false;
                }
            }
            else
            {
                ShowInternetErrorNotification();
                return false;
            }
        }



        public static async Task<bool> HitApiAsync(string uri)
        {
            StringContent a = new StringContent("");

            if (CheckForInternetConnection())
            {
                try
                {
                    HttpResponseMessage response = await App.client.PostAsync(uri, a);
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        // Now update ViewModel data
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
                catch
                {
                    //cannot communicate with server. It may have many reasons.
                    return false;
                }
            }
            else
            {
                ShowInternetErrorNotification();
                return false;
            }
        }

        private static void ShowInternetErrorNotification()
        {
            NotificationWindow.Notification notification = new NotificationWindow.Notification();
            notification.Show();
        }

        public static async Task<ObservableCollection<API_Data.Division>> GetDivisionAsync()
        {
            StringContent a = new StringContent("");
            string uri = "http://api.healthsheba.com/api/v2/location/divisions";
            Welcome welcome = null;
            if (CheckForInternetConnection())
            {
                try
                {
                    HttpResponseMessage response = await App.client.GetAsync(uri);
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        // now serialize data
                        var stringContent = await response.Content.ReadAsStringAsync();
                        welcome = Welcome.FromJson(stringContent);

                        if (welcome.Status == "success")
                        {
                            return new ObservableCollection<API_Data.Division>(welcome.Divisions);
                        }
                        else return null;
                    }
                    catch
                    {
                        return null;
                    }
                }
                catch
                {
                    //cannot communicate with server. It may have many reasons.
                    return null;
                }
            }
            else
            {
                ShowInternetErrorNotification();
                return null;
            }
        }

        public static async Task<ObservableCollection<API_Data.District>> GetDistricAsync(long id)
        {
            StringContent a = new StringContent("");
            string uri = "http://api.healthsheba.com/api/v2/location/divisions/"+ id + "/districts";
            Welcome welcome = null;
            if (CheckForInternetConnection())
            {
                try
                {
                    HttpResponseMessage response = await App.client.GetAsync(uri);
                    try
                    {
                        response.EnsureSuccessStatusCode();
                        // now serialize data
                        var stringContent = await response.Content.ReadAsStringAsync();
                        welcome = Welcome.FromJson(stringContent);

                        if (welcome.Status == "success")
                        {
                            return new ObservableCollection<API_Data.District>(welcome.Districts);
                        }
                        else return null;
                    }
                    catch
                    {
                        return null;
                    }
                }
                catch
                {
                    //cannot communicate with server. It may have many reasons.
                    return null;
                }
            }
            else
            {
                ShowInternetErrorNotification();
                return null;
            }
        }
    }
}
