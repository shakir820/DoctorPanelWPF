﻿using DoctorPanelWPF.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DoctorPanelWPF.Models
{
    public class ChamberModel : INotifyPropertyChanged
    {
        #region General Details
        public string _name;
        public string _address;
        public DateTime? _openingTime;
        public DateTime? _closingTime;
        public bool _pending;
        public string _division;
        public string _district;
        public string _area;
        public string _description;
        public Uri _coverImage;
        public ChamberType _chamberType;


        public string Name { get { return _name; } set { _name = value; RaisePropertyChanged(); } }
        public string Address { get { return _address; } set { _address = value; RaisePropertyChanged(); } }
        public DateTime? OpeningTime { get { return _openingTime; } set { _openingTime = value; RaisePropertyChanged(); } }
        public DateTime? ClosingTime { get { return _closingTime; } set { _closingTime = value; RaisePropertyChanged(); } }
        public bool Pending { get { return _pending; } set { _pending = value; RaisePropertyChanged(); } }
        public string Description { get { return _description; } set { _description = value; RaisePropertyChanged(); } }
        public Uri CoverImage { get { return _coverImage; } set { _coverImage = value; RaisePropertyChanged(); } }
        public ChamberType Chamber_Type { get { return _chamberType; } set { _chamberType = value; RaisePropertyChanged(); } }
        public string Division { get { return _division; } set { _division = value; RaisePropertyChanged(); } }
        public string District { get { return _district; } set { _district = value; RaisePropertyChanged(); } }
        public string Area { get { return _area; } set { _area = value; RaisePropertyChanged(); } }

        public ObservableCollection<ScheduleModel> ChamberSchedules = new ObservableCollection<ScheduleModel>();


        public ObservableCollection<PhoneNumberModel> PersonalNumbers;
        public ObservableCollection<PhoneNumberModel> OfficeNumbers;
        public ObservableCollection<PhoneNumberModel> SerialNumbers;

        public string PhoneNumber { get { return GetAnyNumber(); } }
        #endregion


        #region Fees Details
        public int _newVisitPrice;
        public int _returnVisitPrice;
        public int _reportVisitPrice;

        public int NewVisitPrice { get { return _newVisitPrice; } set { _newVisitPrice = value; RaisePropertyChanged(); } }
        public int ReturnVisitPrice { get { return _returnVisitPrice; } set { _returnVisitPrice = value; RaisePropertyChanged(); } }
        public int ReportVisitPrice { get { return _reportVisitPrice; } set { _reportVisitPrice = value; RaisePropertyChanged(); } }
        #endregion


        public ChamberModel()
        {
            Delete = new RelayCommand(DeleteChamber);
            View = new RelayCommand(ViewChamber);
            Edit = new RelayCommand(EditChamber);

            PersonalNumbers = new ObservableCollection<PhoneNumberModel>();
            PersonalNumbers.Add(new PhoneNumberModel() { Count = 1, NumberType = PhoneNumberType.PersonalNumber, Numbers = this.PersonalNumbers });
            OfficeNumbers = new ObservableCollection<PhoneNumberModel>();
            OfficeNumbers.Add(new PhoneNumberModel() { Count = 1, NumberType = PhoneNumberType.OfficeNumber, Numbers = this.OfficeNumbers });
            SerialNumbers = new ObservableCollection<PhoneNumberModel>();
            SerialNumbers.Add(new PhoneNumberModel() { Count = 1, NumberType = PhoneNumberType.SerialNumber, Numbers = this.SerialNumbers });
        }

        private ICommand _editCommand;
        private ICommand _viewCommand;
        private ICommand _deleteCommand;

        public ICommand Delete { get { return _deleteCommand; } set { _deleteCommand = value; RaisePropertyChanged(); } }
        public ICommand Edit { get { return _editCommand; } set { _editCommand = value; RaisePropertyChanged(); } }
        public ICommand View { get { return _viewCommand; } set { _viewCommand = value; RaisePropertyChanged(); } }

        private string GetAnyNumber()
        {
            foreach (var num in PersonalNumbers)
            {
                if (!string.IsNullOrEmpty(num.PhoneNumber))
                {
                    return num.PhoneNumber;
                }
            }

            foreach (var num in OfficeNumbers)
            {
                if (!string.IsNullOrEmpty(num.PhoneNumber))
                {
                    return num.PhoneNumber;
                }
            }

            foreach (var num in SerialNumbers)
            {
                if (!string.IsNullOrEmpty(num.PhoneNumber))
                {
                    return num.PhoneNumber;
                }
            }

            return "";
        }


        private void EditChamber()
        {
            Dashboard.Current.GotoChamberEditPage(this);
        }

        public void ViewChamber()
        {
            Dashboard.Current.GotoChamberDetailsPage(this);
        }

        private void DeleteChamber()
        {
            ChambersViewModel.Current.Chambers.Remove(this);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
