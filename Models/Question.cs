﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class Question: INotifyPropertyChanged
    {
        public string   _title;
        public string   _detail;
        public string   _date;
        public int      _age;
        public Gender   _gender;
        public string   _questionedBy;

        public string   Title { get { return _title; } set { _title = value; RaisePropertyChanged(); } }
        public string   Detail { get { return _detail; } set { _detail = value; RaisePropertyChanged(); } }
        public string   Date { get { return _date; } set { _date = value; RaisePropertyChanged(); } }
        public int      Age { get { return _age; } set { _age = value; RaisePropertyChanged(); } }
        public Gender   Gender { get { return _gender; } set { _gender = value; RaisePropertyChanged(); } }
        public string   QuestionedBy { get { return _questionedBy; } set { _questionedBy = value; RaisePropertyChanged(); } }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
