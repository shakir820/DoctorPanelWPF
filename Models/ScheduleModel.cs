﻿using DoctorPanelWPF.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class ScheduleModel: INotifyPropertyChanged
    {
        public static ScheduleModel LatestInstance;

        private DateTime? _startDate;
        private DateTime? _endDate;
        private TimeSpan _timeSpendPerPatient;
        private DateTime? _chamberOpeningTime = null;
        private DateTime? _chamberClosingTime = null;
        private bool _chamberTimeForEveryday;
        public bool UserDefined = false;
        public bool ClosingTimeSetByDefault = true;
        public bool OpeningTimeSetByDefault = true;
        public bool EverydayDefinedTime = true;
        public bool SpecificDaysDefinedTime = false;
        public TimeSpan DefinedTimePerPatient = TimeSpan.FromMinutes(5);
        public int VisitPatientPerTimeSlot = 1;
        public int _count;
        private ICommand _resheduleCommand;
        private ICommand _closeTimeDropDownCloseCommand;
        private ICommand _openTimeDropDownCloseCommand;


        public event EventHandler<WeekDayCountChangedEventArgs> WeekDayCollectionChanged;
        public ChamberModel Chamber;
        public int Count { get { return _count; } set { _count = value; RaisePropertyChanged(); } }
        public DateTime? StartDate { get { return _startDate; } set { _startDate = value; RaisePropertyChanged(); } }
        public DateTime? EndDate { get { return _endDate; } set { _endDate = value; RaisePropertyChanged(); } }
        public TimeSpan TimeSpendPerPatient { get { return _timeSpendPerPatient; } set { _timeSpendPerPatient = value; RaisePropertyChanged(); } }
        public bool ChamberTimeForEveryday { get { return _chamberTimeForEveryday; } set { _chamberTimeForEveryday = value; RaisePropertyChanged(); } }

        public DateTime? ChamberOpeningTime
        {
            get { return _chamberOpeningTime; }
            set
            {
                if(_chamberOpeningTime != value)
                {
                    _chamberOpeningTime = value;
                    if (value != null)
                    {
                        OpeningTimeSetByDefault = false;
                        //ValidateTime();
                    }
                    else
                    {
                        OpeningTimeSetByDefault = true;
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public DateTime? ChamberClosingTime
        {
            get { return _chamberClosingTime; }
            set
            {
                if(_chamberClosingTime != value)
                {
                    _chamberClosingTime = value;
                    if (value != null)
                    {
                        ClosingTimeSetByDefault = false;
                        //ValidateTime();
                    }
                    else
                    {
                        ClosingTimeSetByDefault = true;
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public ICommand CloseTimeDropDownCloseCommand
        {
            get { return _closeTimeDropDownCloseCommand; }
            set { _closeTimeDropDownCloseCommand = value; RaisePropertyChanged(); }
        }

        public ICommand OpenTimeDropDownCloseCommand
        {
            get { return _openTimeDropDownCloseCommand; }
            set { _openTimeDropDownCloseCommand = value; RaisePropertyChanged(); }
        }

        public ICommand ResheduleCommand { get { return _resheduleCommand; } set { _resheduleCommand = value; RaisePropertyChanged(); } }

        public ObservableCollection<WeekDayModel> WeekDaysCollection = new ObservableCollection<WeekDayModel>();

        public ScheduleModel()
        {
            LatestInstance = this;
            WeekDaysCollection.Add(new WeekDayModel("Saturday", this));
            WeekDaysCollection.Add(new WeekDayModel("Sunday", this));
            WeekDaysCollection.Add(new WeekDayModel("Monday", this));
            WeekDaysCollection.Add(new WeekDayModel("Tuesday", this));
            WeekDaysCollection.Add(new WeekDayModel("Wednesday", this));
            WeekDaysCollection.Add(new WeekDayModel("Thursday", this));
            WeekDaysCollection.Add(new WeekDayModel("Friday", this));

            //define event
            WeekDaysCollection[0].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[1].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[2].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[3].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[4].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[5].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;
            WeekDaysCollection[6].WeekDayTimeSlotCountChanged += ScheduleModel_WeekDayTimeSlotCountChanged;


            ResheduleCommand = new RelayCommand(GoToScheduleEditPage);
            OpenTimeDropDownCloseCommand = new RelayCommand(OpeningCloseMethod);
            CloseTimeDropDownCloseCommand = new RelayCommand(ClosingCloseMethod);
        }

        private void ScheduleModel_WeekDayTimeSlotCountChanged(object sender, WeekDayCountChangedEventArgs e)
        {
            WeekDayCollectionChanged?.Invoke(this, new WeekDayCountChangedEventArgs() { WeekDay = e.WeekDay, count = e.count });
        }

        private void ClosingCloseMethod()
        {
            if (OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateTime();
            }
        }

        private void OpeningCloseMethod()
        {
            if (OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateTime();
            }
        }

        public ScheduleModel ShallowCopy()
        {
            return (ScheduleModel) this.MemberwiseClone();
        }

        public DateTime? GetDefaultDateAndTime(string type, TimeModel timeModel = null)
        {
            if (timeModel != null)
            {
                if (type == "ForOpening")
                    return ScheduleModel.LatestInstance.ChamberOpeningTime;
                else return ScheduleModel.LatestInstance.ChamberClosingTime;
            }
            else
            {
                var today = DateTime.Now;
                var year = today.Year;
                var month = today.Month;
                var day = today.Day;

                if (type == "ForOpening")
                {
                    return new DateTime(year, month, day, 0, 0, 0);
                }
                else
                {
                    return new DateTime(year, month, day, 23, 0, 0);
                }
            }
        }

        public void DeleteTimeSlot(WeekDayModel wd, TimeModel timeslot)
        {
           var day =  WeekDaysCollection.Single(a => a == wd);
            day.DeleteTimeSlot(timeslot);
        }

        private void GoToScheduleEditPage()
        {
            Dashboard.Current.GoToScheduleEditPage(this, Chamber);
        }

        public void UpdateOpeningTime(DateTime dateTime)
        {
            ChamberOpeningTime = dateTime;
            OpeningTimeSetByDefault = false;

            if(OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateTime();
            }
        }

        public void UpdateClosingTime(DateTime dateTime)
        {
            ChamberClosingTime = dateTime;
            ClosingTimeSetByDefault = false;

            if (OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateTime();
            }
        }

        private void ValidateTime()
        {
            if(OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                if (((DateTime)ChamberClosingTime).CompareTo((DateTime)ChamberOpeningTime) <= 0)
                {
                    //invalid time
                     MainWindow.Current.ShowInvalidTimeDialog();
                    ChamberOpeningTime = null;
                    ChamberClosingTime = null;
                    UserDefined = false;
                    return;
                }
                else UserDefined = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
