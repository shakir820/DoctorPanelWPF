﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public enum TemperatureUnit
    {
        Celsius,
        Fahrenheit
    }
}
