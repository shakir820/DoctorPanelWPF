﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class PhoneNumberModel : INotifyPropertyChanged
    {
        private int             _count              = 1;
        private string          _phoneNumber        = "";
        private PhoneNumberType _phoneNumberType;
        private ICommand _addCommand;
        private ICommand _removeCommand;

        public ObservableCollection<PhoneNumberModel> Numbers;

        public PhoneNumberModel()
        {
            AddCommand = new RelayCommand(AddMoreNumber);
            RemoveCommand = new RelayCommand(RemoveThisNumber);
        }

        public ICommand AddCommand
        {
            get { return _addCommand; }
            set { _addCommand = value; RaisePropertyChanged(); }
        }

        public ICommand RemoveCommand
        {
            get { return _removeCommand; }
            set { _removeCommand = value; RaisePropertyChanged(); }
        }

        private void RemoveThisNumber()
        {
            Numbers.Remove(this);
        }

        private void AddMoreNumber()
        {
            Numbers.Add(new PhoneNumberModel() { Count = this.Count + 1 , Numbers = this.Numbers });
        }

        public PhoneNumberType NumberType
        {
            get { return _phoneNumberType; }
            set { _phoneNumberType = value; RaisePropertyChanged(); }
        }

        public int Count
        {
            get { return _count; }
            set { _count = value; RaisePropertyChanged(); }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; RaisePropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
