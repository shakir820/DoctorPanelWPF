﻿using DoctorPanelWPF.ContentDialogues;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class Specialty: INotifyPropertyChanged
    {
        public int _id;
        public string _name;

        public int Id { get { return _id; } set { _id = value; RaisePropertyChanged(); } }
        public string Name { get { return _name; } set { _name = value; RaisePropertyChanged(); } }
        public ICommand DeleteCommand { get; }

        public Specialty()
        {
            DeleteCommand = new RelayCommand(DeleteSpecialty);
        }

        private void DeleteSpecialty()
        {
            if (AddNewSpecialtyDialogue.Current != null)
            {
                AddNewSpecialtyDialogue.Current.SelectedItems.Remove(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
