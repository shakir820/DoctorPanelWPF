﻿using DoctorPanelWPF.Views.ChamberViews.SubViewPages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DoctorPanelWPF.Helpers;
using DoctorPanelWPF.CustomControls;

namespace DoctorPanelWPF.Models
{
    public class TimeModel: INotifyPropertyChanged
    {
        #region Private field
        private int _count;
        private DateTime? _openingTime = null;
        private DateTime? _closingTime = null;
        private ICommand _CancelCommand;
        private WeekDayModel _weekDay;
        private DateTime? previousOpeningTime;
        private DateTime? previousClosingTime;
        private bool _validTime;
        private ICommand _closeTimeDropDownCloseCommand;
        private ICommand _openTimeDropDownCloseCommand;
        #endregion



        #region Public fields
        public bool UserDefined = false;
        public bool Default_Initialized = false;
        public ScheduleModel Schedule;
        public bool ClosingTimeSetByDefault = true;
        public bool OpeningTimeSetByDefault = true;
        #endregion


        #region Properties
        public bool ValidTime
        {
            get { return _validTime; }
            set
            {
                _validTime = value;
                RaisePropertyChanged();
            }
        }

        public ICommand CloseTimeDropDownCloseCommand
        {
            get { return _closeTimeDropDownCloseCommand; }
            set { _closeTimeDropDownCloseCommand = value; RaisePropertyChanged(); }
        }

        public ICommand OpenTimeDropDownCloseCommand
        {
            get { return _openTimeDropDownCloseCommand; }
            set { _openTimeDropDownCloseCommand = value; RaisePropertyChanged(); }
        }

        public int Count { get { return _count; } set { _count = value; RaisePropertyChanged(); } }

        public DateTime? OpeningTime
        {
            get { return _openingTime; }
            set
            {
                if(_openingTime != value)
                {
                    _openingTime = value;
                    if(value != null)
                    {
                        OpeningTimeSetByDefault = false;
                        //ValidateDefinedTime();
                    }
                    else
                    {
                        OpeningTimeSetByDefault = true;
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public DateTime? ClosingTime
        {
            get { return _closingTime; }
            set
            {
                if(_closingTime != value)
                {
                    _closingTime = value;
                    if(value != null)
                    {
                        ClosingTimeSetByDefault = false;
                        //ValidateDefinedTime();
                    }
                    else
                    {
                        ClosingTimeSetByDefault = true;
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public ICommand Cancelcmd { get { return _CancelCommand; } set { _CancelCommand = value; RaisePropertyChanged(); } }
        public string WeekDayName { get; set; }
        public WeekDayModel WeekDay { get { return _weekDay; } set { _weekDay = value; RaisePropertyChanged(); } }
       
        #endregion

        public TimeModel(ScheduleModel scheduleModel, WeekDayModel weekDayModel, string weekdayname)
        {
            WeekDay = weekDayModel;
            WeekDayName = weekdayname;
            Schedule = ScheduleModel.LatestInstance;
            Cancelcmd = new RelayCommand(CancelFunc);
            OpenTimeDropDownCloseCommand = new RelayCommand(OpeningCloseMethod);
            CloseTimeDropDownCloseCommand = new RelayCommand(ClosingCloseMethod);
        }

        public TimeModel(bool defaultInitialized, WeekDayModel weekDayModel, string weekdayname)
        {
            Default_Initialized = defaultInitialized;
            WeekDay = weekDayModel;
            WeekDayName = weekdayname;
            Schedule = ScheduleModel.LatestInstance;
            Cancelcmd = new RelayCommand(CancelFunc);
            OpenTimeDropDownCloseCommand = new RelayCommand(OpeningCloseMethod);
            CloseTimeDropDownCloseCommand = new RelayCommand(ClosingCloseMethod);
        }


        private void ClosingCloseMethod()
        {
            if (OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateDefinedTime();
            }
        }

        private void OpeningCloseMethod()
        {
            if (OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                UserDefined = true;
                ValidateDefinedTime();
            }
        }


        //we may need to update this function. As because it may throw an exception
        public void CancelFunc()
        {
            Schedule.DeleteTimeSlot(WeekDay, this);
            //try
            //{
            //    CreateNewSchedulePage.Current.EnableButtons(WeekDay);
            //}
            //catch
            //{

            //}
   
        }

        private void GetOpeningAndClosingTime()
        {
            //if(Default_Initialised  != true)
            //{
            //    OpeningTime = DateTimeValidationHelper.GetOpeningTime(this, WeekDay);
            //    ClosingTime = DateTimeValidationHelper.GetClosingTime(this, WeekDay);
            //}         
        }


        public void ChangeBoundaryOpeningTime(DateTime openingTime)
        {
            OpeningTimeSetByDefault = true;
            OpeningTime = openingTime;
        }

        public void ChangeBoundaryClosingTime(DateTime closingTime)
        {
            ClosingTimeSetByDefault = true;
            ClosingTime = closingTime;  
        }



        private void ValidateDefinedTime()
        {
            if(OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
            {
                //check for this instance
                if (((DateTime)ClosingTime).CompareTo((DateTime)OpeningTime) <= 0)
                {
                    //invalid time
                    MainWindow.Current.ShowInvalidTimeDialog();
                    OpeningTime = null;
                    ClosingTime = null;

                    //OpeningTimeSetByDefault = true;
                    //ClosingTimeSetByDefault = true;
                    UserDefined = false;
                    ValidTime = false;

                    return;
                }
                else
                {
                    //time is valid for this instance. now check for this weekday
                    if (WeekDay.TimeSlots.Count > 1)
                    {
                        foreach (var time in WeekDay.TimeSlots)
                        {
                            if (time != this)
                            {
                                if (time.UserDefined == true)
                                {
                                    if (((DateTime)time.ClosingTime).CompareTo((DateTime)OpeningTime) > 0)
                                    {
                                        if (((DateTime)time.OpeningTime).CompareTo((DateTime)ClosingTime) >= 0)
                                        {
                                            UserDefined = true;
                                        }
                                        else
                                        {
                                            //invalid time
                                            MainWindow.Current.ShowInvalidTimeDialog();
                                            OpeningTime = null;
                                            ClosingTime = null;
                                            OpeningTimeSetByDefault = true;
                                            ClosingTimeSetByDefault = true;
                                            UserDefined = false;
                                            ValidTime = false;
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        //valid time
                                        ValidTime = true;
                                        UserDefined = true;
                                    }
                                }
                                else continue;
                            }
                        }
                    }
                    if (Schedule.OpeningTimeSetByDefault == false && ClosingTimeSetByDefault == false)
                    {
                        if (((DateTime)Schedule.ChamberOpeningTime).CompareTo((DateTime)OpeningTime) <= 0 && ((DateTime)Schedule.ChamberClosingTime).CompareTo((DateTime)ClosingTime) >= 0)
                        {
                            //valid time
                            ValidTime = true;
                            UserDefined = true;
                        }
                        else
                        {
                            MainWindow.Current.ShowInvalidTimeDialog();
                            OpeningTime = null;
                            ClosingTime = null;
                            OpeningTimeSetByDefault = true;
                            ClosingTimeSetByDefault = true;
                            UserDefined = false;
                            ValidTime = false;
                            return;
                        }
                    }
                    else
                    {
                        MainWindow.Current.ShowInvalidTimeDialog();
                        OpeningTime = null;
                        ClosingTime = null;
                        OpeningTimeSetByDefault = true;
                        ClosingTimeSetByDefault = true;
                        UserDefined = false;
                        ValidTime = false;
                        return;
                    }
                }
            }
          
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
