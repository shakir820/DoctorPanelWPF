﻿using DoctorPanelWPF.ContentDialogues;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class Degree : INotifyPropertyChanged
    {
        private int _id;
        private string _title;
        private string _instituteName;
        private string _subjectName;
        private string _passingYear;
        private int _itemCount;

       

        public string Title { get { return _title; } set { _title = value; RaisePropertyChanged(); } }
        public string InstituteName { get { return _instituteName; } set { _instituteName = value; RaisePropertyChanged(); } }
        public string SubjectName { get { return _subjectName; } set { _subjectName = value; RaisePropertyChanged(); } }
        public string PassingYear { get { return _passingYear; } set { _passingYear = value; RaisePropertyChanged(); } }
        public int Id { get { return _id; } set { _id = value; } }
        public int ItemCount { get { return _itemCount; } set { _itemCount = value; RaisePropertyChanged(); } }

        public Degree()
        {
            SomeCommand = new RelayCommand(DeleteDegree);
        }

        public ICommand SomeCommand { get; }

      
        private void DeleteDegree()
        {
            if (AddNewEducationDialog.Current != null)
            {
                AddNewEducationDialog.Current.degrees.Remove(this);
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
