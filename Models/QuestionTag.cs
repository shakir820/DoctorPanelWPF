﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace DoctorPanelWPF.Models
{
    public class QuestionTag: INotifyPropertyChanged
    {
        private string _name;
        private bool _isChecked = false;

        public string Name { get { return _name; } set { _name = value; RaisePropertyChanged(); } }
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                Application.Current.Dispatcher.Invoke(new Action(() => { RaisePropertyChanged(); }));
            }
        }
               
        public ICommand SomeCommand { get; }

        public QuestionTag()
        {
            SomeCommand = new RelayCommand(CheckItem);
        }


        private void CheckItem()
        {
            if (IsChecked != true)
            {
                IsChecked = true;
            }
            else
            {
                IsChecked = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class QuestionTags
    {
        public ObservableCollection<QuestionTag> QuestionPreferences = new ObservableCollection<QuestionTag>();
        public static QuestionTags Current;

        public QuestionTags()
        {
            Current = this;
            GenerateSomeData();
        }

        private void GenerateSomeData()
        {
            QuestionPreferences.Add(new QuestionTag() { Name = "THD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "MHD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "DFD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "FVD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "TYD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "GUD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "CPD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "RUD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "DPD", IsChecked = false });
            QuestionPreferences.Add(new QuestionTag() { Name = "WPPD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "GGTD", IsChecked = false });
        }
    }
}
