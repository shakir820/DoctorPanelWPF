﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DoctorPanelWPF.Models
{
    public class PastMedicalModel: INotifyPropertyChanged
    {
        public PastMedicalModel()
        {
            DeleteCommand = new RelayCommand(Delete);
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }
        private PrescriptionModel prescriptionModel;

        public PrescriptionModel Prescription
        {
            get { return prescriptionModel; }
            set { prescriptionModel = value; RaisePropertyChanged(); }
        }

        private double _duration;
        public double Duration { get { return _duration; } set { _duration = value; RaisePropertyChanged(); } }

        private DurationType _durationUnit;
        public DurationType DurationUnit { get { return _durationUnit; } set { _durationUnit = value; RaisePropertyChanged(); } }

        private string _note;
        public string Note { get { return _note; } set { _note = value; RaisePropertyChanged(); } }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get { return _deleteCommand; }
            set { _deleteCommand = value; RaisePropertyChanged(); }
        }

        private void Delete()
        {
            if (Prescription != null)
            {
                Prescription.PastMedicalsHistory.Remove(this);
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
