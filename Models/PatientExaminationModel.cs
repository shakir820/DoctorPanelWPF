﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DoctorPanelWPF.Models
{
    public class PatientExaminationModel : INotifyPropertyChanged
    {
        private bool PatientHeightDefined = false;
        private bool PatientWeightDefined = false;
        private int? _heightInCentimeter;
        private int? _heightInFeet;
        private int? _heightInInch;
        private double _temperature;
        private double _patientWeightInKg;
        private double _patientWeightInPound;
        private double _patientWeight;
        private TemperatureUnit _temperatureUnitType;
        private WeightUnit _weightUnitType;
        private int _temperatureInCelsius;
        private int _temperatureInFahrenheit;
        private double _patientBMI;
        private int _patientPulse;
        private int _respiratoryRate;

        public double PatientWeight
        {
            get { return _patientWeight; }
            set
            {
                if(_patientWeight != value)
                {
                    _patientWeight = value;
                    if(WeightUnitType == WeightUnit.Kg)
                    {
                        PatientWeightInKg = value;
                        PatientWeightInPound = ConvertValueFromKgToPound(value);
                    }
                    else
                    {
                        PatientWeightInPound = value;
                        PatientWeightInKg = ConvertValueFromPoundToKg(value);
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public int? HeightInCentimeter
        {
            get { return _heightInCentimeter; }
            set
            {
               if(_heightInCentimeter != value)
                {
                    _heightInCentimeter = value;
                    RaisePropertyChanged();
                    CalculatePatientBMI();
                }
            }
        }

        public int? HeightInFeet
        {
            get { return _heightInFeet; }
            set
            {
                if (_heightInFeet != value)
                {
                    _heightInFeet = value;
                    RaisePropertyChanged();
                }
            }
        }

        public int? HeightInInch
        {
            get { return _heightInInch; }
            set
            {
                if(_heightInInch != value)
                {
                    _heightInInch = value;
                    RaisePropertyChanged();
                }
            }
        }

        
        public double Temperature
        {
            get { return _temperature; }
            set
            {
                if(_temperature != value)
                {
                    _temperature = value;
                    if(TemperatureUnitType == TemperatureUnit.Celsius)
                    {
                        TemperatureInCelsius = (int)value;
                        TemperatureInFahrenheit = (int)ConvertTempFromCelToFah(TemperatureInCelsius);
                    }
                    else
                    {
                        TemperatureInFahrenheit = (int)value;
                        TemperatureInCelsius = (int)ConvertTempFromFahToCel(TemperatureInFahrenheit);
                    }
                    RaisePropertyChanged();
                }
            }
        }

        public int TemperatureInFahrenheit
        {
            get { return _temperatureInFahrenheit; }
            set
            {
                if (_temperatureInFahrenheit != value)
                {
                    _temperatureInFahrenheit = value;
                    RaisePropertyChanged();
                }
            }
        }

        public int TemperatureInCelsius
        {
            get { return _temperatureInCelsius; }
            set
            {
                if(_temperatureInCelsius != value)
                {
                    _temperatureInCelsius = value;
                    RaisePropertyChanged();
                }
            }
        }

        
        public TemperatureUnit TemperatureUnitType
        {
            get { return _temperatureUnitType; }
            set
            {
                if (_temperatureUnitType != value)
                {
                    _temperatureUnitType = value;
                    if (value == TemperatureUnit.Celsius)
                    {
                        if (_temperature != 0)
                        {
                            TemperatureInCelsius = (int)Temperature;
                            TemperatureInFahrenheit = (int)ConvertTempFromCelToFah(Temperature);
                        }
                    }
                    else if (value == TemperatureUnit.Fahrenheit)
                    {
                        if (_temperature != 0)
                        {
                            TemperatureInCelsius = (int)ConvertTempFromFahToCel(Temperature);
                            TemperatureInFahrenheit = (int)Temperature;
                        }
                    }

                    _temperatureUnitType = value;
                    RaisePropertyChanged();
                }
                
            }
        }



        public double PatientWeightInKg
        {
            get { return _patientWeightInKg; }
            set
            {
                if(_patientWeightInKg != value)
                {
                    _patientWeightInKg = value;
                    RaisePropertyChanged();
                    CalculatePatientBMI();
                }
            }
        }

        public double PatientWeightInPound
        {
            get { return _patientWeightInPound; }
            set
            {
                if(_patientWeightInPound != value)
                {
                    _patientWeightInPound = value;
                    RaisePropertyChanged();
                }
               
            }
        }

       
        public WeightUnit WeightUnitType
        {
            get { return _weightUnitType; }
            set
            {
                if (_weightUnitType != value)
                {
                    _weightUnitType = value;
                    if(value == WeightUnit.Kg)
                    {
                        PatientWeightInKg = PatientWeight;
                        PatientWeightInPound = ConvertValueFromKgToPound(PatientWeightInKg);
                    }
                    else
                    {
                        PatientWeightInPound = PatientWeight;
                        PatientWeightInKg = ConvertValueFromPoundToKg(PatientWeightInPound);
                    }
                    RaisePropertyChanged();
                }
            }
        }

       
        public int PatientPulse
        {
            get { return _patientPulse; }
            set { _patientPulse = value; RaisePropertyChanged(); }
        }

       
        public int RespiratoryRate
        {
            get { return _respiratoryRate; }
            set { _respiratoryRate = value; RaisePropertyChanged(); }
        }

        public double PatientBMI
        {
            get { return _patientBMI; }
            set { _patientBMI = value; RaisePropertyChanged(); }
        }

        #region Blood Pressure
        private int _systolic;
        public int Systolic
        {
            get { return _systolic; }
            set { _systolic = value; RaisePropertyChanged(); }
        }

        private int _diastolic;
        public int Diastolic
        {
            get { return _diastolic; }
            set { _diastolic = value; RaisePropertyChanged(); }
        }
        #endregion

        private double ConvertValueFromPoundToKg(double pound)
        {
            return pound / 2.205;
        }

        private double ConvertValueFromKgToPound(double kg)
        {
            return kg * 2.205;
        }

        private double ConvertTempFromFahToCel(double fer)
        {
            return (fer - 32) * 5 / 9;
        }

        private double ConvertTempFromCelToFah(double cel)
        {
            return (cel * 9 / 5) + 32;
        }

       
        

        public void CalCulateFeetAndInch(int cent)
        {
            if(cent != 0)
            {
                int inch = (int)(cent / 2.54);
                double feet = inch / 12;
                string[] parts = feet.ToString().Split('.');
                int f = int.Parse(parts[0]);
                //int  = int.Parse(parts[1]);
                int i = inch % 12;
                HeightInFeet = f;
                HeightInInch = i;
            }
        }

        public void CalCulateCentimeter(int feet, int inch)
        {
            int i = 0;

            if (feet != 0)
            {
                i = feet * 12;
            }

            if(inch != 0)
            {
                i = i + inch;
            }
            int cent =  (int)(i * 2.54);

            HeightInCentimeter = cent;
        }

        private void CalculatePatientBMI()
        {
            if (_patientWeightInKg != 0 && _heightInCentimeter != 0 && _heightInCentimeter != null)
            {
                double meter = ((double)_heightInCentimeter / 100);
                double meter2 = meter * meter;
                PatientBMI = _patientWeightInKg / meter2;
            }
            else PatientBMI = 0;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
