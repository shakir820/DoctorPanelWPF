﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Models
{
    public class ChambersViewModel
    {
        public static ChambersViewModel Current;
        public ObservableCollection<ChamberModel> Chambers = new ObservableCollection<ChamberModel>();

        public ChambersViewModel()
        {
            Current = this;
            GenerateChambers();
        }

        public void GenerateChambers()
        {
            Chambers.Add(new ChamberModel()
            {
                Name = "AEF Chamber",
                Address = "U-17, CAAB Staff Quarter, Dhaka-1229",
                OpeningTime = new DateTime(2004, 5, 12, 5, 00, 00),
                ClosingTime = new DateTime(2004, 5, 12, 10, 00, 00),
                Pending = false
            });


            Chambers.Add(new ChamberModel()
            {
                Name = "AEF Chamber",
                Address = "U-17, CAAB Staff Quarter, Dhaka-1229",
                OpeningTime = new DateTime(2004, 5, 12, 5, 00, 00),
                ClosingTime = new DateTime(2004, 5, 12, 10, 00, 00),
                Pending = true
            });

            Chambers.Add(new ChamberModel()
            {
                Name = "AEF Chamber",
                Address = "U-17, CAAB Staff Quarter, Dhaka-1229",
                OpeningTime = new DateTime(2004, 5, 12, 5, 00, 00),
                ClosingTime = new DateTime(2004, 5, 12, 10, 00, 00),
                Pending = false
            });

            Chambers.Add(new ChamberModel()
            {
                Name = "AEF Chamber",
                Address = "U-17, CAAB Staff Quarter, Dhaka-1229",
                OpeningTime = new DateTime(2004, 5, 12, 5, 00, 00),
                ClosingTime = new DateTime(2004, 5, 12, 10, 00, 00),
                Pending = true
            });

            Chambers.Add(new ChamberModel()
            {
                Name = "AEF Chamber",
                Address = "U-17, CAAB Staff Quarter, Dhaka-1229",
                OpeningTime = new DateTime(2004, 5, 12, 5, 00, 00),
                ClosingTime = new DateTime(2004, 5, 12, 10, 00, 00),
                Pending = false
            });
        }

        //public void AddNewChamberToDatabse(ChamberModel chamber)
        //{
        //    Let me see first if it does work well

        //    DataAccessLibrary.DatabaseHelper.AddChamber(chamber.Name, chamber.Chamber_Type.ToString(),
        //        chamber.Address, chamber.Pending, chamber.OpeningTime, chamber.ClosingTime, chamber.Division, chamber.District,
        //        chamber.Area, chamber.Description, chamber.NewVisitPrice, chamber.ReturnVisitPrice, chamber.ReportVisitPrice);
        //}
    }
}
