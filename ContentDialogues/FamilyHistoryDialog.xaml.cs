﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for FamilyHistoryDialog.xaml
    /// </summary>
    public partial class FamilyHistoryDialog : UserControl
    {
        public FamilyHistoryDialog()
        {
            InitializeComponent();
            GetSuggestions();
            DataContext = this;
        }

        public ObservableCollection<FamilyHistoryModel> SuggestedFamilyHistory = new ObservableCollection<FamilyHistoryModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedFamilyHistory;
            SelectedItemsListView.ItemsSource = Prescription.FamilyHistory;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.FamilyHistory)
            {
                SuggestedFamilyHistory.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new FamilyHistoryModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.FamilyHistory.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.FamilyHistory.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.FamilyHistory.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.FamilyHistory.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.FamilyHistory.Count > 0)
            {
                foreach (var item in Prescription.FamilyHistory)
                {
                    if (SuggestedFamilyHistory.Contains(item))
                    {
                        SuggestedFamilyHistory.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FamilyHistoryModel familyHistory = (FamilyHistoryModel)SuggestionListView.SelectedItem;
            familyHistory.Duration = 0.0;
            familyHistory.DurationUnit = DurationType.Hour;
            familyHistory.Note = "";
            familyHistory.Relation = "";
            familyHistory.Prescription = Prescription;
            Prescription.FamilyHistory.Add(familyHistory);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedFamilyHistory.Remove(familyHistory);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<FamilyHistoryModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedFamilyHistory
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedFamilyHistory;
            }
        }
    }
}
