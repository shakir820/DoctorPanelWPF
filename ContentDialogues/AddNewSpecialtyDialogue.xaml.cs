﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for AddNewSpecialtyDialogue.xaml
    /// </summary>
    public partial class AddNewSpecialtyDialogue : UserControl
    {
        public static AddNewSpecialtyDialogue Current;
        public ObservableCollection<string> items = new ObservableCollection<string>();
        public ObservableCollection<Specialty> SelectedItems = new ObservableCollection<Specialty>();

        public AddNewSpecialtyDialogue()
        {
            InitializeComponent();
            Current = this;
            GenerateComboBoxItems();
        }



        public void GenerateComboBoxItems()
        {
            items.Add("OMG");
            items.Add("GG");
            items.Add("LOL");
            items.Add("FCC");
            items.Add("PICKABO");
            items.Add("LION");
            items.Add("King");
            items.Add("Dominator");
            items.Add("Shut Down");
            items.Add("EZ");
        }

       

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ItemsComboBox.ItemsSource = items;
            SelectedItemsListView.ItemsSource = SelectedItems;
        }

        private void ItemsComboBox_Selected(object sender, RoutedEventArgs e)
        {
            string name = ItemsComboBox.SelectedItem as string;
            SelectedItems.Add(new Specialty() { Name = name });
        }

        private void ItemsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = ItemsComboBox.SelectedItem as string;
            SelectedItems.Add(new Specialty() { Name = name });
        }
    }
}
