﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for InvalidTimeWarningDialog.xaml
    /// </summary>
    public partial class InvalidTimeWarningDialog : UserControl
    {

        //string msg;
        public InvalidTimeWarningDialog()
        {
            InitializeComponent();
            this.Loaded += InvalidTimeWarningDialog_Loaded;
           
            //msg = (string)this.DataContext;
        }

        private void InvalidTimeWarningDialog_Loaded(object sender, RoutedEventArgs e)
        {

            TitleTextBlock.Text = "The time duration you have chosen is not a valid time. Choose a valid time.";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
