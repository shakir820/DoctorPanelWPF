﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for DiagnosisDialog.xaml
    /// </summary>
    public partial class DiagnosisDialog : UserControl
    {
        public DiagnosisDialog()
        {
            InitializeComponent();
            GetSuggestions();
            DataContext = this;
        }


        public ObservableCollection<DiagnosisModel> SuggestedDiagnosis = new ObservableCollection<DiagnosisModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedDiagnosis;
            SelectedItemsListView.ItemsSource = Prescription.DiagnosisList;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.DiagnosisList)
            {
                SuggestedDiagnosis.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new DiagnosisModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.DiagnosisList.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.DiagnosisList.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.DiagnosisList.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.DiagnosisList.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.DiagnosisList.Count > 0)
            {
                foreach (var item in Prescription.DiagnosisList)
                {
                    if (SuggestedDiagnosis.Contains(item))
                    {
                        SuggestedDiagnosis.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DiagnosisModel diagnosis = (DiagnosisModel)SuggestionListView.SelectedItem;
            diagnosis.Note = "";
            diagnosis.Prescription = Prescription;
            Prescription.DiagnosisList.Add(diagnosis);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedDiagnosis.Remove(diagnosis);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<DiagnosisModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedDiagnosis
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedDiagnosis;
            }
        }
    }
}
