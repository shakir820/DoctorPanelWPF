﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DoctorPanelWPF.ContentDialogues.Converters
{
    public class StringToDoubleConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double val = (double)value;
                return val.ToString();
            }
            else return "";
            
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;
            double result;
            if(double.TryParse(val, out result))
            {
                return result;
            }
            else
            {
                return 0.0;
            }
        }
    }


    public class StringToIntegerConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                int val = (int)value;
                return val.ToString();
            }
            else return "";
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;
            int result;
            if (int.TryParse(val, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}
