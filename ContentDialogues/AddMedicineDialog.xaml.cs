﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for AddMedicineDialog.xaml
    /// </summary>
    public partial class AddMedicineDialog : UserControl
    {
        public AddMedicineDialog()
        {
            InitializeComponent();
            GetSuggestions();
            DataContext = this;
        }

        public ObservableCollection<MedicineModel> SuggestedMedicines = new ObservableCollection<MedicineModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedMedicines;
            SelectedItemsListView.ItemsSource = Prescription.Medicines;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.MedicineList)
            {
                SuggestedMedicines.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new MedicineModel() { Name = name, Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap };
                    com.Prescription = Prescription;
                    Prescription.Medicines.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.MedicineList.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.MedicineList.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.MedicineList.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.Medicines.Count > 0)
            {
                foreach (var item in Prescription.Medicines)
                {
                    if (SuggestedMedicines.Contains(item))
                    {
                        SuggestedMedicines.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MedicineModel medicine = (MedicineModel)SuggestionListView.SelectedItem;
            
            medicine.Prescription = Prescription;
            Prescription.Medicines.Add(medicine);
            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedMedicines.Remove(medicine);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<MedicineModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedMedicines
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedMedicines;
            }
        }
    }
}
