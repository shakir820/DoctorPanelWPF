﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for PastMedicalHistoryDialog.xaml
    /// </summary>
    public partial class PastMedicalHistoryDialog : UserControl
    {
        public PastMedicalHistoryDialog()
        {
            InitializeComponent();
            GetSuggestions();
        }



        public ObservableCollection<PastMedicalModel> SuggestedPastMedicalHistory = new ObservableCollection<PastMedicalModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedPastMedicalHistory;
            SelectedItemsListView.ItemsSource = Prescription.PastMedicalsHistory;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.PastMedicalHistory)
            {
                SuggestedPastMedicalHistory.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new PastMedicalModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.PastMedicalsHistory.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.PastMedicalHistory.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.PastMedicalHistory.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.PastMedicalHistory.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.PastMedicalsHistory.Count > 0)
            {
                foreach (var item in Prescription.PastMedicalsHistory)
                {
                    if (SuggestedPastMedicalHistory.Contains(item))
                    {
                        SuggestedPastMedicalHistory.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PastMedicalModel medicalHistory = (PastMedicalModel)SuggestionListView.SelectedItem;
            medicalHistory.Duration = 0.0;
            medicalHistory.DurationUnit = DurationType.Hour;
            medicalHistory.Note = "";
            medicalHistory.Prescription = Prescription;
            Prescription.PastMedicalsHistory.Add(medicalHistory);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedPastMedicalHistory.Remove(medicalHistory);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<PastMedicalModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedPastMedicalHistory
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedPastMedicalHistory;
            }
        }
    }
}
