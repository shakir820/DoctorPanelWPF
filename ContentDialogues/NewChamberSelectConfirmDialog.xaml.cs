﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for NewChamberSelectConfirmDialog.xaml
    /// </summary>
    public partial class NewChamberSelectConfirmDialog : UserControl
    {
        public NewChamberSelectConfirmDialog()
        {
            InitializeComponent();
        }

        private void NoBtn_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void YesBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
