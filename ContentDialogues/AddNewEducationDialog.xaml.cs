﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for AddNewEducationDialog.xaml
    /// </summary>
    public partial class AddNewEducationDialog : UserControl
    {
        public static AddNewEducationDialog Current;

        public ObservableCollection<Degree> degrees = new ObservableCollection<Degree>();

        public AddNewEducationDialog()
        {
            InitializeComponent();
            Current = this;
            GenerateSampleData();
        }


        private void AddMoreBtn_Click(object sender, RoutedEventArgs e)
        {
            degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE", ItemCount = 2 });
        }

        public void GenerateSampleData()
        {
            degrees.Add(new Degree() { Title = "B.Sc.", Id = 5, InstituteName = "Presidency University", PassingYear = "2018, 5, 14", SubjectName = "CSE", ItemCount = 0 });
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            entryLisView.ItemsSource = degrees;
        }

        private void CancelThisBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
