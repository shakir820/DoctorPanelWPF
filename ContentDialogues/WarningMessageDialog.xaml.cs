﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for WarningMessageDialog.xaml
    /// </summary>
    public partial class WarningMessageDialog : UserControl
    {
        public static WarningMessageDialog Current;
        public string msg = "";
        public WarningMessageDialog()
        {
            InitializeComponent();
            Current = this;
            this.Loaded += InvalidTimeWarningDialog_Loaded;
        }

        private void InvalidTimeWarningDialog_Loaded(object sender, RoutedEventArgs e)
        {
            TitleTextBlock.Text = msg;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
