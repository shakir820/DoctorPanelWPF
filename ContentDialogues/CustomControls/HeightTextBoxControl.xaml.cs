﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues.CustomControls
{
    /// <summary>
    /// Interaction logic for HeightTextBoxControl.xaml
    /// </summary>
    public partial class HeightTextBoxControl : UserControl, INotifyPropertyChanged
    {
        public HeightTextBoxControl()
        {
            InitializeComponent();
            //DataContext = this;
        }




        #region boolean fields
        bool InPropertyChangedCallback = false;
        bool InHeightInCentimeter = false;
        bool InFeetAndInch = false;
        bool InCalculateHeightInCentimeter = false;
        #endregion





        private double? _heightInCentimeter;
        public double? HeightInCentimeter
        {
            get { return _heightInCentimeter; }
            set
            {
                if (_heightInCentimeter != value)
                {
                    _heightInCentimeter = value;
                    if (value != null)
                    {
                        CalCulateHeightInFeetAndInch((double)value);
                        PatientHeight = value;
                    }
                    RaisePropertyChanged();
                }
            }
        }



        private int? _feet;
        public int? Feet
        {
            get { return _feet; }
            set
            {
                if(_feet != value)
                {
                    _feet = value;
                  
                    double? cent = CalculateHeightInCentimeter((int)_feet, (int)_inch);
                    _heightInCentimeter = cent;
                    PatientHeight = cent;
                    RaisePropertyChanged("HeightInCentimeter");
                    RaisePropertyChanged();
                }
            }
        }



        private int? _inch;
        public int? Inch
        {
            get { return _inch; }
            set
            {
                if(_inch != value)
                {
                    _inch = value;

                    double? cent = CalculateHeightInCentimeter((int)_feet, (int)_inch);
                    _heightInCentimeter = cent;
                    PatientHeight = cent;
                    RaisePropertyChanged("HeightInCentimeter");
                    RaisePropertyChanged();
                }
            }
        }





        #region dependency property
        public double? PatientHeight
        {
            get { return (double?)GetValue(PatientHeightProperty); }
            set { SetValue(PatientHeightProperty, value);
                //RaisePropertyChanged(); 
            }
        }

        public static DependencyProperty PatientHeightProperty = DependencyProperty.Register(
            "PatientHeight", typeof(double?), typeof(HeightTextBoxControl), new PropertyMetadata(null, new
             PropertyChangedCallback(OnHeightPropertValueChanged)));

        private static void OnHeightPropertValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (HeightTextBoxControl)d;
            if(control.InPropertyChangedCallback == false)
            {
                control.InPropertyChangedCallback = true;
                if (e.NewValue != null)
                {
                    if(control._heightInCentimeter != (double)e.NewValue)
                    {
                        control.PatientHeight = (double)e.NewValue;
                        control._heightInCentimeter = (double)e.NewValue;
                        control.RaisePropertyChanged("HeightInCentimeter");
                        control.CalCulateHeightInFeetAndInch((double)e.NewValue);
                    }                   
                }
                control.InPropertyChangedCallback = false;
            }
        }
        #endregion





        private void CalCulateHeightInFeetAndInch(double heightVAL)
        {
            double centimeter = heightVAL;
            var feet = centimeter / 30.48;
            string s = feet.ToString("0.00", CultureInfo.InvariantCulture);
            string[] parts = s.Split('.');
            int f = int.Parse(parts[0]);
            int p = int.Parse(parts[1]);
            int inch = p * 12;
            _feet = f;
            _inch = inch;
            RaisePropertyChanged("Feet");
            RaisePropertyChanged("Inch");
        }




        private double? CalculateHeightInCentimeter(int feet, int inch)
        {
            string s = feet.ToString() + "." + inch.ToString();
            double val;
            double cent;
            if (double.TryParse(s, out val))
            {
                cent = val * 30.48;
                return cent;
            }
            else return null;
        }








        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
