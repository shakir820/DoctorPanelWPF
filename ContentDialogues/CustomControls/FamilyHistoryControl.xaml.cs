﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues.CustomControls
{
    /// <summary>
    /// Interaction logic for FamilyHistoryControl.xaml
    /// </summary>
    public partial class FamilyHistoryControl : UserControl
    {
        public FamilyHistoryControl()
        {
            InitializeComponent();
        }


        private bool InCallBackMethod = false;
        private bool InSelectionChangedEvent = false;

        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }



        public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register("DeleteCommand",
            typeof(ICommand), typeof(FamilyHistoryControl));

        private DurationType? _durationUnit;
        public DurationType? DurationUnit
        {
            get { return (DurationType)GetValue(DurationUnitProperty); }
            set
            {
                _durationUnit = value;
                SetValue(DurationUnitProperty, value);
            }
        }

        public static readonly DependencyProperty DurationUnitProperty = DependencyProperty.Register(
            "DurationUnit", typeof(DurationType?), typeof(FamilyHistoryControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(DurationUnitChangedCallBack)));

        private static void DurationUnitChangedCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FamilyHistoryControl t = (FamilyHistoryControl)d;
            t.InCallBackMethod = true;
            //t.DurationUnit = (DurationType)e.NewValue;
            if (!t.InSelectionChangedEvent)
            {
                t.DurationUnitComboBox.SelectedIndex = (int)t.DurationUnit;
            }
            t.InCallBackMethod = false;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteCommand != null)
            {
                if (DeleteCommand.CanExecute(null))
                {
                    DeleteCommand.Execute(null);
                }
            }
        }

        private void DurationUnitComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InSelectionChangedEvent = true;
            if (InCallBackMethod != true)
            {

                //var comboBox = (ComboBox)sender;
                string val = (string)((ComboBoxItem)DurationUnitComboBox.SelectedValue).Tag;
                if (!string.IsNullOrEmpty(val))
                {
                    DurationType result;
                    if (Enum.TryParse(val, out result))
                    {
                        DurationUnit = result;
                        if (result == DurationType.LongTime)
                        {
                            DurationTextBox.IsEnabled = false;
                        }
                        else
                        {
                            DurationTextBox.IsEnabled = true;
                        }
                    }
                }
            }
            InSelectionChangedEvent = false;
        }
    }
}
