﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues.CustomControls
{
    /// <summary>
    /// Interaction logic for MedicineItemControl.xaml
    /// </summary>
    public partial class MedicineItemControl : UserControl
    {
        public MedicineItemControl()
        {
            InitializeComponent();
            GenerateDefaultData();
        }

        private ObservableCollection<DrugTypeModel> DrugTypeList = new ObservableCollection<DrugTypeModel>();
        private ObservableCollection<ScheduleFrequencyModel> FrequencyList = new ObservableCollection<ScheduleFrequencyModel>();
        private ObservableCollection<MedicineRouteModel> RouteList = new ObservableCollection<MedicineRouteModel>();
        private ObservableCollection<MedicineDurationModel> DurationList = new ObservableCollection<MedicineDurationModel>();
        private ObservableCollection<CountModel> CountList = new ObservableCollection<CountModel>();


        private void GenerateDefaultData()
        {
            //Drug Type
            DrugTypeList.Add(new DrugTypeModel() { Name = "Tab", Type = DrugType.Tab });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Cap", Type = DrugType.Cap });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Spray", Type = DrugType.Spray });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Suppository", Type = DrugType.Suppository });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Inhaler", Type = DrugType.Inhaler });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Nebulizer", Type = DrugType.Nebulizer });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Ointment", Type = DrugType.Ointment });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Cream", Type = DrugType.Cream });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Eye Drop", Type = DrugType.EyeDrop });
            DrugTypeList.Add(new DrugTypeModel() { Name = "Drop", Type = DrugType.Drop });

            DrugTypeComboBox.ItemsSource = DrugTypeList;

            
            //Frequency Details
            FrequencyList.Add(new ScheduleFrequencyModel() { Name = "Thrice A Day", Frequency = ScheduleFrequency.ThriceADay });
            FrequencyList.Add(new ScheduleFrequencyModel() { Name = "Four Times in a Day", Frequency = ScheduleFrequency.FourTimesInADay });
            FrequencyList.Add(new ScheduleFrequencyModel() { Name = "As Needed", Frequency = ScheduleFrequency.AsNeeded });

            FrquencyComboBox.ItemsSource = FrequencyList;


            //Route Data
            RouteList.Add(new MedicineRouteModel() { Name = "Per Urethal", MedicineRoute = Route.PerUrethal });
            RouteList.Add(new MedicineRouteModel() { Name = "Over The Tongue", MedicineRoute = Route.OverTheTongue });
            RouteList.Add(new MedicineRouteModel() { Name = "Ear", MedicineRoute = Route.Ear });
            RouteList.Add(new MedicineRouteModel() { Name = "Eye", MedicineRoute = Route.Eye });
            RouteList.Add(new MedicineRouteModel() { Name = "Nasal", MedicineRoute = Route.Nasal });
            RouteList.Add(new MedicineRouteModel() { Name = "Inhalation", MedicineRoute = Route.Inhalation });
            RouteList.Add(new MedicineRouteModel() { Name = "Skin", MedicineRoute = Route.Skin });
            RouteList.Add(new MedicineRouteModel() { Name = "Locally", MedicineRoute = Route.Locally });
            RouteList.Add(new MedicineRouteModel() { Name = "Oral", MedicineRoute = Route.Oral });
            RouteList.Add(new MedicineRouteModel() { Name = "I/V", MedicineRoute = Route.IV });
            RouteList.Add(new MedicineRouteModel() { Name = "I/M", MedicineRoute = Route.IM });
            RouteList.Add(new MedicineRouteModel() { Name = "S/C", MedicineRoute = Route.SC });
            RouteList.Add(new MedicineRouteModel() { Name = "Per Rectal", MedicineRoute = Route.PerRectal });
            RouteList.Add(new MedicineRouteModel() { Name = "Intra Dermal", MedicineRoute = Route.IntraDermal });
            RouteList.Add(new MedicineRouteModel() { Name = "Sub Lingual", MedicineRoute = Route.SubLingual });
            RouteList.Add(new MedicineRouteModel() { Name = "Per Veginal", MedicineRoute = Route.PerVeginal });

            RouteComboBox.ItemsSource = RouteList;

            //Duration Data
            DurationList.Add(new MedicineDurationModel() { Name = "Days", Duration = MedicineDurationType.Days });
            DurationList.Add(new MedicineDurationModel() { Name = "Weeks", Duration = MedicineDurationType.Weeks });
            DurationList.Add(new MedicineDurationModel() { Name = "Months", Duration = MedicineDurationType.Months });
            DurationList.Add(new MedicineDurationModel() { Name = "Years", Duration = MedicineDurationType.Years });
            DurationList.Add(new MedicineDurationModel() { Name = "Continue", Duration = MedicineDurationType.Continue });

            DurationTypeComboBox.ItemsSource = DurationList;

            CountList.Add(new CountModel() { Name = "0", Value = 0.0 });
            CountList.Add(new CountModel() { Name = "1", Value = 1.0 });
            CountList.Add(new CountModel() { Name = "2", Value = 2.0 });
            CountList.Add(new CountModel() { Name = "3", Value = 3.0 });
            CountList.Add(new CountModel() { Name = "4", Value = 4.0 });
            CountList.Add(new CountModel() { Name = "5", Value = 5.0 });
            CountList.Add(new CountModel() { Name = "6", Value = 6.0 });
            CountList.Add(new CountModel() { Name = "7", Value = 7.0 });
            CountList.Add(new CountModel() { Name = "8", Value = 8.0 });
            CountList.Add(new CountModel() { Name = "9", Value = 9.0 });
            CountList.Add(new CountModel() { Name = "1/2", Value = 0.5 });
            CountList.Add(new CountModel() { Name = "1/4", Value = 0.25 });
            CountList.Add(new CountModel() { Name = "3/4", Value = 0.75 });


            ThreeTimesOneComboBox.ItemsSource = CountList;
            //ThreeTimesOneComboBox.SelectedIndex = 0;
            ThreeTimesTwoComboBox.ItemsSource = CountList;
            //ThreeTimesTwoComboBox.SelectedIndex = 0;
            ThreeTimesThreeComboBox.ItemsSource = CountList;
            //ThreeTimesThreeComboBox.SelectedIndex = 0;
            FourTimesOneComboBox.ItemsSource = CountList;
            //FourTimesOneComboBox.SelectedIndex = 0;
            FourTimesTwoComboBox.ItemsSource = CountList;
            //FourTimesTwoComboBox.SelectedIndex = 0;
            FourTimesThreeComboBox.ItemsSource = CountList;
            //FourTimesThreeComboBox.SelectedIndex = 0;
            FourTimesFourComboBox.ItemsSource = CountList;
            //FourTimesFourComboBox.SelectedIndex = 0;

            //FrquencyComboBox.SelectedIndex = 0;
        }



        #region Dependency Property
        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }

        public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register("DeleteCommand",
            typeof(ICommand), typeof(MedicineItemControl));






        public double? ThreeTimesOne
        {
            get { return (double?)GetValue(ThreeTimesOneProperty); }
            set { SetValue(ThreeTimesOneProperty, value); }
        }

        public static readonly DependencyProperty ThreeTimesOneProperty = DependencyProperty.Register(
            "ThreeTimesOne", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(ThreeTimesOnePropertyChangedCallback)));

        private static void ThreeTimesOnePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double?)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.ThreeTimesOneComboBox.SelectionChanged -= control.ThreeTimesOneComboBox_SelectionChanged;
                control.ThreeTimesOneComboBox.SelectedValue = control.CountList[0];
                control.ThreeTimesOneComboBox.SelectionChanged += control.ThreeTimesOneComboBox_SelectionChanged;
                return;
            }
            
            control.ThreeTimesOneComboBox.SelectionChanged -= control.ThreeTimesOneComboBox_SelectionChanged;
            control.ThreeTimesOneComboBox.SelectedValue = selectedVal;
            control.ThreeTimesOneComboBox.SelectionChanged += control.ThreeTimesOneComboBox_SelectionChanged;
        }


        public double? ThreeTimesTwo
        {
            get { return (double?)GetValue(ThreeTimesTwoProperty); }
            set { SetValue(ThreeTimesTwoProperty, value); }
        }

        public static readonly DependencyProperty ThreeTimesTwoProperty = DependencyProperty.Register(
            "ThreeTimesTwo", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(ThreeTimesTwoPropertyChangedCallback)));

        private static void ThreeTimesTwoPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.ThreeTimesTwoComboBox.SelectionChanged -= control.ThreeTimesTwoComboBox_SelectionChanged;
                control.ThreeTimesTwoComboBox.SelectedValue = control.CountList[0];
                control.ThreeTimesTwoComboBox.SelectionChanged += control.ThreeTimesTwoComboBox_SelectionChanged;
                return;
            }
            
            control.ThreeTimesTwoComboBox.SelectionChanged -= control.ThreeTimesTwoComboBox_SelectionChanged;
            control.ThreeTimesTwoComboBox.SelectedValue = selectedVal;
            control.ThreeTimesTwoComboBox.SelectionChanged += control.ThreeTimesTwoComboBox_SelectionChanged;
        }

        public double? ThreeTimesThree
        {
            get { return (double?)GetValue(ThreeTimesThreeProperty); }
            set { SetValue(ThreeTimesThreeProperty, value); }
        }

        public static readonly DependencyProperty ThreeTimesThreeProperty = DependencyProperty.Register(
            "ThreeTimesThree", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(ThreeTimesThreePropertyChangedCallback)));

        private static void ThreeTimesThreePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;

            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.ThreeTimesThreeComboBox.SelectionChanged -= control.ThreeTimesThreeComboBox_SelectionChanged;
                control.ThreeTimesThreeComboBox.SelectedValue = control.CountList[0];
                control.ThreeTimesThreeComboBox.SelectionChanged += control.ThreeTimesThreeComboBox_SelectionChanged;
                return;
            }
           
            control.ThreeTimesThreeComboBox.SelectionChanged -= control.ThreeTimesThreeComboBox_SelectionChanged;
            control.ThreeTimesThreeComboBox.SelectedValue = selectedVal;
            control.ThreeTimesThreeComboBox.SelectionChanged += control.ThreeTimesThreeComboBox_SelectionChanged;
        }





        public double? FourTimesOne
        {
            get { return (double?)GetValue(FourTimesOneProperty); }
            set { SetValue(FourTimesOneProperty, value); }
        }

        public static readonly DependencyProperty FourTimesOneProperty = DependencyProperty.Register(
            "FourTimesOne", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(FourTimesOnePropertyChangedCallback)));

        private static void FourTimesOnePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.FourTimesOneComboBox.SelectionChanged -= control.FourTimesOneComboBox_SelectionChanged;
                control.FourTimesOneComboBox.SelectedValue = control.CountList[0];
                control.FourTimesOneComboBox.SelectionChanged += control.FourTimesOneComboBox_SelectionChanged;
                return;
            }
            
            control.FourTimesOneComboBox.SelectionChanged -= control.FourTimesOneComboBox_SelectionChanged;
            control.FourTimesOneComboBox.SelectedValue = selectedVal;
            control.FourTimesOneComboBox.SelectionChanged += control.FourTimesOneComboBox_SelectionChanged;
        }

        public double? FourTimesTwo
        {
            get { return (double?)GetValue(FourTimesTwoProperty); }
            set { SetValue(FourTimesTwoProperty, value); }
        }

        public static readonly DependencyProperty FourTimesTwoProperty = DependencyProperty.Register(
            "FourTimesTwo", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(FourTimesTwoPropertyChangedCallback)));

        private static void FourTimesTwoPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.FourTimesTwoComboBox.SelectionChanged -= control.FourTimesTwoComboBox_SelectionChanged;
                control.FourTimesTwoComboBox.SelectedValue = control.CountList[0];
                control.FourTimesTwoComboBox.SelectionChanged += control.FourTimesTwoComboBox_SelectionChanged;
                return;
            }
            
            control.FourTimesTwoComboBox.SelectionChanged -= control.FourTimesTwoComboBox_SelectionChanged;
            control.FourTimesTwoComboBox.SelectedValue = selectedVal;
            control.FourTimesTwoComboBox.SelectionChanged += control.FourTimesTwoComboBox_SelectionChanged;
        }

        public double? FourTimesThree
        {
            get { return (double?)GetValue(FourTimesThreeProperty); }
            set { SetValue(FourTimesThreeProperty, value); }
        }

        public static readonly DependencyProperty FourTimesThreeProperty = DependencyProperty.Register(
            "FourTimesThree", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(FourTimesThreePropertyChangedCallback)));

        private static void FourTimesThreePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.FourTimesThreeComboBox.SelectionChanged -= control.FourTimesThreeComboBox_SelectionChanged;
                control.FourTimesThreeComboBox.SelectedValue = control.CountList[0];
                control.FourTimesThreeComboBox.SelectionChanged += control.FourTimesThreeComboBox_SelectionChanged;
                return;
            }
           
            control.FourTimesThreeComboBox.SelectionChanged -= control.FourTimesThreeComboBox_SelectionChanged;
            control.FourTimesThreeComboBox.SelectedValue = selectedVal;
            control.FourTimesThreeComboBox.SelectionChanged += control.FourTimesThreeComboBox_SelectionChanged;
        }

        public double? FourTimesFour
        {
            get { return (double?)GetValue(FourTimesFourProperty); }
            set { SetValue(FourTimesFourProperty, value); }
        }

        public static readonly DependencyProperty FourTimesFourProperty = DependencyProperty.Register(
            "FourTimesFour", typeof(double?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(FourTimesFourPropertyChangedCallback)));

        private static void FourTimesFourPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (double)e.NewValue;
            CountModel selectedVal;
            try
            {
                selectedVal = control.CountList.SingleOrDefault(a => a.Value == val);
            }
            catch
            {
                control.FourTimesFourComboBox.SelectionChanged -= control.FourTimesFourComboBox_SelectionChanged;
                control.FourTimesFourComboBox.SelectedValue = control.CountList[0];
                control.FourTimesFourComboBox.SelectionChanged += control.FourTimesFourComboBox_SelectionChanged;
                return;
            }
         
            control.FourTimesFourComboBox.SelectionChanged -= control.FourTimesFourComboBox_SelectionChanged;
            control.FourTimesFourComboBox.SelectedValue = selectedVal;
            control.FourTimesFourComboBox.SelectionChanged += control.FourTimesFourComboBox_SelectionChanged;
        }





        public DrugType? MedicineDrugType
        {
            get { return (DrugType?)GetValue(MedicineDrugTypeProperty); }
            set { SetValue(MedicineDrugTypeProperty, value); }
        }

        public static readonly DependencyProperty MedicineDrugTypeProperty = DependencyProperty.Register(
            "MedicineDrugType", typeof(DrugType?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(DrugTypeChangedCallback)));

        private static void DrugTypeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (DrugType)e.NewValue;
            var selectedVal = control.DrugTypeList.SingleOrDefault(a => a.Type == val);
            control.DrugTypeComboBox.SelectionChanged -= control.DrugTypeComboBox_SelectionChanged;
            control.DrugTypeComboBox.SelectedValue = selectedVal;
            control.DrugTypeComboBox.SelectionChanged += control.DrugTypeComboBox_SelectionChanged;
        }

        public ScheduleFrequency? MedicineScheduleFrquency
        {
            get { return (ScheduleFrequency?)GetValue(MedicineScheduleFrquencyProperty); }
            set { SetValue(MedicineScheduleFrquencyProperty, value); }
        }

        public static readonly DependencyProperty MedicineScheduleFrquencyProperty = DependencyProperty.Register(
            "MedicineScheduleFrquency", typeof(ScheduleFrequency?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(MedicineScheduleFrquencyChangedCallback)));

        private static void MedicineScheduleFrquencyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (ScheduleFrequency)e.NewValue;
            var selectedVal = control.FrequencyList.SingleOrDefault(a => a.Frequency == val);
            control.FrquencyComboBox.SelectionChanged -= control.FrquencyComboBox_SelectionChanged;
            control.FrquencyComboBox.SelectedValue = selectedVal;
            control.FrquencyComboBox.SelectionChanged += control.FrquencyComboBox_SelectionChanged;
        }

        public Route? MedicineRouteType
        {
            get { return (Route?)GetValue(MedicineRouteTypeProperty); }
            set { SetValue(MedicineRouteTypeProperty, value); }
        }

        public static readonly DependencyProperty MedicineRouteTypeProperty = DependencyProperty.Register(
            "MedicineRouteType", typeof(Route?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(MedicineRouteTypeChangedCallback)));

        private static void MedicineRouteTypeChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (Route)e.NewValue;
            var selectedVal = control.RouteList.SingleOrDefault(a => a.MedicineRoute == val);
            control.RouteComboBox.SelectionChanged -= control.RouteComboBox_SelectionChanged;
            control.RouteComboBox.SelectedValue = selectedVal;
            control.RouteComboBox.SelectionChanged += control.RouteComboBox_SelectionChanged;
        }

        public MedicineDurationType? MedicineDuration
        {
            get { return (MedicineDurationType?)GetValue(MedicineDurationProperty); }
            set { SetValue(MedicineDurationProperty, value); }
        }

        public static readonly DependencyProperty MedicineDurationProperty = DependencyProperty.Register(
            "MedicineDuration", typeof(MedicineDurationType?), typeof(MedicineItemControl), new PropertyMetadata(null, new
                 PropertyChangedCallback(MedicineDurationPropertyChangedCallback)));

        private static void MedicineDurationPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MedicineItemControl control = (MedicineItemControl)d;
            var val = (MedicineDurationType)e.NewValue;
            var selectedVal = control.DurationList.SingleOrDefault(a => a.Duration == val);
            control.DurationTypeComboBox.SelectionChanged -= control.DurationTypeComboBox_SelectionChanged;
            control.DurationTypeComboBox.SelectedValue = selectedVal;
            control.DurationTypeComboBox.SelectionChanged += control.DurationTypeComboBox_SelectionChanged;
        }
        #endregion


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if(DeleteCommand != null)
            {
                if (DeleteCommand.CanExecute(null))
                {
                    DeleteCommand.Execute(null);
                }
            }
        }

        private void DrugTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (DrugTypeModel)DrugTypeComboBox.SelectedValue;
            MedicineDrugType = SelectedVal.Type;
        }

        private void FrquencyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (ScheduleFrequencyModel)FrquencyComboBox.SelectedValue;
            MedicineScheduleFrquency = SelectedVal.Frequency;

            switch (SelectedVal.Frequency)
            {
                case ScheduleFrequency.ThriceADay:
                    ThreeTimesSP.Visibility = Visibility.Visible;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    break;

                case ScheduleFrequency.FourTimesInADay:
                    ThreeTimesSP.Visibility = Visibility.Collapsed;
                    FourTimesSP.Visibility = Visibility.Visible;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    break;

                case ScheduleFrequency.AsNeeded:
                    ThreeTimesSP.Visibility = Visibility.Collapsed;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Visible;
                    break;

                default:
                    ThreeTimesSP.Visibility = Visibility.Visible;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        private void RouteComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (MedicineRouteModel)RouteComboBox.SelectedValue;
            MedicineRouteType = SelectedVal.MedicineRoute;
        }

        private void DurationTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (MedicineDurationModel)DurationTypeComboBox.SelectedValue;
            MedicineDuration = SelectedVal.Duration;

            switch (SelectedVal.Duration)
            {
                case MedicineDurationType.Continue:
                    DurationTextBox.IsEnabled = false;
                    break;

                default:
                    DurationTextBox.IsEnabled = true;
                    break;
            }
        }

        private void ThreeTimesOneComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)ThreeTimesOneComboBox.SelectedValue;
            ThreeTimesOne = SelectedVal.Value;
        }

        private void ThreeTimesTwoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)ThreeTimesTwoComboBox.SelectedValue;
            ThreeTimesTwo = SelectedVal.Value;
        }

        private void ThreeTimesThreeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)ThreeTimesThreeComboBox.SelectedValue;
            ThreeTimesThree = SelectedVal.Value;
        }

        private void FourTimesOneComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)FourTimesOneComboBox.SelectedValue;
            FourTimesOne = SelectedVal.Value;
        }

        private void FourTimesTwoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)FourTimesTwoComboBox.SelectedValue;
            FourTimesTwo = SelectedVal.Value;
        }

        private void FourTimesThreeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)FourTimesThreeComboBox.SelectedValue;
            FourTimesThree = SelectedVal.Value;
        }

        private void FourTimesFourComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var SelectedVal = (CountModel)FourTimesFourComboBox.SelectedValue;
            FourTimesFour = SelectedVal.Value;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var index = FrquencyComboBox.SelectedIndex;
            switch (index)
            {
                case 0:
                    ThreeTimesSP.Visibility = Visibility.Visible;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    break;

                case 1:
                    ThreeTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    FourTimesSP.Visibility = Visibility.Visible;
                    break;

                case 2:
                    ThreeTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Visible;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    break;

                default:
                    ThreeTimesSP.Visibility = Visibility.Visible;
                    FourTimesSP.Visibility = Visibility.Collapsed;
                    CustomScheduleTextBox.Visibility = Visibility.Collapsed;
                    break;
            }
        }
    }






    public class DrugTypeModel: INotifyPropertyChanged
    {
        private string name;
        private DrugType drugType;


        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public DrugType Type { get { return drugType; } set { drugType = value; RaisePropertyChanged(); } }
       

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class ScheduleFrequencyModel: INotifyPropertyChanged
    {
        private string name;
        private ScheduleFrequency frequency;


        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public ScheduleFrequency Frequency { get { return frequency; } set { frequency = value; RaisePropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class MedicineRouteModel : INotifyPropertyChanged
    {
        private string name;
        private Route medicineRoute;


        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public Route MedicineRoute { get { return medicineRoute; } set { medicineRoute = value; RaisePropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class MedicineDurationModel : INotifyPropertyChanged
    {
        private string name;
        private MedicineDurationType duration;


        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public MedicineDurationType Duration { get { return duration; } set { duration = value; RaisePropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class CountModel: INotifyPropertyChanged
    {
        private string name;
        private double? _value;

        public string Name { get { return name; } set { name = value; RaisePropertyChanged(); } }
        public double? Value { get { return _value; } set { _value = value; RaisePropertyChanged(); } }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
