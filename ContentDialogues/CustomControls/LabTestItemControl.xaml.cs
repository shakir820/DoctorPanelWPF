﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues.CustomControls
{
    /// <summary>
    /// Interaction logic for LabTestItemControl.xaml
    /// </summary>
    public partial class LabTestItemControl : UserControl
    {
        public LabTestItemControl()
        {
            InitializeComponent();
        }


        private bool InCallBackMethod = false;
        private bool InSelectionChangedEvent = false;

        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }



        public static readonly DependencyProperty DeleteCommandProperty = DependencyProperty.Register("DeleteCommand",
            typeof(ICommand), typeof(LabTestItemControl));


        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteCommand != null)
            {
                if (DeleteCommand.CanExecute(null))
                {
                    DeleteCommand.Execute(null);
                }
            }
        }
    }
}
