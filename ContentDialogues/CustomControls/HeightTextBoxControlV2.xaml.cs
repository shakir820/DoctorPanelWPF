﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues.CustomControls
{
    /// <summary>
    /// Interaction logic for HeightTextBoxControlV2.xaml
    /// </summary>
    public partial class HeightTextBoxControlV2 : UserControl, INotifyPropertyChanged
    {
        public HeightTextBoxControlV2()
        {
            InitializeComponent();
        }

        private bool InCentimeterChangedCallback = false;
        private bool InFeetChangedCallback = false;
        private bool InInchChangedCallback = false;
        private bool DontCalculateFeetAndInch = false;
        private bool DontCalculateCentimeter = false;
        private bool InCentimeter = false;
        private bool InFeet = false;
        private bool InInch = false;
        private int? centimeter;
        private int? feet;
        private int? inch;

        public int? Centimeter
        {
            get { return centimeter; }
            set
            {
                if(InCentimeter == false)
                {
                    InCentimeter = true;
                    if (centimeter != value)
                    {
                        centimeter = value;
                        if (HeightInCentimeter != value)
                        {
                            HeightInCentimeter = value;
                        }
                        RaisePropertyChanged();
                        if(DontCalculateFeetAndInch == false)
                        {
                            RaiseCentimeterChangedEvent((int)value);
                        }
                    }
                    InCentimeter = false;
                }
               
            }
        }

        public int? Feet
        {
            get { return feet; }
            set
            {
                if(InFeet == false)
                {
                    InFeet = true;
                    if(feet != value)
                    {
                        feet = value;
                        if(HeightInFeet != value)
                        {
                            HeightInFeet = value;
                        }
                        RaisePropertyChanged();
                        if(DontCalculateCentimeter == false)
                        {
                            RaiseFeetInchChangedEvent(value, Inch);
                        }
                       
                    }
                    InFeet = false;
                }
               
            }
        }

        public int? Inch
        {
            get { return inch; }
            set
            {
                if(InInch == false)
                {
                    if(inch != value)
                    {
                        inch = value;
                        if(HeightInInch != value)
                        {
                            HeightInInch = value;
                        }
                        RaisePropertyChanged();
                        if(DontCalculateCentimeter == false)
                        {
                            RaiseFeetInchChangedEvent(Feet, value);
                        } 
                    }
                   
                }
               
            }
        }

        #region dependancy property

        public int? HeightInCentimeter
        {
            get { return (int?)GetValue(HeightInCentimeterProperty); }
            set { SetValue(HeightInCentimeterProperty, value); }
        }

        public static readonly DependencyProperty HeightInCentimeterProperty = DependencyProperty.Register(
            "HeightInCentimeter", typeof(int?), typeof(HeightTextBoxControlV2), new PropertyMetadata(null, new
                 PropertyChangedCallback(OnHeightInCentimeterChanged)));

        private static void OnHeightInCentimeterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thisControl = (HeightTextBoxControlV2)d;
            if(thisControl.InCentimeterChangedCallback == false)
            {
                thisControl.InCentimeterChangedCallback = true;

                thisControl.DontCalculateFeetAndInch = true;
                thisControl.Centimeter = (int)e.NewValue;
                thisControl.DontCalculateFeetAndInch = false;

                thisControl.InCentimeterChangedCallback = false;
            }
        }


        public int? HeightInFeet
        {
            get { return (int?)GetValue(HeightInFeetProperty);}
            set { SetValue(HeightInFeetProperty, value); }
        }

        public static readonly DependencyProperty HeightInFeetProperty = DependencyProperty.Register(
            "HeightInFeet", typeof(int?), typeof(HeightTextBoxControlV2), new PropertyMetadata(null, new
                PropertyChangedCallback(OnHeightInFeetChanged)));

        private static void OnHeightInFeetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thisControl = (HeightTextBoxControlV2)d;
            if (thisControl.InFeetChangedCallback == false)
            {
                thisControl.InFeetChangedCallback = true;

                thisControl.DontCalculateCentimeter = true;
                thisControl.Feet = (int)e.NewValue;
                thisControl.DontCalculateCentimeter = false;

                thisControl.InFeetChangedCallback = false;
            }
        }


        public int? HeightInInch
        {
            get { return (int?)GetValue(HeightInInchProperty); }
            set { SetValue(HeightInInchProperty, value); }
        }

        public static readonly DependencyProperty HeightInInchProperty = DependencyProperty.Register(
            "HeightInInch", typeof(int?), typeof(HeightTextBoxControlV2), new PropertyMetadata(null, new
                PropertyChangedCallback(OnHeightInInchChanged)));

        private static void OnHeightInInchChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thisControl = (HeightTextBoxControlV2)d;
            if (thisControl.InInchChangedCallback == false)
            {
                thisControl.InInchChangedCallback = true;

                thisControl.DontCalculateCentimeter = true;
                thisControl.Inch = (int)e.NewValue;
                thisControl.DontCalculateCentimeter = false;

                thisControl.InInchChangedCallback = false;
            }
        }
        #endregion
        public event EventHandler<CentimeterChangedEventArgs> CentimeterChanged;
        public event EventHandler<FeetAndInchChangedEventArgs> FeetAndInchChanged;

        private void RaiseFeetInchChangedEvent(int? feet, int? inch)
        {
            if (feet != null && inch != null)
            {
                FeetAndInchChangedEventArgs eventArgs = new FeetAndInchChangedEventArgs() { Feet = (int)feet, Inch = (int)inch };
                FeetAndInchChanged?.Invoke(this, eventArgs);
            }
            else if(feet == null && inch != null)
            {
                FeetAndInchChangedEventArgs eventArgs = new FeetAndInchChangedEventArgs() { Feet = 0, Inch = (int)inch };
                FeetAndInchChanged?.Invoke(this, eventArgs);
            }
            else if(feet != null && inch == null)
            {
                FeetAndInchChangedEventArgs eventArgs = new FeetAndInchChangedEventArgs() { Feet = (int)feet, Inch = 0 };
                FeetAndInchChanged?.Invoke(this, eventArgs);
            }
        }

        private void RaiseCentimeterChangedEvent(int cent)
        {
            CentimeterChangedEventArgs centimeterChangedEventArgs = new CentimeterChangedEventArgs() { Centimeter = cent };
            CentimeterChanged?.Invoke(this, centimeterChangedEventArgs);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class CentimeterChangedEventArgs: EventArgs
    {
        public int Centimeter;
    }

    public class FeetAndInchChangedEventArgs: EventArgs
    {
        public int Feet;
        public int Inch;
    }
}
