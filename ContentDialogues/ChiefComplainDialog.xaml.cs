﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for ChiefComplainDialog.xaml
    /// </summary>
    public partial class ChiefComplainDialog : UserControl
    {
        public ChiefComplainDialog()
        {
            InitializeComponent();
            GetSuggestions();
        }

        public ObservableCollection<ChiefComplainModel> SuggestedChiefComplains = new ObservableCollection<ChiefComplainModel>();

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach(var item in FrequentlyUsedItemsCollectionModel.Current.ChiefComplains)
            {
                SuggestedChiefComplains.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if(Prescription != null)
                {
                    var com = new ChiefComplainModel() { ComplainName = name };
                    com.Prescription = Prescription;
                    Prescription.ChiefComplains.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.ChiefComplains.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.ChiefComplains.Count > 10)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.ChiefComplains.RemoveAt(10);
                    }
                }
            }
        }

        
        private void SuggestionListView_Selected(object sender, RoutedEventArgs e)
        {
           
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedChiefComplains;
            ItemsListView.ItemsSource = Prescription.ChiefComplains;
            FilterSuggestionData();
        }

        private void FilterSuggestionData()
        {
            if(Prescription.ChiefComplains.Count > 0)
            {
                foreach (var item in Prescription.ChiefComplains)
                {
                    if (SuggestedChiefComplains.Contains(item))
                    {
                        SuggestedChiefComplains.Remove(item);
                    }
                }
            }
        }

        private void ItemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChiefComplainModel complain = (ChiefComplainModel)SuggestionListView.SelectedItem;
            complain.Duration = 0.0;
            complain.DurationUnit = DurationType.Hour;
            complain.Note = "";
            complain.Prescription = Prescription;
            Prescription.ChiefComplains.Add(complain);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedChiefComplains.Remove(complain);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }
    }
}