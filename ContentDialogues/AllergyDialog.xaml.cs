﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for AllergyDialog.xaml
    /// </summary>
    public partial class AllergyDialog : UserControl
    {
        public AllergyDialog()
        {
            InitializeComponent();
            GetSuggestions();
            DataContext = this;
        }



        public ObservableCollection<AllergyModel> SuggestedAllergies = new ObservableCollection<AllergyModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedAllergies;
            SelectedItemsListView.ItemsSource = Prescription.Allergies;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.Allergies)
            {
                SuggestedAllergies.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new AllergyModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.Allergies.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.Allergies.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.Allergies.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.Allergies.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.Allergies.Count > 0)
            {
                foreach (var item in Prescription.Allergies)
                {
                    if (SuggestedAllergies.Contains(item))
                    {
                        SuggestedAllergies.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AllergyModel allergy = (AllergyModel)SuggestionListView.SelectedItem;
            allergy.Duration = 0.0;
            allergy.DurationUnit = DurationType.Hour;
            allergy.Note = "";
            allergy.Prescription = Prescription;
            Prescription.Allergies.Add(allergy);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedAllergies.Remove(allergy);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<AllergyModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedAllergies
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedAllergies;
            }
        }
    }
}
