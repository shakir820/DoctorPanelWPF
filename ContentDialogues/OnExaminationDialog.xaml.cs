﻿using DoctorPanelWPF.ContentDialogues.CustomControls;
using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for OnExaminationDialog.xaml
    /// </summary>
    public partial class OnExaminationDialog : UserControl, INotifyPropertyChanged
    {
        public OnExaminationDialog()
        {
            InitializeComponent();
            DataContext = this;
        }

        private PatientExaminationModel _patientExamination;
        public PatientExaminationModel PatientExamination
        {
            get { return _patientExamination; }
            set { _patientExamination = value; RaisePropertyChanged(); }
        }






        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // DataContext = this;
            GetDataReady();

        }

        private void GetDataReady()
        {
            if(PatientExamination != null)
            {

                //for weight
                if(PatientExamination.WeightUnitType == WeightUnit.Kg)
                {
                    WeightUnitComboBox.SelectedIndex = 0;
                }
                else if (PatientExamination.WeightUnitType == WeightUnit.Pound)
                {
                    WeightUnitComboBox.SelectedIndex = 1;
                }

                //for temperature
                if(PatientExamination.TemperatureUnitType == TemperatureUnit.Celsius)
                {
                    TemperatureUnitComboBox.SelectedIndex = 0;
                }
                else if (PatientExamination.TemperatureUnitType == TemperatureUnit.Fahrenheit)
                {
                    TemperatureUnitComboBox.SelectedIndex = 1;
                }
            }
        }

        private void WeightUnitComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem boxItem = (ComboBoxItem)WeightUnitComboBox.SelectedValue;
            switch (boxItem.Tag)
            {
                case "KG":
                    PatientExamination.WeightUnitType = WeightUnit.Kg;
                    break;

                case "Pound":
                    PatientExamination.WeightUnitType = WeightUnit.Pound;
                    break;

                default:
                    PatientExamination.WeightUnitType = WeightUnit.Kg;
                    break;
            }
        }

        private void TemperatureUnitComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem boxItem = (ComboBoxItem)TemperatureUnitComboBox.SelectedValue;
            switch (boxItem.Tag)
            {
                case "Celsius":
                    PatientExamination.TemperatureUnitType = TemperatureUnit.Celsius;
                    break;

                case "Fahrenheit":
                    PatientExamination.TemperatureUnitType = TemperatureUnit.Fahrenheit;
                    break;

                default:
                    PatientExamination.TemperatureUnitType = TemperatureUnit.Celsius;
                    break;
            }
        }

        private void HeightTextBoxControlV2_CentimeterChanged(object sender, CentimeterChangedEventArgs e)
        {
            CentimeterChangedEventArgs args = e;
            PatientExamination.CalCulateFeetAndInch(args.Centimeter);
        }

        private void HeightTextBoxControlV2_FeetAndInchChanged(object sender, FeetAndInchChangedEventArgs e)
        {
            FeetAndInchChangedEventArgs args = e;
            PatientExamination.CalCulateCentimeter(args.Feet, args.Inch);
        }
    }
}
