﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for ChronicDiseaseDialog.xaml
    /// </summary>
    public partial class ChronicDiseaseDialog : UserControl
    {
        public ChronicDiseaseDialog()
        {
            InitializeComponent();
            GetSuggestions();
        }

        public ObservableCollection<ChronicDiseaseModel> SuggestedChronicDisease = new ObservableCollection<ChronicDiseaseModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedChronicDisease;
            SelectedItemsListView.ItemsSource = Prescription.ChronicDiseases;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.ChronicDiseases)
            {
                SuggestedChronicDisease.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new ChronicDiseaseModel() { DiseaseName = name };
                    com.Prescription = Prescription;
                    Prescription.ChronicDiseases.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.ChronicDiseases.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.ChronicDiseases.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.ChronicDiseases.RemoveAt(20);
                    }
                }
            }
        }


      

       

        private void FilterSuggestionData()
        {
            if (Prescription.ChronicDiseases.Count > 0)
            {
                foreach (var item in Prescription.ChronicDiseases)
                {
                    if (SuggestedChronicDisease.Contains(item))
                    {
                        SuggestedChronicDisease.Remove(item);
                    }
                }
            }
        }

     

        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChronicDiseaseModel ChronicDisease = (ChronicDiseaseModel)SuggestionListView.SelectedItem;
            ChronicDisease.Duration = 0.0;
            ChronicDisease.DurationUnit = DurationType.Hour;
            ChronicDisease.Note = "";
            ChronicDisease.Prescription = Prescription;
            Prescription.ChronicDiseases.Add(ChronicDisease);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedChronicDisease.Remove(ChronicDisease);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<ChronicDiseaseModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedChronicDisease
               .Where(c => (c.DiseaseName.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedChronicDisease;
            }
        }
    }
}
