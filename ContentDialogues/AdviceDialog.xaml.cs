﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for AdviceDialog.xaml
    /// </summary>
    public partial class AdviceDialog : UserControl
    {
        public AdviceDialog()
        {
            InitializeComponent();
            GetSuggestions();
        }

        public ObservableCollection<AdviceModel> SuggestedAdvice = new ObservableCollection<AdviceModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedAdvice;
            SelectedItemsListView.ItemsSource = Prescription.Advices;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.AdviceList)
            {
                SuggestedAdvice.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new AdviceModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.Advices.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.AdviceList.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.AdviceList.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.AdviceList.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.Advices.Count > 0)
            {
                foreach (var item in Prescription.Advices)
                {
                    if (SuggestedAdvice.Contains(item))
                    {
                        SuggestedAdvice.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AdviceModel advice = (AdviceModel)SuggestionListView.SelectedItem;
           
            advice.Note = "";
            advice.Prescription = Prescription;
            Prescription.Advices.Add(advice);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedAdvice.Remove(advice);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<AdviceModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedAdvice
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedAdvice;
            }
        }
    }
}
