﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for InvestigationLabTestDialog.xaml
    /// </summary>
    public partial class InvestigationLabTestDialog : UserControl
    {
        public InvestigationLabTestDialog()
        {
            InitializeComponent();
            GetSuggestions();
            DataContext = this;
        }



        public ObservableCollection<InvestigationModel> SuggestedLabTests = new ObservableCollection<InvestigationModel>();

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SuggestionListView.ItemsSource = SuggestedLabTests;
            SelectedItemsListView.ItemsSource = Prescription.Investigations;
            FilterSuggestionData();
        }

        public PrescriptionModel Prescription;


        private void GetSuggestions()
        {
            foreach (var item in FrequentlyUsedItemsCollectionModel.Current.InvestigationLabTests)
            {
                SuggestedLabTests.Add(item);
            }
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                string name = SearchBox.Text;
                if (Prescription != null)
                {
                    var com = new InvestigationModel() { Name = name };
                    com.Prescription = Prescription;
                    Prescription.Investigations.Add(com);

                    FrequentlyUsedItemsCollectionModel.Current.InvestigationLabTests.Insert(0, com);

                    if (FrequentlyUsedItemsCollectionModel.Current.InvestigationLabTests.Count > 20)
                    {
                        FrequentlyUsedItemsCollectionModel.Current.InvestigationLabTests.RemoveAt(20);
                    }
                }
            }
        }






        private void FilterSuggestionData()
        {
            if (Prescription.Investigations.Count > 0)
            {
                foreach (var item in Prescription.Investigations)
                {
                    if (SuggestedLabTests.Contains(item))
                    {
                        SuggestedLabTests.Remove(item);
                    }
                }
            }
        }



        private void SuggestionListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InvestigationModel investigation = (InvestigationModel)SuggestionListView.SelectedItem;
            investigation.Note = "";
            investigation.Prescription = Prescription;
            Prescription.Investigations.Add(investigation);

            //string name = complain.ComplainName;
            //var MatchedComplain = SuggestedChiefComplains.Single(y => y.ComplainName == name);
            SuggestionListView.SelectionChanged -= SuggestionListView_SelectionChanged;
            SuggestedLabTests.Remove(investigation);
            SuggestionListView.SelectionChanged += SuggestionListView_SelectionChanged;
        }

        private List<InvestigationModel> GetMatchingSuggestions(string query)
        {
            var result = SuggestedLabTests
               .Where(c => (c.Name.IndexOf(query, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
            return result;
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(((TextBox)sender).Text))
            {
                var result = GetMatchingSuggestions(((TextBox)sender).Text);
                SuggestionListView.ItemsSource = result;
            }
            else
            {
                SuggestionListView.ItemsSource = SuggestedLabTests;
            }
        }
    }
}
