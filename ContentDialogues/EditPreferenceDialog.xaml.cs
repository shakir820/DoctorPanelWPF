﻿using DoctorPanelWPF.Models;
using DoctorPanelWPF.Views.OnlineConsultationViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.ContentDialogues
{
    /// <summary>
    /// Interaction logic for EditPreferenceDialog.xaml
    /// </summary>
    public partial class EditPreferenceDialog : UserControl
    {
        public ObservableCollection<QuestionTag> QuestionPreferences = new ObservableCollection<QuestionTag>();

        public EditPreferenceDialog()
        {
            InitializeComponent();
        }

        private void ItemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            var button = sender as ToggleButton;
            if(button.IsChecked == true)
            {
                var name = button.Content;

            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ItemsListView.ItemsSource = QuestionTags.Current.QuestionPreferences;
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}
