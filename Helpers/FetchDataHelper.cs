﻿using DoctorPanelWPF.Models;
using DoctorPanelWPF.Models.API_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorPanelWPF.Helpers
{
    public static class FetchDataHelper
    {
        public static UserModel FetchUserData(User user)
        {
            var usermodel = new UserModel();
            //usermodel.Name =  user.Name;
            //usermodel.Email = user.Email;
            usermodel.Address = user.Address;
            usermodel.Age = user.Age;
            usermodel.AlternateEmail = user.AlternateEmail;
            usermodel.AlternateMobile = (string)user.AlternateMobile;
            usermodel.BirthCertId = user.BirthCertId;
            usermodel.BloodGroup = user.BloodGroup;
            usermodel.AccessToken = user.AccessToken;
            usermodel.BirthCertId = user.BirthCertId;
            usermodel.DateOfBirth = user.DateOfBirth;
            usermodel.DistrictId = user.DistrictId;
            usermodel.DistrictName = user.DistrictName;
            usermodel.DivisionId = user.DivisionId;
            usermodel.DivisionName = user.DivisionName;
            usermodel.Email = user.Email;
            usermodel.EmailVerificationCode = user.EmailVerificationCode;
            usermodel.EmailVerified = user.EmailVerified;
            usermodel.EmergencyContact = user.EmergencyContact;
            usermodel.EnabledSubscription = user.EnabledSubscription;
            usermodel.FatherName = user.FatherName;
            usermodel.Gender = user.Gender;
            usermodel.Image = user.Image;
            usermodel.IsPasswordSet = user.IsPasswordSet;
            usermodel.IsPractitioner = user.IsPractitioner;
            usermodel.IsPremium = user.IsPremium;
            usermodel.IsSubscribed = user.IsSubscribed;
            usermodel.IsTempPasswordUsed = user.IsTempPasswordUsed;
            usermodel.IsTrialUser = user.IsTrialUser;
            usermodel.MaritalStatus = user.MaritalStatus;
            usermodel.Mobile = user.Mobile;
            usermodel.MobileVerified = user.MobileVerified;
            usermodel.MotherName = user.MotherName;
            usermodel.Name = user.Name;
            usermodel.Nid = user.Nid;
            usermodel.PassportNo = user.PassportNo;
            usermodel.PractitionerInfo = new Models.PractitionerInfo();
            usermodel.PractitionerInfo.Address = user.PractitionerInfo.Address;
            usermodel.PractitionerInfo.Area = user.PractitionerInfo.Area;
            usermodel.PractitionerInfo.BmdcCertImg = user.PractitionerInfo.BmdcCertImg;
            usermodel.PractitionerInfo.BmdcCertImgVerified = user.PractitionerInfo.BmdcCertImgVerified;
            usermodel.PractitionerInfo.BmdcCertNo = user.PractitionerInfo.BmdcCertNo;
            usermodel.PractitionerInfo.BmdcCertNoVerified = user.PractitionerInfo.BmdcCertNoVerified;
            usermodel.PractitionerInfo.CertificateDetailsProvided = user.PractitionerInfo.CertificateDetailsProvided;
            usermodel.PractitionerInfo.ChamberScheduleAdded = user.PractitionerInfo.ChamberScheduleAdded;
            usermodel.PractitionerInfo.Designation = user.PractitionerInfo.Designation;
            usermodel.PractitionerInfo.District = user.PractitionerInfo.District;
            usermodel.PractitionerInfo.Division = user.PractitionerInfo.Division;
            usermodel.PractitionerInfo.EducationalDetailsProvided = user.PractitionerInfo.EducationalDetailsProvided;
            usermodel.PractitionerInfo.EducationalInfo = user.PractitionerInfo.EducationalInfo;
            usermodel.PractitionerInfo.Email = user.PractitionerInfo.Email;
            usermodel.PractitionerInfo.ImagePath = user.PractitionerInfo.ImagePath;
            usermodel.PractitionerInfo.Latitude = user.PractitionerInfo.Latitude;
            usermodel.PractitionerInfo.Longitude = user.PractitionerInfo.Longitude;
            usermodel.PractitionerInfo.MembershipNumber = user.PractitionerInfo.MembershipNumber;
            usermodel.PractitionerInfo.Mobile = (string)user.PractitionerInfo.Mobile;
            usermodel.PractitionerInfo.Mobile2 = (string)user.PractitionerInfo.Mobile2;
            usermodel.PractitionerInfo.NationalId = user.PractitionerInfo.NationalId;
            usermodel.PractitionerInfo.PersonalDetailsProvided = user.PractitionerInfo.PersonalDetailsProvided;
            usermodel.PractitionerInfo.ProfileCompleteness = (double)user.PractitionerInfo.ProfileCompleteness;
            usermodel.PractitionerInfo.QualificationTitle = user.PractitionerInfo.QualificationTitle;
            usermodel.PractitionerInfo.Specialities = user.PractitionerInfo.Specialities;
            usermodel.PractitionerInfo.SpecializationDesignationProvided = user.PractitionerInfo.SpecializationDesignationProvided;
            usermodel.PractitionerInfo.Upazilla = (long?)user.PractitionerInfo.Upazilla;
            usermodel.PractitionerInfo.VerifiedDoctor = user.PractitionerInfo.VerifiedDoctor;
            usermodel.RegisteredFrom = user.RegisteredFrom;
            usermodel.RequestChangeEmail = user.RequestChangeEmail;
            usermodel.ShortBio = user.ShortBio;
            usermodel.ShortBioBn = user.ShortBioBn;
            usermodel.SubscribedNumber = user.SubscribedNumber;
            usermodel.SubscribedPlans = new System.Collections.ObjectModel.ObservableCollection<Models.SubscribedPlan>(); ;
           
            if(user.SubscribedPlans.Count > 0)
            {
                int i = 0;
             foreach(var item in user.SubscribedPlans)
                {
                    usermodel.SubscribedPlans.Add(new Models.SubscribedPlan());
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo = new Models.InsuranceClaimInfo();
                    
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd = new Models.Ipd();
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd.ClaimLeft = item.InsuranceClaimInfo.Ipd.ClaimLeft;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd.ClaimLimit = item.InsuranceClaimInfo.Ipd.ClaimLimit;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd.MaxLimitPerClaim = item.InsuranceClaimInfo.Ipd.MaxLimitPerClaim;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd.MaxLimitPerNight = item.InsuranceClaimInfo.Ipd.MaxLimitPerNight;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Ipd.TotalClaimed = item.InsuranceClaimInfo.Ipd.TotalClaimed;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest = new Models.Ipd();
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest.ClaimLeft = item.InsuranceClaimInfo.LabTest.ClaimLeft;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest.ClaimLimit = item.InsuranceClaimInfo.LabTest.ClaimLimit;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest.MaxLimitPerClaim = item.InsuranceClaimInfo.LabTest.MaxLimitPerClaim;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest.MaxLimitPerNight = item.InsuranceClaimInfo.LabTest.MaxLimitPerNight;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LabTest.TotalClaimed = item.InsuranceClaimInfo.LabTest.TotalClaimed;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance = new Models.Ipd();
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance.ClaimLeft = item.InsuranceClaimInfo.LifeInsurance.ClaimLeft;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance.ClaimLimit = item.InsuranceClaimInfo.LifeInsurance.ClaimLimit;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance.MaxLimitPerClaim = item.InsuranceClaimInfo.LifeInsurance.MaxLimitPerClaim;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance.MaxLimitPerNight = item.InsuranceClaimInfo.LifeInsurance.MaxLimitPerNight;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.LifeInsurance.TotalClaimed = item.InsuranceClaimInfo.LifeInsurance.TotalClaimed;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd = new Models.Ipd();
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd.ClaimLeft = item.InsuranceClaimInfo.Opd.ClaimLeft;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd.ClaimLimit = item.InsuranceClaimInfo.Opd.ClaimLimit;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd.MaxLimitPerClaim = item.InsuranceClaimInfo.Opd.MaxLimitPerClaim;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd.MaxLimitPerNight = item.InsuranceClaimInfo.Opd.MaxLimitPerNight;
                    usermodel.SubscribedPlans[i].InsuranceClaimInfo.Opd.TotalClaimed = item.InsuranceClaimInfo.Opd.TotalClaimed;
                    usermodel.SubscribedPlans[i].IsActive = item.IsActive;
                    usermodel.SubscribedPlans[i].PlanId = item.PlanId;
                    usermodel.SubscribedPlans[i].PlanName = item.PlanName;
                    usermodel.SubscribedPlans[i].PlanSlug = item.PlanSlug;
                    usermodel.SubscribedPlans[i].SubscriptionActivationDate = item.SubscriptionActivationDate;
                    usermodel.SubscribedPlans[i].SubscriptionExpiryDate = item.SubscriptionExpiryDate;

                    i++;
                }
            }

            usermodel.SubscriptionMaxExpiryDate = user.SubscriptionMaxExpiryDate;
            usermodel.SubscriptionPlanId = user.SubscriptionPlanId;
            usermodel.SubscriptionType = user.SubscriptionType;
            usermodel.UpazillaId = user.UpazillaId??0;
            usermodel.UpazillaName = (string)user.UpazillaName;
            usermodel.UserId = user.UserId;
            usermodel.Username = user.Username;
            usermodel.UserNomineeInfo = user.UserNomineeInfo;


            return usermodel;
        }
    }
}
