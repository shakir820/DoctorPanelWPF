﻿using DataAccessLibrary;
using DoctorPanelWPF.ContentDialogues;
using DoctorPanelWPF.Helpers;
using DoctorPanelWPF.LoginAndRegister;
using DoctorPanelWPF.Models;
using DoctorPanelWPF.Views;
using DoctorPanelWPF.Views.ChamberViews.SubViewPages;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public EducationDetails educationDetails;
        public static MainWindow Current;
        public QuestionTags questionTags;
        public ChambersViewModel chambersViewModel;
        public FrequentlyUsedItemsCollectionModel FrequentlyUsedItemsCollection;
        public LoginWindow loginWindow;
        public UserModel UserData = new UserModel();
        //public ObservableCollection<ScheduleModel> ScheduleList
        public MainWindow()
        {
            InitializeComponent();
            Current = this;
            educationDetails = new EducationDetails();
            questionTags = new QuestionTags();
            chambersViewModel = new ChambersViewModel();
            loginWindow = LoginWindow.Current;
            //DatabaseHelper.InitializeDatabase();
        }

       

        public void GotoLoginPage()
        {
            LoginPage loginPage = new LoginPage();
            frame.Navigate(loginPage);
        }

        public async void ShowInvalidTimeDialog()
        {
            var view = new InvalidTimeWarningDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }



        public async void ShowOnExaminationDialog(PrescriptionModel prescription)
        {
            var view = new OnExaminationDialog();
            view.PatientExamination = prescription.ExaminedDetails;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowAddMedicineDialog(PrescriptionModel prescription)
        {
            var view = new AddMedicineDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowAdviceDialog(PrescriptionModel prescription)
        {
            var view = new AdviceDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowDiagnosisDialog(PrescriptionModel prescription)
        {
            var view = new DiagnosisDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowAllergyDialog(PrescriptionModel prescription)
        {
            var view = new AllergyDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowInvestigationLabTestDialog(PrescriptionModel prescription)
        {
            var view = new InvestigationLabTestDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowFamilyHistoryDialog(PrescriptionModel prescription)
        {
            var view = new FamilyHistoryDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowWarningDialog(string msg = null)
        {
            var view = new WarningMessageDialog();
            WarningMessageDialog.Current.msg = msg;
           
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowPastMedicalHitoryDialog(PrescriptionModel prescription)
        {
            var view = new PastMedicalHistoryDialog();
            view.Prescription = prescription;

            var result = await DialogHost.Show(view, "MyDialogHost");
        }



        public async void ShowInvalidDateDialog()
        {

            var view = new InvalidDateDefinedDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
            //CreateNewSchedulePage.Current.StartDate.SelectedDate = null;
            //CreateNewSchedulePage.Current.EndDate.SelectedDate = null;
        }

        public async void ShowAddNewPatientDialog()
        {
            var view = new AddNewPatientDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowAddNewSpecialtyDialog()
        {
            var view = new AddNewSpecialtyDialogue();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowAddNewComplainDialog(PrescriptionModel presciption)
        {
            var view = new ChiefComplainDialog();
            view.Prescription = presciption;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowEditPreferencesDialog()
        {
            var view = new EditPreferenceDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ShowChronicDiseaseDialog(PrescriptionModel prescription)
        {
            var view = new ChronicDiseaseDialog();
            view.Prescription = prescription;
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ExecuteRunDialog()
        {
            var view = new AddNewEducationDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public async void ExecuteSelectChamberConfirmDialog()
        {
            var view = new NewChamberSelectConfirmDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }

        public void GotoRegisterPage()
        {
            RegisterPage registerPage = new RegisterPage();
            frame.Navigate(registerPage);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Dashboard dashboard = new Dashboard();
            frame.Navigate(dashboard);
            //frame.Navigate(loginPage);
            FrequentlyUsedItemsCollection = new FrequentlyUsedItemsCollectionModel();
            FrequentlyUsedItemsCollection.ChiefComplains.Add(new ChiefComplainModel() { ComplainName = "Fever" });
            FrequentlyUsedItemsCollection.ChiefComplains.Add(new ChiefComplainModel() { ComplainName = "GGEZ" });
            FrequentlyUsedItemsCollection.ChiefComplains.Add(new ChiefComplainModel() { ComplainName = "Yahoo" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Other specified functional intestinal disorders" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Pigmentary glaucoma, right eye, moderate stage" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Simple and mucopurulent chronic bronchitis" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Gottron's papules" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Age-related osteoporosis with current pathological fracture, ankle and foot" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Other cyst of bone, unspecified lower leg" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Injury of trigeminal nerve, unspecified side" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Unspecified superficial injury of unspecified part of neck" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Brown-Sequard syndrome at C4 level of cervical spinal cord, initial encounter" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Other specified injury of brachial artery, unspecified side, subsequent encounter" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Nondisplaced fracture of olecranon process with intraarticular extension of left ulna, initial encounter for open fracture type I or II" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Burn of third degree of buttock, initial encounter" });
            FrequentlyUsedItemsCollection.ChronicDiseases.Add(new ChronicDiseaseModel() { DiseaseName = "Striking against or struck by other automobile airbag, sequela" });
            FrequentlyUsedItemsCollection.PastMedicalHistory.Add(new PastMedicalModel() { Name = "Striking" });
            FrequentlyUsedItemsCollection.PastMedicalHistory.Add(new PastMedicalModel() { Name = "Milk" });
            FrequentlyUsedItemsCollection.PastMedicalHistory.Add(new PastMedicalModel() { Name = "Egg" });
            FrequentlyUsedItemsCollection.Allergies.Add(new AllergyModel() { Name = "Fish" });
            FrequentlyUsedItemsCollection.Allergies.Add(new AllergyModel() { Name = "Prawn" });
            FrequentlyUsedItemsCollection.Allergies.Add(new AllergyModel() { Name = "Egg" });
            FrequentlyUsedItemsCollection.Allergies.Add(new AllergyModel() { Name = "Meat" });
            FrequentlyUsedItemsCollection.Allergies.Add(new AllergyModel() { Name = "Beef" });
            FrequentlyUsedItemsCollection.FamilyHistory.Add(new FamilyHistoryModel() { Name = "diabetes" });
            FrequentlyUsedItemsCollection.FamilyHistory.Add(new FamilyHistoryModel() { Name = "Yasuo" });
            FrequentlyUsedItemsCollection.FamilyHistory.Add(new FamilyHistoryModel() { Name = "Soraka" });
            FrequentlyUsedItemsCollection.InvestigationLabTests.Add(new InvestigationModel() { Name = "diabetes" });
            FrequentlyUsedItemsCollection.InvestigationLabTests.Add(new InvestigationModel() { Name = "Yasuo" });
            FrequentlyUsedItemsCollection.InvestigationLabTests.Add(new InvestigationModel() { Name = "Soraka" });
            FrequentlyUsedItemsCollection.DiagnosisList.Add(new DiagnosisModel() { Name = "diabetes" });
            FrequentlyUsedItemsCollection.DiagnosisList.Add(new DiagnosisModel() { Name = "Yasuo" });
            FrequentlyUsedItemsCollection.DiagnosisList.Add(new DiagnosisModel() { Name = "Soraka" });

            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "1st Cef 500 mg", Unit = "500 mg", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "3rd Ce 100 mg/5 ml", Unit = "100 mg / 5 ml", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "Zimax 1 gm/100 ml", Unit = "1 gm/100 ml", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "Napa 120 mg/5 ml", Unit = "120 mg/5 ml", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "3-Geocef 200 mg", Unit = "200 mg", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "Alugel 250 mg + 400 mg", Unit = "250 mg + 400 mg", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "Acorex 15 mg/5 ml", Unit = "15 mg/5 ml", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });
            FrequentlyUsedItemsCollection.MedicineList.Add(new MedicineModel() { Name = "Acme's Dextrose 5 gm/100 m", Unit= "5 gm/100 m", Frequency = ScheduleFrequency.ThriceADay, MedicineRoute = Route.Oral, MedicineType = DrugType.Cap });

            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Drink a lot of water" });
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Walk everyday"});
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Go to gym"});
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Do some exercise"});
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Dont take bath for few days"});
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Regular check your blood pressure" });
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Brush your teeth twich a day"});
            FrequentlyUsedItemsCollection.AdviceList.Add(new AdviceModel() { Name = "Use drop for eye"});

            loginWindow?.Close();
        }

        internal async void StartLoadingAsync()
        {
            var view = new LoadingDialog();
            var result = await DialogHost.Show(view, "MyDialogHost");
        }



        private void ShutDownBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void MinimizeBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void MaximizeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        public void GotoDashboard()
        {
            Dashboard dashboard = new Dashboard();
            frame.Navigate(dashboard);
        }

        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            bool status;

            if(Dashboard.Current != null)
            {
                 status = Dashboard.Current.GoBack();
                if(status == false)
                {
                    if (frame.CanGoBack) frame.GoBack();
                }
                else
                {
                    return;
                }
            }
            else
            {
                if (frame.CanGoBack) frame.GoBack();
            }
        }
    }
}
