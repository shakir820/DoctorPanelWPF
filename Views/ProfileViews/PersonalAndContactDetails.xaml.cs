﻿using DoctorPanelWPF.Models;
using DoctorPanelWPF.Models.API_Data;
using DoctorPanelWPF.Views.ProfileViews.EditPages;
using HTMLConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;

namespace DoctorPanelWPF.Views
{
    /// <summary>
    /// Interaction logic for PersonalAndContactDetails.xaml
    /// </summary>
    public partial class PersonalAndContactDetails : Page, INotifyPropertyChanged
    {

        private UserModel user_data;
        public UserModel UserData { get { return user_data; } set { user_data = value; RaisePropertyChanged(); } }
        public PersonalAndContactDetails()
        {
            InitializeComponent();
            DataContext = this;
            UserData = ProfilePage.Current?.UserModelData;
        }

        private void ImageBorder_MouseEnter(object sender, MouseEventArgs e)
        {
            CameraGrid.Visibility = Visibility.Visible;
        }

        private void ImageBorder_MouseLeave(object sender, MouseEventArgs e)
        {
            CameraGrid.Visibility = Visibility.Collapsed;
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void ThisPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (user_data.Address != null)
            {
                var htmlAddress = user_data.Address;
                var xamlAddress = HtmlToXamlConverter.ConvertHtmlToXaml(htmlAddress, true);
                //AddressTextBlock = SetRTF(xamlAddress);
                AddressTextBlock.Document = SetRTF(xamlAddress);
            }

            ////= XamlWriter.Save(ff.Blocks);
            ////var Bcollections = ff.Blocks;
            ////AddressTextBlock.Text = Bcollections.ToString(); 

            if (user_data.ShortBio != null)
            {
                var bio = user_data.ShortBio;
                var xamlBio = HtmlToXamlConverter.ConvertHtmlToXaml((string)bio, true);
                //AddressTextBlock = SetRTF(xamlAddress);
                ShortBioRichTextBox.Document = SetRTF(xamlBio);
            }

            // var name = UpdateData.GetPropertyName("Name");
        }

        private static FlowDocument SetRTF(string xamlString)
        {
            StringReader stringReader = new StringReader(xamlString);
            System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
            var sec = XamlReader.Load(xmlReader) as FlowDocument;
            return sec;
            //FlowDocument doc = new FlowDocument();
            //while (sec.Blocks.Count > 0)
            //{
            //    var block = sec.Blocks.FirstBlock;
            //    sec.Blocks.Remove(block);
            //    doc.Blocks.Add(block);
            //}
            //return doc;
        }

        private void UploadBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                var imgData = new Uri(dlg.FileName);
                Thread thread = new Thread(UpdateImageAsync);
                thread.Start(imgData);
            }
        }



        private async void UpdateImageAsync(object img_uri)
        {
            Uri uri = (Uri)img_uri;
            Dictionary<string, object> keyValuePairs = new Dictionary<string, object>();
            keyValuePairs.Add("image", uri);
            var ok = await User.Current.UpdateDataAsync(keyValuePairs);

            if (ok == true)
            {
                Dispatcher.Invoke(() =>
                {
                    UserData.Image = uri;
                });
            }
            else
            {

            }
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            var EditPage = new PersonalDetailsEditPage();
            EditPage.User_Data = UserData;
            ProfilePage.Current.frame.Navigate(EditPage);
        }
    }
}
