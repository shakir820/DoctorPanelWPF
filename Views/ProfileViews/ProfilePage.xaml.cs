﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views
{
    /// <summary>
    /// Interaction logic for ProfilePage.xaml
    /// </summary>
    public partial class ProfilePage : Page, INotifyPropertyChanged
    {
        public static ProfilePage Current;
        public ProfilePage()
        {
            InitializeComponent();
            Current = this;
            DataContext = this;
            PopulateNavigationItems();
        }


        public ObservableCollection<ProfileNavigationItemModel> NavigationItems = new ObservableCollection<ProfileNavigationItemModel>();
        public UserModel userdata;
        public UserModel UserModelData { get { return userdata; } set { userdata = value;  RaisePropertyChanged(); } }

        private void ImageBorder_MouseEnter(object sender, MouseEventArgs e)
        {
           

            //DoubleAnimation animation = new DoubleAnimation();
            //animation.From = 0.0;
            //animation.To = 1.0;
            //animation.Duration = TimeSpan.FromMilliseconds(500);
            //animation.EasingFunction = ();

            //Storyboard sb = new Storyboard();
            //sb.Children.Add(animation);

            //c.Opacity = 1;
            //c.Visibility = Visibility.Visible;

            //Storyboard.SetTarget(sb, c);
            //Storyboard.SetTargetProperty(sb, new PropertyPath(Control.OpacityProperty));

            //sb.Begin();

            //sb.Completed += delegate (object sender, EventArgs e)
            //{
            //    c.Visibility = Visibility.Collapsed;
            //};
        }

        private void ImageBorder_MouseLeave(object sender, MouseEventArgs e)
        {
           
        }

        private void NavigationListView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void NavigationListView_TouchDown(object sender, TouchEventArgs e)
        {

        }

        private void NavigationListView_Selected(object sender, RoutedEventArgs e)
        {
            //var selectedIndex = navigationListView.SelectedIndex;
            //switch (selectedIndex)
            //{
            //    case 0:
            //        frame.Navigate(new Uri("PersonalAndContactDetails.xaml", UriKind.Relative));
            //        break;

            //    case 1:
            //        frame.Navigate(new Uri("EducationalDetails.xaml", UriKind.Relative));
            //        break;

            //    case 2:
            //        frame.Navigate(new Uri("BMDC_CertificateDetails.xaml", UriKind.Relative));
            //        break;

            //    case 3:
            //        frame.Navigate(new Uri("SpecialityAndDesignation.xaml", UriKind.Relative));
            //        break;
            //    default:
            //        frame.Navigate(new Uri("PersonalAndContactDetails.xaml", UriKind.Relative));
            //        break;
            //}
        }

        private void PopulateNavigationItems()
        {
            NavigationItems.Add(new ProfileNavigationItemModel() { Title = "Personal and Contact Details", DetailsProvided = false, Icon = MaterialDesignThemes.Wpf.PackIconKind.Bio });
            NavigationItems.Add(new ProfileNavigationItemModel() { Title = "Educational Details", DetailsProvided = false, Icon = MaterialDesignThemes.Wpf.PackIconKind.Book });
            NavigationItems.Add(new ProfileNavigationItemModel() { Title = "BMDC Certificate Details", DetailsProvided = false, Icon = MaterialDesignThemes.Wpf.PackIconKind.Certificate });
            NavigationItems.Add(new ProfileNavigationItemModel() { Title = "Speciality &amp; Designation", DetailsProvided = false, Icon = MaterialDesignThemes.Wpf.PackIconKind.ChessQueen });
            navigationListView.ItemsSource = NavigationItems;
        }

        private void NavigationListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedIndex = navigationListView.SelectedIndex;
            switch (selectedIndex)
            {
                case 0:
                    PersonalAndContactDetails personalAndContactDetails = new PersonalAndContactDetails();
                    frame.Navigate(personalAndContactDetails);
                    break;

                case 1:
                    EducationalDetails educationalDetails = new EducationalDetails();
                    frame.Navigate(educationalDetails);
                    break;

                case 2:
                    BMDC_CertificateDetails bMDC_CertificateDetails = new BMDC_CertificateDetails();
                    frame.Navigate(bMDC_CertificateDetails);
                    break;

                case 3:
                    SpecialityAndDesignation specialityAndDesignation = new SpecialityAndDesignation();
                    frame.Navigate(specialityAndDesignation);
                    break;

                default:
                    personalAndContactDetails = new PersonalAndContactDetails();
                    frame.Navigate(personalAndContactDetails);
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UserModelData = MainWindow.Current.UserData??null;
            if(UserModelData != null)
            {
                UserModelData.PractitionerInfo.ProfileDetailsChanged += PractitionerInfo_ProfileDetailsChanged;
                //UserModelData.PractitionerInfo.PropertyChanged += PractitionerInfo_PropertyChanged;
                CheckForDetailsProvidedOrNot(UserModelData.PractitionerInfo);
                navigationListView.SelectedIndex = 0;
                CheckForDoctorVerification(UserModelData.PractitionerInfo.VerifiedDoctor);
            }
           
        }

        private void CheckForDetailsProvidedOrNot(PractitionerInfo practitionerInfo)
        {
            NavigationItems[0].DetailsProvided = practitionerInfo.PersonalDetailsProvided;
            NavigationItems[1].DetailsProvided = practitionerInfo.EducationalDetailsProvided;
            NavigationItems[2].DetailsProvided = practitionerInfo.CertificateDetailsProvided;
            NavigationItems[3].DetailsProvided = practitionerInfo.SpecializationDesignationProvided;
        }

        private void PractitionerInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        private void PractitionerInfo_ProfileDetailsChanged(object sender, ProfileDetailsChangedEventArgs e)
        {
            var practitioner_Info = (PractitionerInfo)sender;

            NavigationItems[0].DetailsProvided = practitioner_Info.PersonalDetailsProvided;
            NavigationItems[1].DetailsProvided = practitioner_Info.EducationalDetailsProvided;
            NavigationItems[2].DetailsProvided = practitioner_Info.CertificateDetailsProvided;
            NavigationItems[3].DetailsProvided = practitioner_Info.SpecializationDesignationProvided;
        }

        public void CheckForDoctorVerification(long verified)
        {
            if (verified != 0)
            {
                ApproveIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Check;
                ApprovedTextBlock.Text = "Approved";
            }
            else
            {
                ApprovedTextBlock.Text = "Not Approved";
                ApproveIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Close;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }



    public class ProfileNavigationItemModel: INotifyPropertyChanged
    {

        private string title;
        private MaterialDesignThemes.Wpf.PackIconKind icon;
        private bool detailsProvided;

        public string Title { get { return title;} set { title = value; RaisePropertyChanged(); } }
        public MaterialDesignThemes.Wpf.PackIconKind Icon { get { return icon; } set { icon = value; RaisePropertyChanged(); } }
        public bool DetailsProvided { get { return detailsProvided; } set { detailsProvided = value; RaisePropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
