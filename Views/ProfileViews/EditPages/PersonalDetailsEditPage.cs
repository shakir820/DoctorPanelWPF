﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Markup;

namespace DoctorPanelWPF.Views.ProfileViews.EditPages
{
    public partial class PersonalDetailsEditPage
    {
        private string ConvertRTF_to_HTML_string(FlowDocument rtf)
        {
            try
            {
                string htmlStr = markupConverter.ConvertRtfToHtml(FlowDocument_GetText(rtf));
                HtmlAgilityPack.HtmlDocument htmldoc = new HtmlAgilityPack.HtmlDocument();
                htmldoc.LoadHtml(htmlStr);
                var rootNode = htmldoc.DocumentNode;
                return rootNode.FirstChild.InnerHtml;
            }
            catch
            {
                return null;
            }
        }








        private static FlowDocument SetRTF(string xamlString)
        {
            StringReader stringReader = new StringReader(xamlString);
            System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
            var sec = XamlReader.Load(xmlReader) as FlowDocument;
            return sec;
        }









        public static string FlowDocument_GetText(FlowDocument fd)
        {
            TextRange tr = new TextRange(fd.ContentStart, fd.ContentEnd);
            return tr.Text;
        }
    }
}
