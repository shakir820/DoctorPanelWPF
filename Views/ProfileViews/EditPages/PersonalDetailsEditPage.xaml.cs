﻿using DoctorPanelWPF.ContentDialogues;
using DoctorPanelWPF.Models;
using DoctorPanelWPF.Models.CommunicateWithServer;
using MarkupConverter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using HtmlToXamlConverter = HTMLConverter.HtmlToXamlConverter;

namespace DoctorPanelWPF.Views.ProfileViews.EditPages
{
    /// <summary>
    /// Interaction logic for PersonalDetailsEditPage.xaml
    /// </summary>
    public partial class PersonalDetailsEditPage : Page, INotifyPropertyChanged
    {
        public static PersonalDetailsEditPage Current;
        public PersonalDetailsEditPage()
        {
            InitializeComponent();
            Current = this;
            markupConverter = new MarkupConverter.MarkupConverter();
            DataContext = this;
        }



        #region private fields
        private UserModel user_data;
        private string user_name;
        private int genderIndex;
        private string email;
        private string mobile;
        private string telephone;
        private FlowDocument address;
        private FlowDocument engCardView;
        private FlowDocument bngCardView;
        private IMarkupConverter markupConverter;
        private bool UpdatingfromServer = true;
        private long? divisionID;
        private long? districtID;
        private string divisionName;
        private string districtName;
        private bool Address_Changed = false;
        private bool EngCardView_Changed = false;
        private bool BngCardView_Changed = false;
        private ObservableCollection<Models.API_Data.Division> _divisions = new ObservableCollection<Models.API_Data.Division>();
        private ObservableCollection<Models.API_Data.District> _districts = new ObservableCollection<Models.API_Data.District>();
        #endregion




        #region public properties
        public ObservableCollection<Models.API_Data.Division> Divisions { get { return _divisions; } set { _divisions = value; RaisePropertyChanged(); } }
        public ObservableCollection<Models.API_Data.District> Districts { get { return _districts; } set { _districts = value; RaisePropertyChanged(); } }

        public string User_Name
        {
            get { return user_name; }
            set
            {
                user_name = value;
                if(UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        private void EnableSaveButton()
        {
            SaveBtn.IsEnabled = true;
        }

        public string DivisionName
        {
            get { return divisionName; }
            set
            {
                divisionName = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public string DistricName
        {
            get { return districtName; }
            set
            {
                districtName = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public long? DivisionID
        {
            get { return divisionID; }
            set
            {
                divisionID = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public long? DistricID
        {
            get { return districtID; }
            set
            {
                districtID = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public int GenderIndex
        {
            get { return genderIndex; }
            set
            {
                genderIndex = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public string Mobile
        {
            get { return mobile; }
            set
            {
                mobile = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public string Telephone
        {
            get { return telephone; }
            set
            {
                telephone = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public UserModel User_Data
        {
            get { return user_data; }
            set
            {
                user_data = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public FlowDocument Address
        {
            get { return address; }
            set
            {
                address = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public FlowDocument EngCardView
        {
            get { return engCardView; }
            set
            {
                engCardView = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }

        public FlowDocument BngCardView
        {
            get { return bngCardView; }
            set
            {
                bngCardView = value;
                if (UpdatingfromServer == false)
                {
                    EnableSaveButton();
                }
                RaisePropertyChanged();
            }
        }
        #endregion











        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Populate_DataAsync(User_Data);
        }







        public async void Populate_DataAsync(UserModel userModel)
        {
            User_Name = userModel.Name;

            switch (userModel.Gender)
            {
                case "male":
                    GenderIndex = 0;
                    break;

                case "female":
                    GenderIndex = 1;
                    break;

                case "other":
                    GenderIndex = 2;
                    break;

                default:
                    GenderIndex = 0;
                    break;
            }

            GenderComboBox.SelectedIndex = GenderIndex;

            Mobile = userModel.PractitionerInfo.Mobile;
            Telephone = userModel.PractitionerInfo.Mobile2;

            Email = userModel.Email;

            if (userModel.Address != null)
            {
                var htmlAddress = userModel.Address;
                var xamlAddress = HtmlToXamlConverter.ConvertHtmlToXaml(htmlAddress, true);
                Address = SetRTF(xamlAddress);
                AddressTextBox.Document = Address;
            }

            if (userModel.ShortBio != null)
            {
                var shortBio = (string)userModel.ShortBio;
                shortBio = HtmlToXamlConverter.ConvertHtmlToXaml(shortBio, true);
                EngCardView = SetRTF(shortBio);
                EngCardViewTextBox.Document = EngCardView;
            }

            if (userModel.ShortBioBn != null)
            {
                var shortBio = (string)userModel.ShortBioBn;
                shortBio = HtmlToXamlConverter.ConvertHtmlToXaml(shortBio, true);
                BngCardView = SetRTF(shortBio);
                BngCardViewTextBox.Document = BngCardView;
            }

            //get divisions
            Thread thread = new Thread(GetDivisionDataAsync);
            thread.Start();

            AddressTextBox.TextChanged += AddressTextBox_TextChanged;
            EngCardViewTextBox.TextChanged += EngCardViewTextBox_TextChanged;
            BngCardViewTextBox.TextChanged += BngCardViewTextBox_TextChanged;

            UpdatingfromServer = false;
        }








        private async void GetDivisionDataAsync()
        {
            var divisions = await mDaktarServer.GetDivisionAsync();
            if (divisions != null)
            {
                Dispatcher.Invoke(() => { Divisions = divisions; });
                if (User_Data.DivisionId != null && User_Data.DivisionId != 0)
                {
                    var Selected_div = Divisions.SingleOrDefault(a => a.DivisionId == User_Data.DivisionId);
                    try
                    {
                        Dispatcher.Invoke(() => { DivisionComboBox.SelectedIndex = Divisions.IndexOf(Selected_div); });
                        GetDistrictDataAsync(User_Data.DivisionId);
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        Dispatcher.Invoke(() =>
                        {
                            DivisionComboBox.SelectedIndex = -1;
                            DistrictComboBox.SelectedIndex = -1;
                        });
                    }
                    catch { }
                }
            }
            DivisionComboBox.SelectionChanged += DivisionComboBox_SelectionChanged;
            DistrictComboBox.SelectionChanged += DistrictComboBox_SelectionChanged;
        }











        private void DistrictComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }

        private async void GetDistrictDataAsync(long? id)
        {
            if (id != null && id != 0)
            {
                var districts = await mDaktarServer.GetDistricAsync((long)id);
                if (districts != null)
                {
                    Dispatcher.Invoke(() => { Districts = districts; });
                    //Dispatcher.Invoke(() => { DistrictComboBox.ItemsSource = Districts; });
                    if (User_Data.DistrictId != null && User_Data.DistrictId != 0)
                    {
                        try
                        {
                            var dis = Districts.SingleOrDefault(a => a.DistrictId == User_Data.DistrictId);
                            Dispatcher.Invoke(() => { DistrictComboBox.SelectedIndex = Districts.IndexOf(dis); });
                        }
                        catch { }
                    }
                }
                else
                {

                }
            }
            else
            {
                Dispatcher.Invoke(() => { DistrictComboBox.SelectedIndex = -1; });
            }
        }





       

       

       

        private void AddressTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Address_Changed = true;
            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }

        private void BngCardViewTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            BngCardView_Changed = true;
            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }

        private void EngCardViewTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            EngCardView_Changed = true;
            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }

        private void GenderComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GenderIndex = GenderComboBox.SelectedIndex;
            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }












        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (User_Name != null && User_Name != "" & Email != null && Email != "")
            {
                if (IsValidEmail(Email))
                {
                    Thread updateThread = new Thread(UpdateProfileInfoAsync);
                    updateThread.Start();
                }
            }
        }






        private async void UpdateProfileInfoAsync()
        {
            Dispatcher.Invoke(() => { MainWindow.Current.StartLoadingAsync(); });
            string htmlStr_address = "";
            string htmlStr_engCardView = "";
            string htmlStr_bngCardView = "";

            Dispatcher.Invoke(() => 
            {
                htmlStr_address = ConvertRTF_to_HTML_string(AddressTextBox.Document);
                htmlStr_engCardView = ConvertRTF_to_HTML_string(EngCardViewTextBox.Document);
                htmlStr_bngCardView = ConvertRTF_to_HTML_string(BngCardViewTextBox.Document);
            });

            string gender;
            switch (GenderIndex)
            {
                case 0:
                    gender = "male";
                    break;

                case 1:
                    gender = "female";
                    break;

                case 2:
                    gender = "other";
                    break;

                default:
                    gender = "male";
                    break;
            }

            Models.API_Data.Division div = null;
            Models.API_Data.District dis = null;



            Dispatcher.Invoke(() => 
            {
                if(DivisionComboBox.SelectedIndex != -1)
                {
                    div = DivisionComboBox.SelectedValue as Models.API_Data.Division;
                }
                else
                {
                    div = null;
                }
                if(DistrictComboBox.SelectedIndex != -1)
                {
                    dis = DistrictComboBox.SelectedValue as Models.API_Data.District;
                }
                else
                {
                    dis = null;
                }
            });

            string div_name;
            string div_id;
            string dis_name;
            string dis_id;

            if(div == null)
            {
                div_name = "";
                div_id = "";
            }
            else
            {
                div_name = div.DivisionName;
                div_id = div.DivisionId.ToString();
            }

            if(dis != null)
            {
                dis_name = dis.DistrictName;
                dis_id = dis.DistrictId.ToString(); ;
            }
            else
            {
                dis_name = "";
                dis_id = "";
            }

            string update_uri = "http://api.healthsheba.com/api/v2/practitioner/profile/update/personal-information?api_token=" + User_Data.AccessToken;

            var values = new Dictionary<string, string>
            {
                { "name", User_Name },
                { "gender", gender },
                { "email", Email },
                { "mobile", Mobile},
                { "mobile_2", Telephone},
                { "division_name", div_name },
                { "division_id", div_id },
                { "division", div_id },
                { "district_name", dis_name },
                { "district_id", dis_id },
                { "district", dis_id },
                { "short_bio", htmlStr_engCardView },
                { "short_bio_bn", htmlStr_bngCardView },
                { "address", htmlStr_address }
            };


            var content = new FormUrlEncodedContent(values);

            var gg = await mDaktarServer.UpdateProfileData(update_uri, content);
            Dispatcher.Invoke(() => 
            {
                LoadingDialog.Current.CloseDialog();
                Dashboard.Current.GoBack();
            });
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoBack();
        }









        private void DivisionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var div = DivisionComboBox.SelectedValue as Models.API_Data.Division;
            Thread thread = new Thread(RefreshDistricsAsync);
            thread.Start(div.DivisionId);

            DivisionID = div.DivisionId;
            DivisionName = div.DivisionName;

            if (UpdatingfromServer == false)
            {
                EnableSaveButton();
            }
        }








        public async void RefreshDistricsAsync(object id)
        {
            if ((long)id != 0)
            {
                var districts = await mDaktarServer.GetDistricAsync((long)id);
                if (districts != null)
                {
                    Dispatcher.Invoke(() => { Districts = districts; });
                }
            }
        }














        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }










        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(NameTextBox.Text == "")
            {
                NameTextBox.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            {
                NameTextBox.BorderBrush = new SolidColorBrush(Colors.Gray);
            }
        }










        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }










        private void EmailTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (EmailTextBox.Text == "")
            {
                EmailTextBox.BorderBrush = new SolidColorBrush(Colors.Red);
            }
            else
            {
                if (IsValidEmail(EmailTextBox.Text))
                {
                    EmailTextBox.BorderBrush = new SolidColorBrush(Colors.Gray);
                }
                else
                {
                    EmailTextBox.BorderBrush = new SolidColorBrush(Colors.Red);
                }
            }
        }
    }
}
