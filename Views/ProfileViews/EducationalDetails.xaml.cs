﻿using DoctorPanelWPF.ContentDialogues;
using DoctorPanelWPF.Models;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views
{
    /// <summary>
    /// Interaction logic for EducationalDetails.xaml
    /// </summary>
    public partial class EducationalDetails : Page
    {
        public EducationDetails educationDetails;

        public EducationalDetails()
        {
            InitializeComponent();
            educationDetails = EducationDetails.Current;

        }

        

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
           MainWindow.Current.ExecuteRunDialog();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            degreeListView.ItemsSource = educationDetails.Degrees;
        }


       

        private void ClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
           // throw new NotImplementedException();
        }

        private void DegreeListView_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            int wheel_count = e.Delta;

            if(wheel_count > 0)
            {

            }

            
        }
    }
}
