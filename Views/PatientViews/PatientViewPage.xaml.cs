﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.PatientViews
{
    /// <summary>
    /// Interaction logic for PatientViewPage.xaml
    /// </summary>
    public partial class PatientViewPage : Page
    {
        public static PatientViewPage Current;
        public ObservableCollection<PatientModel> Patients = new ObservableCollection<PatientModel>();

        public PatientViewPage()
        {
            InitializeComponent();
            Current = this;
            PatientListView.ItemsSource = Patients;
            GenerateSomePatient();
        }

        private void AddPatientBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.AddNewPatient();
        }

        public void GenerateSomePatient()
        {
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271"});
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
            Patients.Add(new PatientModel() { PatientName = "Shakir Ahmed", Age = 25, Gender = Gender.Male, Mobile = "+8801670074271" });
        }
    }
}
