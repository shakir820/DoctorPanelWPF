﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.PatientViews
{
    /// <summary>
    /// Interaction logic for CreateNewPrescriptionPage.xaml
    /// </summary>
    public partial class CreateNewPrescriptionPage : Page, INotifyPropertyChanged
    {
        public CreateNewPrescriptionPage()
        {
            InitializeComponent();
            Presciption = new PrescriptionModel();
            DataContext = this;
        }

        private PrescriptionModel _presciption;
        public PrescriptionModel Presciption
        {
            get { return _presciption; }
            set { _presciption = value; RaisePropertyChanged(); }
        }

        private void AddComplainBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowAddNewComplainDialog(Presciption);
        }




        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ChiefComplainListView.ItemsSource = Presciption.ChiefComplains;
            ChronicDiseaseListView.ItemsSource = Presciption.ChronicDiseases;
            PastMedicalHistoryListView.ItemsSource = Presciption.PastMedicalsHistory;
            AllergyListView.ItemsSource = Presciption.Allergies;
            FamilyHistoryListView.ItemsSource = Presciption.FamilyHistory;
            InvestigationListView.ItemsSource = Presciption.Investigations;
            DiagnosisListView.ItemsSource = Presciption.DiagnosisList;
            SelectedMedicineListView.ItemsSource = Presciption.Medicines;
            SelectedAdviceListView.ItemsSource = Presciption.Advices;
        }

        private void ChronicDiseaseAddBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowChronicDiseaseDialog(Presciption);
        }

        private void MedicalHisotyAddBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowPastMedicalHitoryDialog(Presciption);
        }

        private void ExaminationEditBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowOnExaminationDialog(Presciption);
        }

        private void AllergyAddBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowAllergyDialog(Presciption);
        }

        private void FamilyHistoryAddBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowFamilyHistoryDialog(Presciption);
        }

        private void ChiefComplainListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {

        }

        private void NestedListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var unit = e.Delta;
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - unit);
        }

        private void LabTestAddBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowInvestigationLabTestDialog(Presciption);
        }

        private void AddDiagnosisBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowDiagnosisDialog(Presciption);
        }

        private void AddMedicineBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowAddMedicineDialog(Presciption);
        }

        private void AddAdviceBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowAdviceDialog(Presciption);
        }
    }
}
