﻿using DoctorPanelWPF.LoginAndRegister;
using DoctorPanelWPF.Models;
using DoctorPanelWPF.Views.ChamberViews;
using DoctorPanelWPF.Views.ChamberViews.SubViewPages;
using DoctorPanelWPF.Views.OnlineConsultationViews;
using DoctorPanelWPF.Views.PatientViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Page, INotifyPropertyChanged
    {
        public static Dashboard Current;

        public Dashboard()
        {
            InitializeComponent();
            Current = this;
            DataContext = this;
        }

        private UserModel _user;

        public UserModel User_Data
        {
            get { return _user; }
            set { _user = value;  RaisePropertyChanged(); }
        }

        public void GoToRXPage(PrescriptionModel prescriptionModel)
        {
            frame.Navigate(new CreateNewPrescriptionPage());
            //we will give you more info later.
        }

        private void Chip_Click(object sender, RoutedEventArgs e)
        {
            popupBox.IsPopupOpen = popupBox.IsPopupOpen ? false : true;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            User_Data = MainWindow.Current.UserData;
            ProfilePage profilePage = new ProfilePage();
            frame.Navigate(profilePage);
        }

        public bool GoBack()
        {
            if (frame.NavigationService.CanGoBack)
            {
                frame.NavigationService.GoBack();
                return true;
            }
            else return false;
        }

        public void GoToScheduleEditPage()
        {
            frame.Navigate(new BatchReschedulePage());
        }


        public void GoToSchedulePage(ChamberModel chamberModel)
        {
            frame.Navigate(new ScheduleDetailsPage());
            ScheduleDetailsPage.Current.scheduleModels = chamberModel.ChamberSchedules;
            ScheduleDetailsPage.Current.Chamber = chamberModel;

        }

        public void GotoChamberDetailsPage(ChamberModel chamber)
        {
            frame.Navigate(new ChamberDetailsPage());
            ChamberDetailsPage.Current.PopulateChamberValues(chamber);
        }

        public void GotoChamberEditPage(ChamberModel chamber)
        {
            frame.Navigate(new EditChamberPage(), chamber);
            EditChamberPage.Current.Chamber = chamber;
            EditChamberPage.Current.PopulateData();
        }

        public void GoToCreateNewSchedulePage(ChamberModel chamber)
        {
            frame.Navigate(new CreateNewSchedulePage());
            CreateNewSchedulePage.Current.chamber = chamber;
        }


        public void GoToScheduleEditPage(ScheduleModel scheduleModel, ChamberModel chamber)
        {
            var view = new ScheduleEditPage();
            view.BatchSchedule = scheduleModel;
            view.chamber = chamber;
            frame.Navigate(view);
        }

        public void GotoChamberDetailsPage()
        {
            frame.Navigate(new ChamberDetailsPage());
        }

        public void GotoChamberViewPage()
        {
            frame.Navigate(new ChamberViewPage());
        }

        public void GotoCreateNewChamberPage()
        {
            frame.Navigate(new CreateNewChamber());
        }

        public void AddNewPatient()
        {
            frame.Navigate(new AddNewPatientPage());
        }

        private void Lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = lb.SelectedItem as ListBoxItem;

            switch (item.Tag)
            {
                case "Dashboard":
                    frame.NavigationService.Navigate(new DashboardView());
                    break;
                case "Profile":
                    frame.NavigationService.Navigate(new ProfilePage());
                    break;
                case "Patient":
                    frame.NavigationService.Navigate(new PatientViewPage());
                    break;
                case "OnlineConsultation":
                    frame.NavigationService.Navigate(new OnlineConsultation());
                    break;
                case "AnimationTest":
                    frame.NavigationService.Navigate(new AnimationTest());
                    break;
                case "Chamber":
                    frame.NavigationService.Navigate(new ChamberViewPage());
                    break;
            }
        }

        private void EditProfileBtn_Click(object sender, RoutedEventArgs e)
        {

        }

       

        private void LogoutBtn_Click(object sender, RoutedEventArgs e)
        {
            var loginWindow = new LoginWindow();
            loginWindow.Show();
            MainWindow.Current.Close();
        }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
