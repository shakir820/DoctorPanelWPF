﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for ChamberDetailsPage.xaml
    /// </summary>
    public partial class ChamberDetailsPage : Page, INotifyPropertyChanged
    {
        public static ChamberDetailsPage Current;
        public ChamberModel _chamber;



        #region General Details
        public string _name;
        public string _address;
        public DateTime? _openingTime;
        public DateTime? _closingTime;
        public bool _pending;
        public string _division;
        public string _district;
        public string _area;
        public string _description;
        public Uri _coverIamge;
        public ChamberType _chamberType;

        public string ChamberName { get { return _name; } set { _name = value; RaisePropertyChanged(); } }
        public string Address { get { return _address; } set { _address = value; RaisePropertyChanged(); } }
        public DateTime? OpeningTime { get { return _openingTime; } set { _openingTime = value; RaisePropertyChanged(); } }
        public DateTime? ClosingTime { get { return _closingTime; } set { _closingTime = value; RaisePropertyChanged(); } }
        public bool Pending { get { return _pending; } set { _pending = value; RaisePropertyChanged(); } }
        public string Description { get { return _description; } set { _description = value; RaisePropertyChanged(); } }
        public Uri CoverImage { get { return _coverIamge; } set { _coverIamge = value; RaisePropertyChanged(); } }
        public ChamberType Chamber_Type { get { return _chamberType; } set { _chamberType = value; RaisePropertyChanged(); } }
        public string Division { get { return _division; } set { _division = value; RaisePropertyChanged(); } }
        public string District { get { return _district; } set { _district = value; RaisePropertyChanged(); } }
        public string Area { get { return _area; } set { _area = value; RaisePropertyChanged(); } }
        public ObservableCollection<PhoneNumberModel> PersonalNumbers;
        public ObservableCollection<PhoneNumberModel> OfficeNumbers;
        public ObservableCollection<PhoneNumberModel> SerialNumbers;

        public string PersonalNumberViewModel
        {
            get
            {
                if (PersonalNumbers.Count > 0)
                {
                    if (PersonalNumbers[0].PhoneNumber != "")
                    {
                        return PersonalNumbers[0].PhoneNumber;
                    }
                    else return "";

                }
                else return "";
            }
        }
        #endregion


        #region Fees Details
        public int _newVisitPrice;
        public int _returnVisitPrice;
        public int _reportVisitPrice;

        public int NewVisitPrice { get { return _newVisitPrice; } set { _newVisitPrice = value; RaisePropertyChanged(); } }
        public int ReturnVisitPrice { get { return _returnVisitPrice; } set { _returnVisitPrice = value; RaisePropertyChanged(); } }
        public int ReportVisitPrice { get { return _reportVisitPrice; } set { _reportVisitPrice = value; RaisePropertyChanged(); } }
        #endregion


        public ChamberDetailsPage()
        {
            InitializeComponent();   
            Current = this;
            DataContext = this;
        }

      
        public ChamberModel Chamber
        {
            get { return _chamber; }
            set { _chamber = value; RaisePropertyChanged(); }
        }


        private void PersonalContactNumberListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var unit = e.Delta;
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - unit);
        }


        public void PopulateChamberValues(ChamberModel chamber)
        {
            Chamber = chamber;
            ChamberName = chamber.Name;
            Chamber_Type = chamber.Chamber_Type;
            Address = chamber.Address;
            Area = chamber.Area;
            ClosingTime = chamber.ClosingTime;

            if (chamber.CoverImage != null)
            {
                CoverImage = chamber.CoverImage;
                ChamberImage.ImageSource = new BitmapImage( CoverImage);
            }
            else ChamberImage.ImageSource = new BitmapImage(new Uri(@"pack://application:,,,/Resources/placeholder-image.jpg"));

            CoverImage = chamber.CoverImage;
            Description = chamber.Description;
            District = chamber.District;
            Division = chamber.Division;
            NewVisitPrice = chamber.NewVisitPrice;
            OpeningTime = chamber.OpeningTime;
            Pending = chamber.Pending;
            ReportVisitPrice = chamber.ReportVisitPrice;
            ReturnVisitPrice = chamber.ReturnVisitPrice;
            SerialNumbers = chamber.SerialNumbers;
            PersonalNumbers = chamber.PersonalNumbers;
            OfficeNumbers = chamber.OfficeNumbers;
            PersonalContactNumberListView.ItemsSource = PersonalNumbers;
            OfficeContactNumberListView.ItemsSource = OfficeNumbers;
            SerialContactNumberListView.ItemsSource = SerialNumbers;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void ScheduleDetailsBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoToSchedulePage(Chamber);
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GotoChamberEditPage(Chamber);
        }
    }
}
