﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for EditChamberPage.xaml
    /// </summary>
    public partial class EditChamberPage : Page, INotifyPropertyChanged
    {
      
        private int _chamberTypeIndex;
        private BitmapImage _chamberCoverImage;
        private int _divisionIndex =1;
        private int _districtIndex = 1;
        private int _areaIndex = 1;
        private ChamberModel _chamber;

        public ChamberModel Chamber { get { return _chamber; } set { _chamber = value; RaisePropertyChanged(); } }
        public int ChamberTypeIndex { get { return _chamberTypeIndex; } set { _chamberTypeIndex = value; RaisePropertyChanged(); } }
        public int DivisionIndex { get { return _divisionIndex; } set { _divisionIndex = value; RaisePropertyChanged(); } }
        public int DistrictIndex { get { return _districtIndex; } set { _districtIndex = value; RaisePropertyChanged(); } }
        public int AreaIndex { get { return _areaIndex; } set { _areaIndex = value; RaisePropertyChanged(); } }

        public static EditChamberPage Current;

        public EditChamberPage()
        {
            InitializeComponent();
            Current = this;
            DataContext = this;
        }

      
       

        private void ImagePickerBtn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
        
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {         
                string filename = dlg.FileName;
                Chamber.CoverImage = new Uri(dlg.FileName);
            }
        }

        public void PopulateData()
        {
            if (Chamber != null)
            {
                switch (Chamber.Chamber_Type)
                {
                    case ChamberType.Personal:
                        ChamberTypeIndex = 0;
                        break;

                    case ChamberType.Rent:
                        ChamberTypeIndex = 1;
                        break;

                    case ChamberType.Government:
                        ChamberTypeIndex = 2;
                        break;

                    default:
                        ChamberTypeIndex = 3;
                        break;
                }
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            PersonalContactList.PhoneNumberItemsSource = Chamber.PersonalNumbers;
            OfficeContactList.PhoneNumberItemsSource = Chamber.OfficeNumbers;
            SerialContactList.PhoneNumberItemsSource = Chamber.SerialNumbers;

            PopulateData();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoBack();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ChamberNameTextBox.Text == null || ChamberNameTextBox.Text == "")
            {
                MainWindow.Current.ShowWarningDialog("Please Define Your Chamber Name");
            }
            else
            {
                Chamber.Chamber_Type = ChamberType.Personal;


                List<PhoneNumberModel> RemoveNumbersList = new List<PhoneNumberModel>();

                #region for personal numbers
                foreach (var number in Chamber.PersonalNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach (var num in RemoveNumbersList)
                {
                    Chamber.PersonalNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if (Chamber.PersonalNumbers.Count == 0)
                {
                    Chamber.PersonalNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.PersonalNumbers });
                }
                #endregion


                #region for office numbers
                foreach (var number in Chamber.OfficeNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach (var num in RemoveNumbersList)
                {
                    Chamber.OfficeNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if (Chamber.OfficeNumbers.Count == 0)
                {
                    Chamber.OfficeNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.OfficeNumbers });
                }
                #endregion



                #region for serial numbers
                foreach (var number in Chamber.SerialNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach (var num in RemoveNumbersList)
                {
                    Chamber.SerialNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if (Chamber.SerialNumbers.Count == 0)
                {
                    Chamber.SerialNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.SerialNumbers });
                }
                #endregion


             

                if (DivisionComboBox.SelectedIndex != -1)
                {
                    Chamber.Division = ((ComboBoxItem)DivisionComboBox.SelectedValue).Tag.ToString();
                }
                if (DistrictComboBox.SelectedIndex != -1)
                {
                    Chamber.District = ((ComboBoxItem)DistrictComboBox.SelectedValue).Tag.ToString();
                }
                if (AreaComboBox.SelectedIndex != -1)
                {
                    Chamber.Area = ((ComboBoxItem)AreaComboBox.SelectedValue).Tag.ToString();
                }

                Dashboard.Current.GoBack();
            }
        }


        private void PersonalContactNumberListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var unit = e.Delta;
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - unit);
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
