﻿using DoctorPanelWPF.Models;
using DoctorPanelWPF.Views.ChamberViews.SubViewPages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for ScheduleDetailsPage.xaml
    /// </summary>
    public partial class ScheduleDetailsPage : Page
    {
        public static ScheduleDetailsPage Current;
        public ChamberModel Chamber;
        public ObservableCollection<ScheduleModel> scheduleModels;

        public ScheduleDetailsPage()
        {
            InitializeComponent();
            Current = this;
        }

        

        int viewId = 0;

        private void BatchBtn_Click(object sender, RoutedEventArgs e)
        {
            if (viewId == 1)
            {
                //was in batch view 
                BatchBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFE4E4E4"));
                BatchBtn.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF616161"));

                CalendarBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF673AB7"));
                CalendarBtn.Foreground = new SolidColorBrush(Colors.White);
            }
            else
            {
                //was in calendar view
                BatchBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFE4E4E4"));
                BatchBtn.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF616161"));

                CalendarBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF673AB7"));
                CalendarBtn.Foreground = new SolidColorBrush(Colors.White);
            }
            viewId = 1;
            ViewFrame.Navigate(new BatchPage());
            BatchPage.Current.Chamber = Chamber;
        }

        private void CalendarBtn_Click(object sender, RoutedEventArgs e)
        {
            if(viewId == 1)
            {
                //was in batch view
                
                //for calendar btn
                CalendarBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFE4E4E4"));
                CalendarBtn.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF616161"));
                //for batch btn
                BatchBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF673AB7"));
                BatchBtn.Foreground = new SolidColorBrush(Colors.White);

                
            }
            else
            {
                //was in calendar view

                //for calendar btn
                CalendarBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFE4E4E4"));
                CalendarBtn.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF616161"));
                //for batch btn
                BatchBtn.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF673AB7"));
                BatchBtn.Foreground = new SolidColorBrush(Colors.White);
            }

            viewId = 0;
        }

        private void NewScheduleBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoToCreateNewSchedulePage(Chamber);
        }
    }
}
