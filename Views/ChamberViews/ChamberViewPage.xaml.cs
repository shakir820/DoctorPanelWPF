﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for ChamberViewPage.xaml
    /// </summary>
    public partial class ChamberViewPage : Page
    {
        public ChambersViewModel chambersViewModel;
        public ChamberViewPage()
        {
            InitializeComponent();
            GenerateData();
        }

       

        public void GenerateData()
        {
            if(MainWindow.Current.chambersViewModel == null)
            {
                MainWindow.Current.chambersViewModel = new ChambersViewModel();
                chambersViewModel = MainWindow.Current.chambersViewModel;
            }
            else
            {
                chambersViewModel = MainWindow.Current.chambersViewModel;
            }
            
        }

        #region don't mess with this region
        private void ChamberListView_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //var changedUnit = e.Delta;
            //// Get the border of the listview (first child of a listview)
            //Decorator border = VisualTreeHelper.GetChild(ChamberListView, 0) as Decorator;

            //// Get scrollviewer
            //ScrollViewer scrollViewer = border.Child as ScrollViewer;
            //DoubleAnimation offsetAnimation = new DoubleAnimation();
            //offsetAnimation.To = scrollViewer.VerticalOffset + changedUnit;
            //offsetAnimation.Duration = TimeSpan.FromSeconds(0.5);
            //scrollViewer.BeginAnimation(ScrollViewer.VerticalOffsetProperty, offsetAnimation);
        }

        private void ChamberListView_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //var changedUnit = e.HorizontalChange;
            //// Get the border of the listview (first child of a listview)
            ////Decorator border = VisualTreeHelper.GetChild(ChamberListView, 0) as Decorator;

            //// Get scrollviewer
            //if (changedUnit > 0)
            //{
            //    ScrollViewer scrollViewer = sender as ScrollViewer;
            //    DoubleAnimation offsetAnimation = new DoubleAnimation();
            //    offsetAnimation.To = scrollViewer.VerticalOffset + changedUnit;
            //    offsetAnimation.Duration = TimeSpan.FromSeconds(0.5);
            //    scrollViewer.BeginAnimation(ScrollViewer.VerticalOffsetProperty, offsetAnimation);
            //}
        }
        #endregion

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ChamberListView.ItemsSource = ChambersViewModel.Current.Chambers;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NewChamberCreateBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GotoCreateNewChamberPage();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
