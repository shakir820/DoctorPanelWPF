﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for CreateNewChamberPage2.xaml
    /// </summary>
    public partial class CreateNewChamberPage2 : Page, INotifyPropertyChanged
    {
        public static CreateNewChamberPage2 Current;
        public BitmapImage ChamberCoverImage = null;
        public ChamberModel _chamber;
        public ChamberModel Chamber { get { return _chamber; } set { _chamber = value; RaisePropertyChanged(); } }
        #region General Details
        public ChamberType _chamberType;
        public ChamberType Chamber_Type { get { return _chamberType; } set { _chamberType = value; } }
        #endregion





        public CreateNewChamberPage2()
        {
            InitializeComponent();
            Current = this;
            Chamber = new ChamberModel();
            DataContext = this;
        }


    




     



        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if(Chamber == null)
            {
                Chamber = new ChamberModel();
            }

            PersonalContactList.PhoneNumberItemsSource = Chamber.PersonalNumbers;
            OfficeContactList.PhoneNumberItemsSource = Chamber.OfficeNumbers;
            SerialContactList.PhoneNumberItemsSource = Chamber.SerialNumbers;
        }




        private void PersonalContactNumberListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var unit = e.Delta;
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - unit);
            //e.Handled = false;
        }




        private void ImagePickerBtn_Click(object sender, RoutedEventArgs e)
        {
           
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            //dlg.FileName = "Document"; // Default file name
            /*dlg.DefaultExt = ".png"; */// Default file extension
            //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                //var fileSteam = dlg.OpenFile();
                CoverImage.ImageSource = new BitmapImage(new Uri(dlg.FileName));
                Chamber.CoverImage = new Uri(dlg.FileName);
            }
            else
            {
             
            }
        }



        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoBack();
        }



        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {

            //ChamberModel chamber = new ChamberModel();
            if (ChamberNameTextBox.Text == null || ChamberNameTextBox.Text == "")
            {
                MainWindow.Current.ShowWarningDialog("Please Define Your Chamber Name");
            }
            else
            {
                //chamber.Name = ChamberNameTextBox.Text;
                //chamber.Address = string.IsNullOrEmpty(AddressTextBox.Text) ? "" : AddressTextBox.Text;
                Chamber.Chamber_Type = ChamberType.Personal;
             

                //chamber.Description = string.IsNullOrEmpty(DescriptionTextBox.Text) ? "" : DescriptionTextBox.Text;

                int NewVisitPrice;
                if (int.TryParse(NewVisitPriceTextBox.Text, out NewVisitPrice))
                {
                    Chamber.NewVisitPrice = NewVisitPrice;
                }
                else Chamber.NewVisitPrice = 0;

                List<PhoneNumberModel> RemoveNumbersList = new List<PhoneNumberModel>();

                #region for personal numbers
                foreach (var number in Chamber.PersonalNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach(var num in RemoveNumbersList)
                {
                    Chamber.PersonalNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if(Chamber.PersonalNumbers.Count == 0)
                {
                    Chamber.PersonalNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.PersonalNumbers});
                }
                #endregion


                #region for office numbers
                foreach (var number in Chamber.OfficeNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach (var num in RemoveNumbersList)
                {
                    Chamber.OfficeNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if (Chamber.OfficeNumbers.Count == 0)
                {
                    Chamber.OfficeNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.OfficeNumbers });
                }
                #endregion



                #region for serial numbers
                foreach (var number in Chamber.SerialNumbers)
                {
                    var phoneNumberModel = number as PhoneNumberModel;
                    if (string.IsNullOrEmpty(phoneNumberModel.PhoneNumber))
                    {
                        RemoveNumbersList.Add(phoneNumberModel);
                    }
                }
                foreach (var num in RemoveNumbersList)
                {
                    Chamber.SerialNumbers.Remove(num);
                }
                RemoveNumbersList = new List<PhoneNumberModel>();

                if (Chamber.SerialNumbers.Count == 0)
                {
                    Chamber.SerialNumbers.Add(new PhoneNumberModel() { Count = 1, Numbers = Chamber.SerialNumbers });
                }
                #endregion


                Chamber.Pending = true;

                int ReportVisitPrice;
                if (int.TryParse(ReportPriceTextBox.Text, out ReportVisitPrice))
                {
                    Chamber.ReportVisitPrice = ReportVisitPrice;
                }
                else Chamber.ReportVisitPrice = 0;

                int ReturnVisitPrice;
                if (int.TryParse(ReturnVisitPriceTextBox.Text, out ReturnVisitPrice))
                {
                    Chamber.ReturnVisitPrice = ReturnVisitPrice;
                }
                else Chamber.ReturnVisitPrice = 0;

                Chamber.ClosingTime = null;
                Chamber.OpeningTime = null;

                if (DivisionComboBox.SelectedIndex != -1)
                {
                    Chamber.Division = ((ComboBoxItem)DivisionComboBox.SelectedValue).Tag.ToString();
                }
                if (DistrictComboBox.SelectedIndex != -1)
                {
                    Chamber.District = ((ComboBoxItem)DistrictComboBox.SelectedValue).Tag.ToString();
                }
                if (AreaComboBox.SelectedIndex != -1)
                {
                    Chamber.Area = ((ComboBoxItem)AreaComboBox.SelectedValue).Tag.ToString();
                }

                ChambersViewModel.Current.Chambers.Add(Chamber);
                //ChambersViewModel.Current.AddNewChamberToDatabse(chamber);
                Dashboard.Current.GotoChamberViewPage();
            }
        }








        private void ChamberTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ChamberTypeComboBox.SelectedIndex;
            switch (index)
            {
                case 0:
                    Chamber_Type = ChamberType.Personal;
                    break;

                case 1:
                    Chamber_Type = ChamberType.Rent;
                    break;

                case 2:
                    Chamber_Type = ChamberType.Government;
                    break;

                case 3:
                    Chamber_Type = ChamberType.None;
                    break;

                default:
                    Chamber_Type = ChamberType.None;
                    break;
            }
        }

        private void DivisionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }




        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
