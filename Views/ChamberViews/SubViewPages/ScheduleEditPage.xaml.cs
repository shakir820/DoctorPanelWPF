﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews.SubViewPages
{
    /// <summary>
    /// Interaction logic for ScheduleEditPage.xaml
    /// </summary>
    public partial class ScheduleEditPage : Page, INotifyPropertyChanged
    {
        public ScheduleEditPage()
        {
            InitializeComponent();
            DataContext = this;
        }

        public ChamberModel chamber;
        public ScheduleModel BatchSchedule;
        private ScheduleModel _chamberSchedule;
        public ScheduleModel ChamberSchedule
        {
            get { return _chamberSchedule; }
            set { _chamberSchedule = value; RaisePropertyChanged(); }
        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ChamberSchedule = BatchSchedule;
            ChamberSchedule.Chamber = BatchSchedule.Chamber;
            EverydayCheckBox.Checked += EverydayCheckBox_Checked;
            SpecificDaysCheckBox.Checked += SpecificDaysCheckBox_Checked;

            if (ChamberSchedule != null)
            {
                //populate start date and end date
                PopulateDate(ChamberSchedule);

                //get Minute and Person
                PopulatePatientCountAndTime(ChamberSchedule);

                //Get CheckBox value
                GetCheckBoxesValue(ChamberSchedule);

                //populate specific days values
                PopulateWeekDaysValues(ChamberSchedule);
            }

            ChamberSchedule.WeekDayCollectionChanged += ChamberSchedule_WeekDayCollectionChanged;
        }

        private void ChamberSchedule_WeekDayCollectionChanged(object sender, WeekDayCountChangedEventArgs e)
        {
            if (e.count < 3)
            {
                EnableButtons(e.WeekDay);
            }
        }


        private void PopulateWeekDaysValues(ScheduleModel schedule) 
        {
            SatListView.ItemsSource = schedule.WeekDaysCollection[0].TimeSlots;
            if(schedule.WeekDaysCollection[0].TimeSlots.Count > 2) SatAddMoreSlotBtn.IsEnabled = false;

            SunListView.ItemsSource = schedule.WeekDaysCollection[1].TimeSlots;
            if (schedule.WeekDaysCollection[1].TimeSlots.Count > 2) SunAddMoreSlotBtn.IsEnabled = false;

            MonListView.ItemsSource = schedule.WeekDaysCollection[2].TimeSlots;
            if (schedule.WeekDaysCollection[2].TimeSlots.Count > 2) MonAddMoreSlotBtn.IsEnabled = false;

            TueListView.ItemsSource = schedule.WeekDaysCollection[3].TimeSlots;
            if (schedule.WeekDaysCollection[3].TimeSlots.Count > 2) TueAddMoreSlotBtn.IsEnabled = false;

            WedListView.ItemsSource = schedule.WeekDaysCollection[4].TimeSlots;
            if (schedule.WeekDaysCollection[4].TimeSlots.Count > 2) WedAddMoreSlotBtn.IsEnabled = false;

            ThuListView.ItemsSource = schedule.WeekDaysCollection[5].TimeSlots;
            if (schedule.WeekDaysCollection[5].TimeSlots.Count > 2) ThuAddMoreSlotBtn.IsEnabled = false;

            FriListView.ItemsSource = schedule.WeekDaysCollection[6].TimeSlots;
            if (schedule.WeekDaysCollection[6].TimeSlots.Count > 2) FriAddMoreSlotBtn.IsEnabled = false;




            foreach (var slot in schedule.WeekDaysCollection[0].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    SatCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[1].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    SunCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[2].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    MonCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[3].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    TueCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[4].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    WedCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[5].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    ThuCheckBox.IsChecked = true;
                    break;
                }
            }

            foreach (var slot in schedule.WeekDaysCollection[6].TimeSlots)
            {
                if (slot.UserDefined == true)
                {
                    FriCheckBox.IsChecked = true;
                    break;
                }
            }
        }

        private void EverydayCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)EverydayCheckBox.IsChecked)
            {
                SpecificDaysCheckBox.IsChecked = false;
                WeekendGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void SpecificDaysCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)SpecificDaysCheckBox.IsChecked)
            {
                EverydayCheckBox.IsChecked = false;
                WeekendGrid.Visibility = Visibility.Visible;
            }
        }

        private void GetCheckBoxesValue(ScheduleModel schedule)
        {
            if (schedule.EverydayDefinedTime)
            {
                EverydayCheckBox.IsChecked = true;
                SpecificDaysCheckBox.IsChecked = false;
            }
            else
            {
                EverydayCheckBox.IsChecked = false;
                SpecificDaysCheckBox.IsChecked = true;
            }
        }

        private void PopulatePatientCountAndTime(ScheduleModel schedule)
        {
          
            TimeSpan timeSpan = schedule.DefinedTimePerPatient;
            var min = timeSpan.Minutes;
            if (min == 5)
            {
                DefinedTimePerPatientComboBox.SelectedIndex = 0;
            }
            if (min == 10)
            {
                DefinedTimePerPatientComboBox.SelectedIndex = 1;
            }
            if (min == 15)
            {
                DefinedTimePerPatientComboBox.SelectedIndex = 2;
            }
            if (min == 20)
            {
                DefinedTimePerPatientComboBox.SelectedIndex = 3;
            }
            if (min == 30)
            {
                DefinedTimePerPatientComboBox.SelectedIndex = 4;
            }



            var patientCount = schedule.VisitPatientPerTimeSlot;

            if (patientCount == 1)
            {
                PatientCountComboBox.SelectedIndex = 0;
            }
            if (patientCount == 2)
            {
                PatientCountComboBox.SelectedIndex = 1;
            }
            if (patientCount == 3)
            {
                PatientCountComboBox.SelectedIndex = 2;
            }
        }

        private void PopulateDate(ScheduleModel scheduleModel)
        {
            StartDate.SelectedDate = scheduleModel.StartDate;
            EndDate.SelectedDate = scheduleModel.EndDate;
        }


        #region Button Enable or Disable
        public void EnableButtons(WeekDayModel wd)
        {
            switch (wd.WeekDayName)
            {
                case "Saturday":
                    SatAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Sunday":
                    SunAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Monday":
                    MonAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Tuesday":
                    TueAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Wednesday":
                    WedAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Thursday":
                    ThuAddMoreSlotBtn.IsEnabled = true;
                    break;

                case "Friday":
                    FriAddMoreSlotBtn.IsEnabled = true;
                    break;
            }
        }

        #endregion

        #region Add More Slot Button Click Events

        private void SatAddMoreSlotsBtnClick(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[0].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[0].TimeSlots.Add(
                    new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[0], ChamberSchedule.WeekDaysCollection[0].WeekDayName)
                    {
                        Count = count + 1,
                    });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }
        }

        private void SunAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[1].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[1].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[1], ChamberSchedule.WeekDaysCollection[1].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }

        }

        private void MonAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[2].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[2].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[2], ChamberSchedule.WeekDaysCollection[2].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }
        }

        private void TueAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[3].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[3].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[3], ChamberSchedule.WeekDaysCollection[3].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }
        }

        private void WedAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[4].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[4].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[4], ChamberSchedule.WeekDaysCollection[4].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }

        }

        private void ThuAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[5].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[5].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[5], ChamberSchedule.WeekDaysCollection[5].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }
        }

        private void FriAddMoreSlotBtn_Click(object sender, RoutedEventArgs e)
        {
            int count = ChamberSchedule.WeekDaysCollection[6].TimeSlots.Count;
            if (count < 3)
            {
                ChamberSchedule.WeekDaysCollection[6].TimeSlots.Add(new TimeModel(ChamberSchedule, ChamberSchedule.WeekDaysCollection[6], ChamberSchedule.WeekDaysCollection[6].WeekDayName)
                {
                    Count = count + 1,
                });

                if (count == 2)
                {
                    Button btn = sender as Button;
                    btn.IsEnabled = false;
                }
            }
        }

        #endregion








        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            //check for defined date
            var startDate = StartDate.SelectedDate;
            var endDate = EndDate.SelectedDate;
            bool SpecificDaysDefinedTime = false;
            bool EverydayDefinedTime = false;

            if (startDate != null && endDate != null)
            {
                ChamberSchedule.StartDate = StartDate.SelectedDate;
                ChamberSchedule.EndDate = EndDate.SelectedDate;
                if (((DateTime)endDate).CompareTo(((DateTime)startDate)) > 0)
                {
                    //valid time. check for main opening time and closing time
                    if (ChamberSchedule.UserDefined == true)
                    {
                        //get the time and check for further validation
                        if (EverydayCheckBox.IsChecked != true && SpecificDaysCheckBox.IsChecked == true)
                        {
                            //get the specific days' times
                            if (SatCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[0].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (SunCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[1].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (MonCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[2].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (TueCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[3].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (WedCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[4].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (ThuCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[5].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                            if (FriCheckBox.IsChecked == true)
                            {
                                foreach (var time in ChamberSchedule.WeekDaysCollection[6].TimeSlots)
                                {
                                    if (time.UserDefined == true)
                                    {
                                        //get the value;
                                        SpecificDaysDefinedTime = true;
                                    }
                                    else
                                    {
                                        //whatever
                                    }
                                }
                            }
                        }
                        else
                        {
                            //user defined the main time for everyday
                            EverydayDefinedTime = true;
                            SpecificDaysDefinedTime = false;
                        }

                        if (EverydayDefinedTime == true)
                        {
                            ChamberSchedule.EverydayDefinedTime = true;
                            ChamberSchedule.SpecificDaysDefinedTime = false;
                        }
                        else
                        {
                            ChamberSchedule.EverydayDefinedTime = false;
                            ChamberSchedule.SpecificDaysDefinedTime = true;
                        }

                        if (ChamberSchedule != null)
                        {
                            chamber.OpeningTime = ChamberSchedule.ChamberOpeningTime;
                            chamber.ClosingTime = ChamberSchedule.ChamberClosingTime;
                        }
                        Dashboard.Current.GoToSchedulePage(chamber);
                    }
                    else
                    {
                        //user didnt define the main time. do appropriate tasks and don't save
                        MainWindow.Current.ShowInvalidTimeDialog();
                    }
                }
                else
                {
                    //invalid date. dont save.
                    ChamberSchedule.StartDate = null;
                    ChamberSchedule.EndDate = null;
                    MainWindow.Current.ShowInvalidDateDialog();
                    StartDate.SelectedDate = null;
                    EndDate.SelectedDate = null;
                }
            }
            else
            {
                //date is not defined. dont save
                MainWindow.Current.ShowInvalidDateDialog();
            }
        }

        private void DefinedTimePerPatientComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = DefinedTimePerPatientComboBox.SelectedIndex;

            switch (index)
            {
                case 0:
                    ChamberSchedule.DefinedTimePerPatient = TimeSpan.FromMinutes(5);
                    break;

                case 1:
                    ChamberSchedule.DefinedTimePerPatient = TimeSpan.FromMinutes(10);
                    break;

                case 2:
                    ChamberSchedule.DefinedTimePerPatient = TimeSpan.FromMinutes(15);
                    break;

                case 3:
                    ChamberSchedule.DefinedTimePerPatient = TimeSpan.FromMinutes(20);
                    break;

                case 4:
                    ChamberSchedule.DefinedTimePerPatient = TimeSpan.FromMinutes(30);
                    break;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoBack();
        }

        private void PatientCountComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var index = PatientCountComboBox.SelectedIndex;
            switch (index)
            {
                case 0:
                    ChamberSchedule.VisitPatientPerTimeSlot = 1;
                    break;

                case 1:
                    ChamberSchedule.VisitPatientPerTimeSlot = 2;
                    break;

                case 2:
                    ChamberSchedule.VisitPatientPerTimeSlot = 3;
                    break;

                default:
                    ChamberSchedule.VisitPatientPerTimeSlot = 1;
                    break;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
