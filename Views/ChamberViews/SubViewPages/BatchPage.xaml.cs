﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews.SubViewPages
{
    /// <summary>
    /// Interaction logic for BatchPage.xaml
    /// </summary>
    public partial class BatchPage : Page
    {
        public static BatchPage Current;
        //public ObservableCollection<ScheduleModel> scheduleModels = new ObservableCollection<ScheduleModel>();
        public ChamberModel Chamber;
        public BatchPage()
        {
            InitializeComponent();
            Current = this;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BatchListView.ItemsSource = Chamber.ChamberSchedules;
        }
    }
}
