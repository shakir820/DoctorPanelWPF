﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.ChamberViews
{
    /// <summary>
    /// Interaction logic for CreateNewChamber.xaml
    /// </summary>
    public partial class CreateNewChamber : Page
    {
        public CreateNewChamber()
        {
            InitializeComponent();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.GoBack();
        }

    
        private void ChamberSearchListView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var unit = e.Delta;
           
            MainScrollViewer.ScrollToVerticalOffset(MainScrollViewer.VerticalOffset - unit);
        }

        private void CreateNewChamberBtn_Click(object sender, RoutedEventArgs e)
        {
            Dashboard.Current.frame.Navigate(new CreateNewChamberPage2());
        }
    }
}
