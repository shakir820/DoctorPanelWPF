﻿using DoctorPanelWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.Views.OnlineConsultationViews
{
    /// <summary>
    /// Interaction logic for OnlineConsultation.xaml
    /// </summary>
    public partial class OnlineConsultation : Page
    {
        public ObservableCollection<Question> Questions = new ObservableCollection<Question>();
        public static OnlineConsultation Current;
        public ObservableCollection<QuestionTag> QuestionPreferences = new ObservableCollection<QuestionTag>();

        public OnlineConsultation()
        {
            InitializeComponent();
            Current = this;
            SearchResultListView.ItemsSource = Questions;
            GenerateQuestions();
        }

        public void GenerateQuestions()
        {
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
            Questions.Add(new Question() { Title = "Views What could be the reason of \"typedef\" may not be specified here", Age = 45, Date = "Dec 15, 2018", Detail = "We further debugged on this issue and we see that the below error message \"Caused by: org.apache.jackrabbit.rmi.client.RemoteRepositoryException: java.rmi.UnmarshalException: Error unmarshaling", Gender = Gender.Male, QuestionedBy = "Shakir Ahmed" });
        }

        private void ViewBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AnswerBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchResultListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void PreferenceBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.ShowEditPreferencesDialog();
        }

        public void GenerateQuestionTags()
        {
            QuestionPreferences.Add(new QuestionTag() { Name = "THD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "BCD", IsChecked = true });
            QuestionPreferences.Add(new QuestionTag() { Name = "RTY", IsChecked = true });
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            SelectedTagsListView.ItemsSource = QuestionTags.Current.QuestionPreferences;
        }
    }
}
