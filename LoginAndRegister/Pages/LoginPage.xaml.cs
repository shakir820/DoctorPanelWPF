﻿using DoctorPanelWPF.Helpers;
using DoctorPanelWPF.Models.API_Data;
using DoctorPanelWPF.Models.LoginRemeberPassword;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.LoginAndRegister.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page, INotifyPropertyChanged
    {
        public LoginPage()
        {
            InitializeComponent();
            DataContext = this;
        }
        
        private string _email;
        private string _password;
        private bool RememberPasssword = false;
        private bool SaveUserData;
        private bool InLoginProcess = false;

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                if(value != null)
                {
                    CheckForEmailValidation((string)value);
                }

                RaisePropertyChanged();
            }
        }

        public string Password { get { return _password; } set { _password = value; RaisePropertyChanged();} }

        private void RegisterTextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }


        private void CheckForEmailValidation(string e)
        {
            if(IsValidEmail(e))
            {
                EmailWarningSP.Visibility = Visibility.Hidden;
            }
            else
            {
                EmailWarningTextBlock.Text = "Define a valid email";
                EmailWarningSP.Visibility = Visibility.Visible;
            }
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void ForgotPasswordBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void RegisterBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LoginWindow.Current.GoToRegisterPage();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = passwordBox.Password;
        }



        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void RememberCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            RememberPasssword = true;
        }

        private void RememberCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            RememberPasssword = false;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //get user data if available
            Email = LoginWindow.Current.email;
            Password = LoginWindow.Current.password;
            passwordBox.Password = Password??"";
            RememberPasssword = LoginWindow.Current.rememberPassword;
            if (RememberPasssword == true)
            {
                RememberCheckBox.IsChecked = true;
            }
            else RememberCheckBox.IsChecked = false;
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            if(InLoginProcess == false)
            {
                InLoginProcess = true;
                // start loading
                LoginWindow.Current.StartLoading();
                //check for email and password if they are ok with valid info
                Thread loginThread = new Thread(InitialLoginOperation);
                loginThread.Start();
            }


        }



      

        private async void HitLoginApiAsync(string email, string password)
        {
            Welcome welcome = null;
            bool canLogin = true;

            string uri = "http://api.healthsheba.com/api/v2/auth/signin?email=" + email + "&password=" + password;

            StringContent a = new StringContent("");

            //may throw exception
            try
            {
                HttpResponseMessage response = await App.client.PostAsync(uri, a);
                try
                {
                    response.EnsureSuccessStatusCode();
                    var stringContent = await response.Content.ReadAsStringAsync();
                    welcome = Welcome.FromJson(stringContent);

                    if (welcome.Status == "Credential not matched")
                    {
                        Dispatcher.Invoke(()=> 
                        {
                            PasswordWarningTextBlock.Text = "Authentication failed";
                            PassswordWarningSP.Visibility = Visibility.Visible;
                        });
                        canLogin = false;
                    }

                    if (welcome.Status == "Sorry, user is not registered.")
                    {
                        Dispatcher.Invoke(()=> 
                        {
                            EmailWarningTextBlock.Text = "Email doesn't exist";
                            EmailWarningSP.Visibility = Visibility.Visible;
                        });
                        canLogin = false;
                    }

                    if (canLogin == true)
                    {
                        //open MainWindow and go to dashboard and retrieve data
                        var user_data = FetchDataHelper.FetchUserData(welcome.User);
                        Dispatcher.Invoke(() =>
                        {
                            MainWindow mainWindow = new MainWindow();
                            mainWindow.loginWindow = LoginWindow.Current;
                            mainWindow.UserData = user_data;
                            mainWindow.Show();
                        });
                       
                    }
                    else
                    {
                        
                    }
                }
                catch
                {
                    //Cannot convert data;
                }
                // return URI of the created resource.
                //var yy = response.Headers.Location;
            }
            catch
            {
                //cannot communicate with server. It may have many reasons.
                
            }
        }


        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void InitialLoginOperation()
        {
            //check for internet connection
            if (CheckForInternetConnection())
            {
                if (Email != "" && Password != "" && Email != null && Password != null)
                {

                    if (IsValidEmail(Email))
                    {
                        //now save login data
                        if (RememberPasssword == true)
                        {
                            LoginHelper.SaveLoginInfo(new LoginInfoModel() { email = Email, password = Password, rememberPassword = RememberPasssword });
                        }

                        //now hit api
                        HitLoginApiAsync(Email, Password);
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            EmailWarningTextBlock.Text = "Enter a valid email";
                            EmailWarningSP.Visibility = Visibility.Visible;
                        });
                    }
                }
                else
                {
                    if (Password == null || Password == "")
                    {
                        Dispatcher.Invoke(() =>
                        {
                            PasswordWarningTextBlock.Text = "Enter password";
                            PassswordWarningSP.Visibility = Visibility.Visible;
                        });
                    }

                    if (Email == "" || Email == null)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            EmailWarningTextBlock.Text = "Enter email";
                            EmailWarningSP.Visibility = Visibility.Visible;
                        });
                    }
                }
            }
            else
            {
                Dispatcher.Invoke(ShowInternetErrorNotification);
            }

            Dispatcher.Invoke(()=> { LoginWindow.Current.StopLoading(); });
            InLoginProcess = false;
        }


        private void ShowInternetErrorNotification()
        {
            NotificationWindow.Notification notification = new NotificationWindow.Notification();
            notification.Show();
        }
    }
}
