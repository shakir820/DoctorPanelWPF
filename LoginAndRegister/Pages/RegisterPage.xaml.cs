﻿using DoctorPanelWPF.Helpers;
using DoctorPanelWPF.Models;
using DoctorPanelWPF.Models.API_Data;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DoctorPanelWPF.LoginAndRegister.Pages
{
    /// <summary>
    /// Interaction logic for RegisterPage.xaml
    /// </summary>
    public partial class RegisterPage : Page, INotifyPropertyChanged
    {
        public RegisterPage()
        {
            InitializeComponent();
            DataContext = this;
        }

        static HttpClient client = new HttpClient();
        private bool InRegistrationProcess = false;
        private string firstName;
        private string lastName;
        private string email;
        private string password;
        private string bmdc;

        public string FirstName
        {
            get { return firstName; }
            set
            {
                firstName = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get { return lastName; }
            set
            {
                lastName = value;
                RaisePropertyChanged();
            }
        }
        public string Email { get { return email; } set { email = value; RaisePropertyChanged(); } }
        public string Password { get { return password; } set { password = value; RaisePropertyChanged(); } }
        public string BMDC { get { return bmdc; } set { bmdc = value; RaisePropertyChanged(); } }



        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //EmailControl.ShowDefineEmailWarning();
        }



        private void LogInBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LoginWindow.Current.GotoLoginPage();
        }


        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged([CallerMemberName]string name = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }


        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private async void InitiateRegistrationProcessAsync()
        {
            Password = PasswordControl.GetPassword();

            //check for internet connection
            if (CheckForInternetConnection())
            {
                if (FirstName != "" && lastName != "" && BMDC != "" && Email != "" && Password != "" && FirstName != null && lastName != null && BMDC != null && Email != null && Password != null)
                {
                    //now validate if they are correct
                    //check for valid email address
                    if (EmailControl.IsValidEmail(Email))
                    {
                        //Now hit API
                        var data = await GetData(FirstName + " " + LastName, BMDC, Email, Password);
                        
                        if(data != null)
                        {
                            UserModel userModel = FetchDataHelper.FetchUserData(data);
                            // do futher work
                            Dispatcher.Invoke(() => 
                            {
                                MainWindow mainWindow = new MainWindow();
                                mainWindow.UserData = userModel;
                                mainWindow.Show();
                                return;
                            });
                        }
                        {
                            //show error

                        }
                    }
                    else
                    {
                        //invalid email address
                        //do nothing
                    }
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (FirstName == "" || FirstName == null)
                        {
                            firstNameWarning.Visibility = Visibility.Visible;
                        }

                        if (LastName == "" || LastName == null)
                        {
                            lastNameWarning.Visibility = Visibility.Visible;
                        }

                        if (Email == "" || Email == null)
                        {
                            EmailControl.ShowDefineEmailWarning();
                        }

                        if (BMDC == "" || BMDC == null)
                        {
                            bmdcControl.ShowDefineBMDCCertificateWarning();
                        }

                        if (Password == "")
                        {
                            PasswordControl.ShowDefinePasswordWarning();
                        }
                    });
                }
            }
            else
            {
               Dispatcher.Invoke(ShowInternetErrorNotification);
            }

            //stop loading
            Dispatcher.Invoke(LoginWindow.Current.StopLoading);
            InRegistrationProcess = false;
        }

        private void RegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            if(InRegistrationProcess != true)
            {
                InRegistrationProcess = true;
                //start loading
                LoginWindow.Current.StartLoading();
                Thread thread = new Thread(InitiateRegistrationProcessAsync);
                thread.Start();
            }
        }


        private async Task<User> GetData(string name, string bm, string e, string pass)
        {
            Welcome welcome = null;
            bool canRegister = true;
            // may throw exception
            string uri = "http://api.healthsheba.com/api/v2/auth/register?email=" + e + "&" + "password=" 
                + pass + "&is_practitioner=1&bmdc_cert_no=" + bm + "&name=" + name;

            StringContent a = new StringContent("");

            //may throw exception
            try
            {
                HttpResponseMessage response = await client.PostAsync(uri, a);
                try
                {
                    response.EnsureSuccessStatusCode();
                    var stringContent = await response.Content.ReadAsStringAsync();
                    welcome = Welcome.FromJson(stringContent);

                    if (welcome.Status == "You need to provide unique Bmdc Certificate Number")
                    {
                        Dispatcher.Invoke(bmdcControl.ShowAlreadyExistBMDCCertificateWarning);
                        canRegister = false;
                    }

                    if (welcome.Status == "You need to provide unique email address")
                    {
                        Dispatcher.Invoke(EmailControl.ShowAlreadyExistWarning);
                        canRegister = false;
                    }

                    if (canRegister == false)
                    {
                        return null;
                    }
                    else
                    {
                        return welcome.User;
                    }
                }
                catch
                {
                    return null;
                }
                // return URI of the created resource.
                //var yy = response.Headers.Location;
            }
            catch
            {
                //cannot communicate with server. It may have many reasons.
                return null;
            }

        }

        private void ShowInternetErrorNotification()
        {
            NotificationWindow.Notification notification = new NotificationWindow.Notification();
            notification.Show();
        }

        private void FirstNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            firstNameWarning.Visibility = Visibility.Hidden;
        }

        private void LastNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            lastNameWarning.Visibility = Visibility.Hidden;
        }
    }
}
