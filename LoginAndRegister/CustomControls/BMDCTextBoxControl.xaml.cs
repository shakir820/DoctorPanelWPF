﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.LoginAndRegister.CustomControls
{
    /// <summary>
    /// Interaction logic for BMDCTextBoxControl.xaml
    /// </summary>
    public partial class BMDCTextBoxControl : UserControl
    {
        public BMDCTextBoxControl()
        {
            InitializeComponent();
        }


        public void ShowDefineBMDCCertificateWarning()
        {
            WarningTextBlock.Text = "Define BMDC certificate no";
            WarningTextBlock.Visibility = Visibility.Visible;
        }

        public void ShowAlreadyExistBMDCCertificateWarning()
        {
            WarningTextBlock.Text = "BMDC certificate already exist";
            WarningTextBlock.Visibility = Visibility.Visible;
        }

        private void BMDCTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            WarningTextBlock.Visibility = Visibility.Hidden;
        }
    }
}
