﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.LoginAndRegister.CustomControls
{
    /// <summary>
    /// Interaction logic for RegisterPasswordControl.xaml
    /// </summary>
    public partial class RegisterPasswordControl : UserControl
    {
        public RegisterPasswordControl()
        {
            InitializeComponent();
        }

        public void ShowDefinePasswordWarning()
        {
            WarningTextBlock.Text = "Enter password";
            WarningTextBlock.Visibility = Visibility.Visible;
        }

       

        public string GetPassword()
        {
            if (!string.IsNullOrEmpty(passwordBox.Password))
            {
                return passwordBox.Password;
            }
            else return "";
        }
        //public void ShowAlreadyExistBMDCCertificateWarning()
        //{
        //    WarningTextBlock.Text = "";
        //    WarningTextBlock.Visibility = Visibility.Visible;
        //}


        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            WarningTextBlock.Visibility = Visibility.Hidden;
        }
    }
}
