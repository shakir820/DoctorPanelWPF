﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoctorPanelWPF.LoginAndRegister.CustomControls
{
    /// <summary>
    /// Interaction logic for EmailTextBoxControl.xaml
    /// </summary>
    public partial class EmailTextBoxControl : UserControl
    {
        public EmailTextBoxControl()
        {
            InitializeComponent();
        }

        public bool InPropertyChangedCallback = false;

        //public bool InvalidEmail
        //{
        //    get { return (bool)GetValue(InvalidEmailProperty); }
        //    set { SetValue(InvalidEmailProperty, value); }
        //}

        //public static readonly DependencyProperty InvalidEmailProperty = DependencyProperty.Register(
        //    "InvalidEmail", typeof(bool), typeof(EmailTextBoxControl), new PropertyMetadata(false, new
        //         PropertyChangedCallback(InvalidEmailPropertyChangedCallback)));

        //private static void InvalidEmailPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    EmailTextBoxControl control = (EmailTextBoxControl)d;
        //    if(control.InPropertyChangedCallback == false)
        //    {
        //        if((bool)e.NewValue == true)
        //        {
        //            control.WarningTextBlock.Visibility = Visibility.Visible;
        //            control.WarningTextBlock.Text = "Email already exist";
        //        }
        //        else
        //        {
        //            control.WarningTextBlock.Visibility = Visibility.Hidden;
        //        }
        //    }
        //}

        public void ShowAlreadyExistWarning()
        {
            WarningTextBlock.Text = "Email already exist";
            WarningTextBlock.Visibility = Visibility.Visible;
        }

        public void ShowDefineEmailWarning()
        {
            WarningTextBlock.Text = "Enter email address";
            WarningTextBlock.Visibility = Visibility.Visible;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void EmailTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsValidEmail(EmailTextBox.Text))
            {
                WarningTextBlock.Visibility = Visibility.Hidden;
            }
            else
            {
                WarningTextBlock.Text = "Define a valid email address";
                WarningTextBlock.Visibility = Visibility.Visible;
            }

        }
    }
}
