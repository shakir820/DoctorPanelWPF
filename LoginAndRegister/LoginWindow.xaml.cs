﻿using DoctorPanelWPF.LoginAndRegister.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoctorPanelWPF.LoginAndRegister
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public static LoginWindow Current;

        public LoginWindow()
        {
            InitializeComponent();
            Current = this;
        }

        #region user credantial
        public string email;
        public string password;
        public bool rememberPassword = false;
        #endregion


        public void StartLoading()
        {
            Loader.IsIndeterminate = true;
        }

        public void StopLoading()
        {
            Loader.IsIndeterminate = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //restore user loging data
            var userLoginInfo = Models.LoginRemeberPassword.LoginHelper.RetrieveLoginInfo();

            if(userLoginInfo != null)
            {
                email = userLoginInfo.email ?? null;
                password = userLoginInfo.password ?? null;
                rememberPassword = userLoginInfo.rememberPassword;
            }

            Pages.LoginPage loginPage = new Pages.LoginPage();
            frame.Navigate(loginPage);
        }

        public void GoToRegisterPage()
        {
            RegisterPage reg = new RegisterPage();
            frame.Navigate(reg);
        }

        public void GotoLoginPage()
        {
            LoginPage login = new LoginPage();
            frame.Navigate(login);
        }

        private void ShutDownBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void MinimizeBtn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

       

        private void MaximizeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }
    }
}
