﻿#pragma checksum "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "74D9CAA74AED888CA52628F961326515C4D0B4D7"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AnimatedScrollViewer;
using DoctorPanelWPF.CustomControls;
using DoctorPanelWPF.Models;
using DoctorPanelWPF.Views.ChamberViews;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DoctorPanelWPF.Views.ChamberViews {
    
    
    /// <summary>
    /// CreateNewChamberPage2
    /// </summary>
    public partial class CreateNewChamberPage2 : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 150 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AnimatedScrollViewer.AnimatedScrollViewer MainScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ChamberNameTextBlock;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ChamberNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ChamberTypeComboBox;
        
        #line default
        #line hidden
        
        
        #line 319 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DoctorPanelWPF.CustomControls.PhoneNumberListControl PersonalContactList;
        
        #line default
        #line hidden
        
        
        #line 354 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DoctorPanelWPF.CustomControls.PhoneNumberListControl OfficeContactList;
        
        #line default
        #line hidden
        
        
        #line 388 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DoctorPanelWPF.CustomControls.PhoneNumberListControl SerialContactList;
        
        #line default
        #line hidden
        
        
        #line 422 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox DivisionComboBox;
        
        #line default
        #line hidden
        
        
        #line 470 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox DistrictComboBox;
        
        #line default
        #line hidden
        
        
        #line 518 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AreaComboBox;
        
        #line default
        #line hidden
        
        
        #line 566 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AddressTextBox;
        
        #line default
        #line hidden
        
        
        #line 604 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DescriptionTextBox;
        
        #line default
        #line hidden
        
        
        #line 652 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NewVisitPriceTextBox;
        
        #line default
        #line hidden
        
        
        #line 690 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ReturnVisitPriceTextBox;
        
        #line default
        #line hidden
        
        
        #line 729 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ReportPriceTextBox;
        
        #line default
        #line hidden
        
        
        #line 760 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.ImageBrush CoverImage;
        
        #line default
        #line hidden
        
        
        #line 765 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ImagePickerBtn;
        
        #line default
        #line hidden
        
        
        #line 778 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelBtn;
        
        #line default
        #line hidden
        
        
        #line 784 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DoctorPanelWPF;component/views/chamberviews/createnewchamberpage2.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 16 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            ((DoctorPanelWPF.Views.ChamberViews.CreateNewChamberPage2)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Page_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MainScrollViewer = ((AnimatedScrollViewer.AnimatedScrollViewer)(target));
            return;
            case 3:
            this.ChamberNameTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.ChamberNameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.ChamberTypeComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 270 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            this.ChamberTypeComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ChamberTypeComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.PersonalContactList = ((DoctorPanelWPF.CustomControls.PhoneNumberListControl)(target));
            return;
            case 7:
            this.OfficeContactList = ((DoctorPanelWPF.CustomControls.PhoneNumberListControl)(target));
            return;
            case 8:
            this.SerialContactList = ((DoctorPanelWPF.CustomControls.PhoneNumberListControl)(target));
            return;
            case 9:
            this.DivisionComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 421 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            this.DivisionComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.DivisionComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.DistrictComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.AreaComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.AddressTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.DescriptionTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.NewVisitPriceTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.ReturnVisitPriceTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.ReportPriceTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.CoverImage = ((System.Windows.Media.ImageBrush)(target));
            return;
            case 18:
            this.ImagePickerBtn = ((System.Windows.Controls.Button)(target));
            
            #line 766 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            this.ImagePickerBtn.Click += new System.Windows.RoutedEventHandler(this.ImagePickerBtn_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.CancelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 783 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            this.CancelBtn.Click += new System.Windows.RoutedEventHandler(this.CancelBtn_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.SaveBtn = ((System.Windows.Controls.Button)(target));
            
            #line 786 "..\..\..\..\Views\ChamberViews\CreateNewChamberPage2.xaml"
            this.SaveBtn.Click += new System.Windows.RoutedEventHandler(this.SaveBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

